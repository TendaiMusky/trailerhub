package com.tendai.trailerhub.data.response;

import com.tendai.trailerhub.data.model.SeriesGenreList;

import java.util.ArrayList;
import java.util.List;

public class FakeOfficialSeriesGenres extends ResponseFactory<SeriesGenreList> {
    @Override
    List<SeriesGenreList> createList() {
        ArrayList<SeriesGenreList> genres = new ArrayList<>();
        genres.add(new SeriesGenreList(10759, "Action & Adventure"));
        genres.add(new SeriesGenreList(80,"Crime"));
        genres.add(new SeriesGenreList(16,"Animation"));
        genres.add(new SeriesGenreList(35,"Comedy"));

        return genres;
    }

    @Override
    SeriesGenreList create() {
        return null;
    }
}
