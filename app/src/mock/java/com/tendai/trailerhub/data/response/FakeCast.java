package com.tendai.trailerhub.data.response;

import com.tendai.trailerhub.data.model.Cast;

import java.util.ArrayList;
import java.util.List;

public class FakeCast extends ResponseFactory<Cast> {

    @Override
    Cast create() {
        return new Cast(23,"Cast 23","profilePath","Character 23",27);
    }

    @Override
    protected List<Cast> createList() {

        ArrayList<Cast> cast = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            cast.add(new Cast(i,"Cast "+i,"profilePath  ","Character "+i,25,15));
        }
        return cast;
    }
}
