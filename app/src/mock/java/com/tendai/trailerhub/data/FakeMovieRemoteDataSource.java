package com.tendai.trailerhub.data;

import com.tendai.trailerhub.data.model.Cast;
import com.tendai.trailerhub.data.model.Movie;
import com.tendai.trailerhub.data.model.MovieGenreList;
import com.tendai.trailerhub.data.response.FakeCast;
import com.tendai.trailerhub.data.response.FakeMovies;
import com.tendai.trailerhub.data.response.FakeOfficialMovieGenres;
import com.tendai.trailerhub.data.response.ResponseFactory;

import java.util.List;

public class FakeMovieRemoteDataSource implements MovieDataSource.Remote {
    private static FakeMovieRemoteDataSource INSTANCE;

    private FakeMovieRemoteDataSource() {

    }

    @Override
    public void getMovieDetails(long movieId, CallBacks.GetMovieDetails callback) {
        ResponseFactory<Movie> movieList = new FakeMovies();
        Movie movie = movieList.response();
        callback.onSuccess(movie);

    }

    @Override
    public void getMovieCast(long movieId, CallBacks.GetMovieCast callback) {
        ResponseFactory<Cast> castList = new FakeCast();
        List<Cast> cast = castList.responseList();
        callback.onSuccess(cast);
    }

    @Override
    public void getMovieByCategory(String category, CallBacks.GetMovies callback) {
        ResponseFactory<Movie> movieList = new FakeMovies();
        List<Movie> movies = movieList.responseList();
        callback.onSuccess(movies);
    }

    @Override
    public void getMovieByGenre(long genreId, CallBacks.GetMovies callback) {
        ResponseFactory<Movie> movieList = new FakeMovies();
        List<Movie> movies = movieList.responseList();
        callback.onSuccess(movies);
    }

    @Override
    public void getOfficialMovieGenres(CallBacks.GetMovieGenres callback) {
        ResponseFactory<MovieGenreList> response = new FakeOfficialMovieGenres();
        List<MovieGenreList> genres = response.responseList();
        callback.onSuccess(genres);
    }

    @Override
    public void refreshMovies() {

    }

    public static FakeMovieRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FakeMovieRemoteDataSource();
        }
        return INSTANCE;
    }
}
