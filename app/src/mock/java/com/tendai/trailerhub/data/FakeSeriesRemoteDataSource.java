package com.tendai.trailerhub.data;

import com.tendai.trailerhub.data.model.Cast;
import com.tendai.trailerhub.data.model.Series;
import com.tendai.trailerhub.data.model.SeriesGenreList;
import com.tendai.trailerhub.data.response.FakeCast;
import com.tendai.trailerhub.data.response.FakeOfficialSeriesGenres;
import com.tendai.trailerhub.data.response.FakeSeries;
import com.tendai.trailerhub.data.response.ResponseFactory;

import java.util.List;

public class FakeSeriesRemoteDataSource implements SeriesDataSource.Remote {
    private static FakeSeriesRemoteDataSource INSTANCE;

    private FakeSeriesRemoteDataSource() {

    }

    @Override
    public void getSeriesDetails(long SeriesId, CallBacks.GetSeriesDetails callback) {
        ResponseFactory<Series> responseFactory = new FakeSeries();
        Series series = responseFactory.response();
        callback.onSuccess(series);
    }

    @Override
    public void getSeriesCast(long seriesId, CallBacks.GetSeriesCast callback) {
        ResponseFactory<Cast> responseFactory = new FakeCast();
        List<Cast> cast = responseFactory.responseList();
        callback.onSuccess(cast);
    }

    @Override
    public void getSeriesByCategory(String category, CallBacks.GetSeries callback) {
        ResponseFactory<Series> responseFactory = new FakeSeries();
        List<Series> series = responseFactory.responseList();
        callback.onSuccess(series);
    }

    @Override
    public void getSeriesByGenre(long seriesId, CallBacks.GetSeries callback) {
        ResponseFactory<Series> responseFactory = new FakeSeries();
        List<Series> series = responseFactory.responseList();
        callback.onSuccess(series);
    }

    @Override
    public void getOfficialSeriesGenres(CallBacks.GetSeriesGenres callback) {
        ResponseFactory<SeriesGenreList> responseFactory = new FakeOfficialSeriesGenres();
        List<SeriesGenreList> genres = responseFactory.responseList();
        callback.onSuccess(genres);
    }

    @Override
    public void refreshSeries() {

    }


    public static FakeSeriesRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FakeSeriesRemoteDataSource();
        }
        return INSTANCE;
    }

}
