package com.tendai.trailerhub.data.response;


import com.tendai.trailerhub.data.model.Movie;
import com.tendai.trailerhub.data.model.MovieGenreList;

import java.util.ArrayList;
import java.util.List;

public class FakeMovies extends ResponseFactory<Movie> {
    @Override
    protected List<Movie> createList() {
        ArrayList<Movie> movies = new ArrayList<>();
        List<MovieGenreList> genres = new ArrayList<>();
        List<MovieGenreList> genres2 = new ArrayList<>();

        genres.add(new MovieGenreList(28, "Action"));
        genres.add(new MovieGenreList(16, "Animation"));
        genres.add(new MovieGenreList(35, "Comedy"));

        genres2.add(new MovieGenreList(35, "Comedy"));
        genres2.add(new MovieGenreList(12, "Adventure"));

        for (int i = 1; i < 4; i++) {
            movies.add(new Movie(i,
                    "Movie " + i,
                    "This is  the description of movie " + i,
                    189,
                    7.3,
                    "posterPath",
                    "null",
                    "201"+i,
                    genres,
                    "TOP RATED"
                    ));
        }
        for (int i = 23; i < 28; i++) {
            movies.add(new Movie(i,
                    "Movie " + i,
                    "This is the description of movie " + i,
                    187,
                    2.3+i,
                    "posterPath",
                    "backdropPath",
                    "20"+i,
                    genres2,
                    "null"
                    ));
        }
        return movies;
    }

    @Override
    Movie create() {
        List<MovieGenreList> genres = new ArrayList<>();

        genres.add(new MovieGenreList(28, "Action"));
        genres.add(new MovieGenreList(16, "Animation"));
        genres.add(new MovieGenreList(35, "Comedy"));

        return new Movie(234298,
                "Movie X" ,
                "This is the description of movie  X",
                187,
                4.7,
                "posterPath",
                "backdropPath",
                "2006",
                genres,
                "null"
        );
    }
}
