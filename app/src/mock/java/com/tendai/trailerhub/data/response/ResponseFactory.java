package com.tendai.trailerhub.data.response;

import java.util.List;

public abstract class ResponseFactory<T> {

    public List<T> responseList() {
        return createList();
    }

    public T response() {
        return create();
    }

    abstract List<T> createList();

    abstract T create();
}
