package com.tendai.trailerhub.data.response;


import com.tendai.trailerhub.data.model.MovieGenreList;

import java.util.ArrayList;
import java.util.List;

public class FakeOfficialMovieGenres extends ResponseFactory<MovieGenreList> {
    @Override
    protected List<MovieGenreList> createList() {
        ArrayList<MovieGenreList> genres = new ArrayList<>();
        genres.add(new MovieGenreList(28, "Action"));
        genres.add(new MovieGenreList(12,"Adventure"));
        genres.add(new MovieGenreList(16,"Animation"));
        genres.add(new MovieGenreList(35,"Comedy"));

        return genres;
    }

    @Override
    MovieGenreList create() {
        return null;
    }
}
