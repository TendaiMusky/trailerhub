package com.tendai.trailerhub.data;


public class FakeRemoteDataSource implements DataSource {
    private static FakeRemoteDataSource INSTANCE;

    private FakeRemoteDataSource() {
    }

    public static FakeRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FakeRemoteDataSource();
        }
        return INSTANCE;
    }


    @Override
    public void getTrending(String mediaType,
                            String timeFrame,
                            CallBacks.GetTrending callBack,
                            CallBacks.DeleteMoviesByCategory movieCallback,
                            CallBacks.DeleteSeriesByCategory seriesCallback,
                            CallBacks.SaveSeries saveSeriesCallback,
                            CallBacks.SaveMovies saveMovieCallback) {

    }

    @Override
    public void refreshTrending() {

    }
}
