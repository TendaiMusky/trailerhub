package com.tendai.trailerhub.data.response;


import com.tendai.trailerhub.data.model.Series;
import com.tendai.trailerhub.data.model.SeriesGenreList;

import java.util.ArrayList;
import java.util.List;

public class FakeSeries extends ResponseFactory<Series> {
    @Override
    protected List<Series> createList() {
        List<Series> series = new ArrayList<>();

        List<SeriesGenreList> genres = new ArrayList<>();
        genres.add(new SeriesGenreList(10759, "Action & Adventure"));
        genres.add(new SeriesGenreList(35, "Comedy"));

        List<SeriesGenreList> genres2 = new ArrayList<>();
        genres2.add(new SeriesGenreList(80, "Crime"));
        genres2.add(new SeriesGenreList(35, "Comedy"));
        genres2.add(new SeriesGenreList(16, "Animation"));

        for (int i = 11; i < 16; i++) {
            series.add(new Series(i,
                    "Series "+i,
                    "This is the description of Series "+i,
                    6,
                    5.6,
                    "backdropPath",
                    "backdropPath",
                    "20"+i,
                    genres2,
                    ""));
        }

        for (int i = 4; i < 7; i++) {
            series.add(new Series(i,
                    "Series "+i,
                    "This is the description of Series "+i,
                    6,
                    5.6,
                    "backdropPath",
                    "backdropPath",
                    "20"+i,
                    genres,
                    ""));
        }

        return series;
    }

    @Override
    Series create() {
        List<SeriesGenreList> genres = new ArrayList<>();
        genres.add(new SeriesGenreList(16, "Animation"));
        genres.add(new SeriesGenreList(35, "Comedy"));

        return new Series(23456,
                "Series 23456",
                "This is the description of Series 23456",
                6,
                5.6,
                "backdropPath",
                "backdropPath",
                "2016",
                genres,
                "");
    }
}
