package com.tendai.trailerhub;

import android.content.Context;

import androidx.annotation.NonNull;

import com.tendai.trailerhub.data.FakeMovieRemoteDataSource;
import com.tendai.trailerhub.data.MovieRepository;
import com.tendai.trailerhub.data.local.LocalMovieDataSource;
import com.tendai.trailerhub.data.local.MovieSeriesDatabase;
import com.tendai.trailerhub.domain.UseCaseHandler;
import com.tendai.trailerhub.domain.usecase.movies.GetMovie;

public class MovieInjection {
    private static MovieRepository provideMovieRepository(@NonNull Context context) {
        MovieSeriesDatabase database = MovieSeriesDatabase.getInstance(context);
        return MovieRepository.getInstance(FakeMovieRemoteDataSource.getInstance(),
                LocalMovieDataSource.getInstance(database.movieDao(),
                        database.castDao(),
                        database.officialMovieGenresDao(),
                        database.movieGenreCrossReferenceDao()));
    }

    public static UseCaseHandler provideUseCasHandler(@NonNull Context context) {
        return UseCaseHandler.getInstance();
    }

    public static GetMovie provideGetMovie(@NonNull Context context) {
        return new GetMovie(MovieInjection.provideMovieRepository(context));
    }

    public static GetMovieCast provideGetMovieCast(@NonNull Context context) {
        return new GetMovieCast(MovieInjection.provideMovieRepository(context));
    }

    public static GetMovieGenres provideGetMovieGenres(@NonNull Context context) {
        return new GetMovieGenres(MovieInjection.provideMovieRepository(context));
    }

    public static GetOfficialMovieGenres provideGetOfficialMovieGenres(@NonNull Context context) {
        return new GetOfficialMovieGenres(MovieInjection.provideMovieRepository(context));
    }

    public static SaveMovie provideSaveMovie(@NonNull Context context) {
        return new SaveMovie(MovieInjection.provideMovieRepository(context));
    }

    public static SaveMovieCast provideSaveMovieCast(@NonNull Context context) {
        return new SaveMovieCast(MovieInjection.provideMovieRepository(context));
    }

    public static SaveMovieGenreCrossReferences provideSaveMovieGenreCrossReferences(@NonNull Context context) {
        return new SaveMovieGenreCrossReferences(MovieInjection.provideMovieRepository(context));
    }

    public static SaveMovieOfficialGenres provideSaveMovieOfficialGenres(@NonNull Context context) {
        return new SaveMovieOfficialGenres(MovieInjection.provideMovieRepository(context));
    }
}