package com.tendai.trailerhub;

import android.content.Context;

import androidx.annotation.NonNull;

import com.tendai.trailerhub.data.FakeRemoteDataSource;
import com.tendai.trailerhub.data.Repository;
import com.tendai.trailerhub.data.local.LocalMovieDataSource;
import com.tendai.trailerhub.data.local.LocalSeriesDataSource;
import com.tendai.trailerhub.data.local.MovieSeriesDatabase;
import com.tendai.trailerhub.domain.UseCaseHandler;
import com.tendai.trailerhub.domain.usecase.GetTrending;

public class Injection {
    private static Repository provideInjection(@NonNull Context context) {
        MovieSeriesDatabase database = MovieSeriesDatabase.getInstance(context);

        return Repository.getInstance(FakeRemoteDataSource.getInstance(),
                LocalMovieDataSource.getInstance(database.movieDao(),
                        database.castDao(),
                        database.officialMovieGenresDao(),
                        database.movieGenreCrossReferenceDao()),
                LocalSeriesDataSource.getInstance(database.seriesDao(),
                        database.castDao(),
                        database.officialSeriesGenresDao(),
                        database.seriesGenreCrossReferenceDao())
                );

    }
    public static UseCaseHandler provideUseCasHandler(@NonNull Context context) {
        return UseCaseHandler.getInstance();
    }

    public static GetTrending provideGetTrending(@NonNull Context context) {
        return new GetTrending(Injection.provideInjection(context));
    }
}
