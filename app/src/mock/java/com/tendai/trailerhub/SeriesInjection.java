package com.tendai.trailerhub;

import android.content.Context;

import androidx.annotation.NonNull;

import com.tendai.trailerhub.data.FakeSeriesRemoteDataSource;
import com.tendai.trailerhub.data.SeriesRepository;
import com.tendai.trailerhub.data.local.LocalSeriesDataSource;
import com.tendai.trailerhub.data.local.MovieSeriesDatabase;
import com.tendai.trailerhub.domain.UseCaseHandler;
import com.tendai.trailerhub.domain.usecase.series.GetSeries;

public class SeriesInjection {
    private static SeriesRepository provideSeriesRepository(@NonNull Context context) {
        MovieSeriesDatabase database = MovieSeriesDatabase.getInstance(context);
        return SeriesRepository.getInstance(FakeSeriesRemoteDataSource.getInstance(),
                LocalSeriesDataSource.getInstance(database.seriesDao(),
                        database.castDao(),
                        database.officialSeriesGenresDao(),
                        database.seriesGenreCrossReferenceDao()));
    }

    public static UseCaseHandler provideUseCasHandler(@NonNull Context context) {
        return UseCaseHandler.getInstance();
    }

    public static GetSeries provideGetSeries(@NonNull Context context) {
        return new GetSeries(SeriesInjection.provideSeriesRepository(context));
    }

    public static GetSeriesCast provideGetSeriesCast(@NonNull Context context) {
        return new GetSeriesCast(SeriesInjection.provideSeriesRepository(context));
    }

    public static GetSeriesGenres provideGetSeriesGenres(@NonNull Context context) {
        return new GetSeriesGenres(SeriesInjection.provideSeriesRepository(context));
    }

    public static GetOfficialSeriesGenres provideGetOfficialSeriesGenres(@NonNull Context context) {
        return new GetOfficialSeriesGenres(SeriesInjection.provideSeriesRepository(context));
    }

    public static SaveSeries provideSaveSeries(@NonNull Context context) {
        return new SaveSeries(SeriesInjection.provideSeriesRepository(context));
    }

    public static SaveSeriesCast provideSaveSeriesCast(@NonNull Context context) {
        return new SaveSeriesCast(SeriesInjection.provideSeriesRepository(context));
    }

    public static SaveSeriesOfficialGenres provideSaveSeriesOfficialGenres(@NonNull Context context) {
        return new SaveSeriesOfficialGenres(SeriesInjection.provideSeriesRepository(context));
    }

    public static SaveSeriesGenresCrossReferences provideSaveSeriesCrossReferences(@NonNull Context context) {
        return new SaveSeriesGenresCrossReferences(SeriesInjection.provideSeriesRepository(context));
    }

}
