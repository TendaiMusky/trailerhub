package com.tendai.trailerhub;

import com.tendai.trailerhub.domain.UseCase;
import com.tendai.trailerhub.domain.UseCaseScheduler;

public class TestUseCasScheduler implements UseCaseScheduler {
    @Override
    public void execute(Runnable runnable) {
        runnable.run();
    }

    @Override
    public <V extends UseCase.ResponseValues> void notifySuccess(V response, UseCase.UseCaseCallback<V> useCaseCallback) {
        useCaseCallback.onSuccess(response);
    }

    @Override
    public <V extends UseCase.ResponseValues> void notifyFailure(UseCase.UseCaseCallback<V> useCaseCallback) {
        useCaseCallback.onFailure();
    }
}
