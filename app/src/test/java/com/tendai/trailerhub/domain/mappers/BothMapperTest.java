package com.tendai.trailerhub.domain.mappers;

import com.tendai.trailerhub.data.FakeBothData;
import com.tendai.trailerhub.data.FakeSimpleMovieData;
import com.tendai.trailerhub.data.FakeSimpleSeriesData;
import com.tendai.trailerhub.data.model.Both;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.domain.movieseries.MovieSeriesMapper;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class BothMapperTest {
    private MovieSeriesMapper mMapper;
    private List<Both> mBothList;
    private List<SimpleMovie> mSimpleMovieList;
    private List<SimpleSeries> mSimpleSeriesList;

    @Before
    public void init() {
        mMapper = new MovieSeriesMapper();
        mBothList = FakeBothData.provideFakeBothData();
        mSimpleMovieList = FakeSimpleMovieData.provideFakeSimpleMovieListBoth();
        mSimpleSeriesList = FakeSimpleSeriesData.provideFakeSimpleSeriesBoth();

    }

    @Test
    public void mapBoth_returnsSimpleMovieList() {
        List<SimpleMovie> simpleMovie = mMapper.mapMovie(mBothList);

        for (int i = 0; i < mSimpleMovieList.size(); i++) {
            assertEquals(simpleMovie.get(i).getId(), mSimpleMovieList.get(i).getId());
            assertEquals(simpleMovie.get(i).getTitle(), mSimpleMovieList.get(i).getTitle());
            assertEquals(simpleMovie.get(i).getBackdropPath(), mSimpleMovieList.get(i).getBackdropPath());
        }
    }

    @Test
    public void mapBoth_returnsSeriesList() {
        List<SimpleSeries> simpleSeries = mMapper.mapSeries(mBothList);

        for (int i = 0; i < mSimpleSeriesList.size(); i++) {
            assertEquals(simpleSeries.get(i).getId(), mSimpleSeriesList.get(i).getId());
            assertEquals(simpleSeries.get(i).getTitle(), mSimpleSeriesList.get(i).getTitle());
            assertEquals(simpleSeries.get(i).getBackdropPath(), mSimpleSeriesList.get(i).getBackdropPath());
        }
    }
}