package com.tendai.trailerhub.domain.mappers;

import com.tendai.trailerhub.data.FakeMovieData;
import com.tendai.trailerhub.data.FakeSimpleMovieData;
import com.tendai.trailerhub.data.model.Movie;
import com.tendai.trailerhub.domain.movie.MovieMapper;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class MovieMapperTest {
    private MovieMapper mMapper;
    private SimpleMovie mSimpleMovie1;
    private List<SimpleMovie> mSimpleMovieList;
    private List<Movie> mMovieList;
    private Movie mMovie;

    @Before
    public void init() {
        mMapper = new MovieMapper();
        mSimpleMovie1 = FakeSimpleMovieData.provideFakeSimpleMovie();
        mSimpleMovieList = FakeSimpleMovieData.provideFakeSimpleMovieList();
        mMovieList = FakeMovieData.provideFakeMovieList();
        mMovie = FakeMovieData.provideFakeMovie();
    }

    @Test
    public void mapMovie_returnSimpleMovie() {
        SimpleMovie simpleMovie = mMapper.map(mMovie);

        assertEquals(simpleMovie.getId(), mSimpleMovie1.getId());
        assertEquals(simpleMovie.getTitle(), mSimpleMovie1.getTitle());
        assertEquals(simpleMovie.getOverview(), mSimpleMovie1.getOverview());
        assertEquals(simpleMovie.getBackdropPath(), mSimpleMovie1.getBackdropPath());
        assertEquals(simpleMovie.getPosterPath(), mSimpleMovie1.getPosterPath());
        assertEquals(simpleMovie.getRuntime(), mSimpleMovie1.getRuntime());
        assertEquals(simpleMovie.getVoteAverage(), mSimpleMovie1.getVoteAverage(), 0.0);

        for (int i = 0; i < simpleMovie.getCast().size(); i++) {
            assertEquals(simpleMovie.getCast().get(i).getId(),  mSimpleMovie1.getCast().get(i).getId());
            assertEquals(simpleMovie.getCast().get(i).getName(),  mSimpleMovie1.getCast().get(i).getName());
            assertEquals(simpleMovie.getCast().get(i).getPosterPath(),  mSimpleMovie1.getCast().get(i).getPosterPath());
            assertEquals(simpleMovie.getCast().get(i).getCharacter(),  mSimpleMovie1.getCast().get(i).getCharacter());

        }

        for (int i = 0; i < simpleMovie.getGenres().size(); i++) {
            assertEquals(simpleMovie.getGenres().get(i).getId(),mSimpleMovie1.getGenres().get(i).getId());
            assertEquals(simpleMovie.getGenres().get(i).getName(), mSimpleMovie1.getGenres().get(i).getName());
        }
    }

    @Test
    public void mapMovieList_returnSimpleMovieList() {
        List<SimpleMovie> simpleMovieList = mMapper.map(mMovieList);

        for (int i = 0; i < mMovieList.size(); i++) {
            assertEquals(simpleMovieList.get(i).getId(),mSimpleMovieList.get(i).getId());
            assertEquals(simpleMovieList.get(i).getRuntime(),mSimpleMovieList.get(i).getRuntime());
            assertEquals(simpleMovieList.get(i).getGenreIds(),mSimpleMovieList.get(i).getGenreIds());
            assertEquals(simpleMovieList.get(i).getPosterPath(),mSimpleMovieList.get(i).getPosterPath());
            assertEquals(simpleMovieList.get(i).getOverview(),mSimpleMovieList.get(i).getOverview());
            assertEquals(simpleMovieList.get(i).getTitle(),mSimpleMovieList.get(i).getTitle());
            assertEquals(simpleMovieList.get(i).getVoteAverage(),mSimpleMovieList.get(i).getVoteAverage(),0.0);
        }
    }
}