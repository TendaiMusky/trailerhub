package com.tendai.trailerhub.domain.usecase;

import com.tendai.trailerhub.TestUseCasScheduler;
import com.tendai.trailerhub.data.CallBacks;
import com.tendai.trailerhub.data.MovieDataSource;
import com.tendai.trailerhub.domain.UseCase;
import com.tendai.trailerhub.domain.UseCaseHandler;
import com.tendai.trailerhub.domain.movie.usecase.GetMovie;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.verify;

public class GetMovieTest {
    private UseCaseHandler mUseCaseHandler;
    private GetMovie mGetMovie;

    private GetMovie.RequestValues mRequestValues;

//    @Captor
//    private ArgumentCaptor<GetMovie.RequestValues> mRequestValuesCaptor;

    @Mock
    private MovieDataSource mMovieRepository;

    //    @Mock
//    private CallBacks.GetMovieDetails mGetMovieDetails;
//
    @Captor
    private ArgumentCaptor<CallBacks.GetMovieDetails> mGetMovieDetailsCaptor;

    @Captor
    ArgumentCaptor<CallBacks.GetMovies> mGetMoviesCaptor;

    @Mock
    private UseCase.UseCaseCallback<GetMovie.ResponseValues> mUseCaseCallback;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mUseCaseHandler = new UseCaseHandler(new TestUseCasScheduler());
        mGetMovie = new GetMovie(mMovieRepository);
        mRequestValues  = new GetMovie.RequestValues("Drama",false,1);
        mRequestValues.setRequestType(GetMovie.RequestValues.TYPE_CATEGORY);
/*
Should For getMovieDetailsTest
 */
//        mRequestValues = new GetMovie.RequestValues(39);
//        mRequestValues.setRequestType(GetMovie.RequestValues.TYPE_MOVIE_ID);

/*
  For getMovieByGenreTest
 */
//        mRequestValues = new GetMovie.RequestValues("Hello",true,1);
//        mRequestValues.setRequestType(GetMovie.RequestValues.TYPE_GENRE_ID);
    }

    @Test
    public void shouldCall_getMovieDetails() {
        //when
        mUseCaseHandler.execute(mGetMovie, mRequestValues, mUseCaseCallback);

        //then
        verify(mMovieRepository).getMovieDetails(eq(mRequestValues.getMovieId()),
                mGetMovieDetailsCaptor.capture());

    }

    @Test
    public void shouldCall_getMovieByGenreId() {
        mUseCaseHandler.execute(mGetMovie, mRequestValues, mUseCaseCallback);

        verify(mMovieRepository).getMovieByGenre(eq(mRequestValues.getGenreId()),
                eq(mRequestValues.getPageNumber()), mGetMoviesCaptor.capture());
    }

    @Test
    public void shouldCall_getMovieByCategory() {
        mUseCaseHandler.execute(mGetMovie, mRequestValues, mUseCaseCallback);

        verify(mMovieRepository).getMovieByCategory(eq(mRequestValues.getCategory()),
                eq(mRequestValues.getPageNumber()),mGetMoviesCaptor.capture());

    }
//Left Out the other useCases as the logic is simply straightForward
}