package com.tendai.trailerhub.domain.mappers;

import com.tendai.trailerhub.data.FakeSeriesData;
import com.tendai.trailerhub.data.FakeSimpleSeriesData;
import com.tendai.trailerhub.data.model.Series;
import com.tendai.trailerhub.domain.series.SeriesMapper;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class SeriesMapperTest {
    private Series mSeries;
    private SimpleSeries mSimpleSeries;
    private List<Series> mSeriesList;
    private List<SimpleSeries> mSimpleSeriesList;
    private SeriesMapper mMapper;

    @Before
    public void init() {
        mMapper = new SeriesMapper();
        mSeries = FakeSeriesData.provideFakeSeries();
        mSeriesList = FakeSeriesData.provideFakeSeriesList();
        mSimpleSeries = FakeSimpleSeriesData.provideFakeSimpleSeries();
        mSimpleSeriesList = FakeSimpleSeriesData.provideFakeSimpleSeriesList();
    }

    @Test
    public void mapSeries_returnSimpleSeries() {

        SimpleSeries simpleSeries = mMapper.map(mSeries);

        assertEquals(simpleSeries.getId(), mSimpleSeries.getId());
        assertEquals(simpleSeries.getTitle(), mSimpleSeries.getTitle());
        assertEquals(simpleSeries.getOverview(), mSimpleSeries.getOverview());
        assertEquals(simpleSeries.getBackdropPath(), mSimpleSeries.getBackdropPath());
        assertEquals(simpleSeries.getPosterPath(), mSimpleSeries.getPosterPath());
        assertEquals(simpleSeries.getNumberOfSeasons(), mSimpleSeries.getNumberOfSeasons());
        assertEquals(simpleSeries.getVoteAverage(), mSimpleSeries.getVoteAverage(), 0.0);

        for (int i = 0; i<simpleSeries.getCast().size(); i++) {
            assertEquals(simpleSeries.getCast().get(i).getId(), mSimpleSeries.getCast().get(i).getId());
            assertEquals(simpleSeries.getCast().get(i).getName(), mSimpleSeries.getCast().get(i).getName());
            assertEquals(simpleSeries.getCast().get(i).getPosterPath(), mSimpleSeries.getCast().get(i).getPosterPath());
            assertEquals(simpleSeries.getCast().get(i).getCharacter(), mSimpleSeries.getCast().get(i).getCharacter());

        }

        for (int i = 0; i < simpleSeries.getGenres().size(); i++) {
            assertEquals(simpleSeries.getGenres().get(i).getId(), mSimpleSeries.getGenres().get(i).getId());
            assertEquals(simpleSeries.getGenres().get(i).getName(), mSimpleSeries.getGenres().get(i).getName());
        }
    }

    @Test
    public void mapSeriesList_returnSimpleSeriesList() {
        List<SimpleSeries> simpleSeriesList = mMapper.map(mSeriesList);

        for (int i = 0; i < mSeriesList.size(); i++) {
            assertEquals(simpleSeriesList.get(i).getId(), mSimpleSeriesList.get(i).getId());
            assertEquals(simpleSeriesList.get(i).getNumberOfSeasons(), mSimpleSeriesList.get(i).getNumberOfSeasons());
            assertEquals(simpleSeriesList.get(i).getGenreIds(), mSimpleSeriesList.get(i).getGenreIds());
            assertEquals(simpleSeriesList.get(i).getPosterPath(), mSimpleSeriesList.get(i).getPosterPath());
            assertEquals(simpleSeriesList.get(i).getOverview(), mSimpleSeriesList.get(i).getOverview());
            assertEquals(simpleSeriesList.get(i).getTitle(), mSimpleSeriesList.get(i).getTitle());
            assertEquals(simpleSeriesList.get(i).getVoteAverage(), mSimpleSeriesList.get(i).getVoteAverage(), 0.0);
        }
    }
}