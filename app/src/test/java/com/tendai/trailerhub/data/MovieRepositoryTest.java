package com.tendai.trailerhub.data;

import com.tendai.trailerhub.data.model.Movie;
import com.tendai.trailerhub.presentation.utils.Config;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class MovieRepositoryTest {
    @Mock
    private MovieDataSource mMovieDataSource;

    @Mock
    private CallBacks.GetMovieDetails mGetMovieDetails;

    @Mock
    private CallBacks.GetMovies mGetMovies;

    @Captor
    private ArgumentCaptor<CallBacks.GetMovieDetails> mGetMovieDetailsCaptor;

    @Captor
    private ArgumentCaptor<CallBacks.GetMovies> mGetMovieCaptor;

    private MovieRepository mMovieRepository;
    private List<Movie> mMoviesList;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mMovieRepository = MovieRepository.getInstance(mMovieDataSource);
        mMoviesList = FakeMovieData.provideFakeMovieList();
    }

    @Test
    public void getMovieByCategory_retrievesFromCacheIfCategoriesAreTheSame() {
        mMovieRepository.refreshMovies();
        mMovieRepository.getMovieByCategory(Config.POPULAR, 1, mGetMovies);

        verify(mMovieDataSource).getMovieByCategory(eq(Config.POPULAR), anyInt(), mGetMovieCaptor.capture());
        mGetMovieCaptor.getValue().onSuccess(FakeMovieData.provideFakeMovieList());

        mMovieRepository.getMovieByCategory(Config.POPULAR, 1, mGetMovies);
        verifyNoMoreInteractions(mMovieDataSource);
    }

    @Test
    public void getMovieByCategory_retrievesFromNetworkIfCategoriesDiffer() {
        mMovieRepository.refreshMovies();
        //when
        mMovieRepository.getMovieByCategory(Config.POPULAR, 1, mGetMovies);

        //then
        verify(mMovieDataSource).getMovieByCategory(anyString(), anyInt(), mGetMovieCaptor.capture());
        mGetMovieCaptor.getValue().onSuccess(FakeMovieData.provideFakeMovieList());
        assertEquals(mMovieRepository.cachedMoviesById.size(), 2);

        //when
        mMovieRepository.getMovieByCategory(Config.UPCOMING, 1, mGetMovies);

        //then
        verify(mMovieDataSource).getMovieByCategory(eq(Config.UPCOMING), anyInt(), mGetMovieCaptor.capture());

        //when
        mMovieRepository.getMovieByCategory(Config.POPULAR, 1, mGetMovies);

        //then
        verifyNoMoreInteractions(mMovieDataSource);

        //when
        mMovieRepository.getMovieByCategory(Config.UPCOMING, 1, mGetMovies);

        //then
        verifyNoMoreInteractions(mMovieDataSource);
        assertEquals(mMovieRepository.cachedMoviesById.size(), 2);
    }

    @Test
    public void getMovieByGenreId_retrievesFromCacheIfGenreIdIsTheSame() {
        mMovieRepository.refreshMovies();

        //when
        mMovieRepository.getMovieByGenre("28", 1, mGetMovies);

        //then
        verify(mMovieDataSource).getMovieByGenre(anyString(), anyInt(), mGetMovieCaptor.capture());
        mGetMovieCaptor.getValue().onSuccess(FakeMovieData.provideFakeMovieList());

        //when
        mMovieRepository.getMovieByGenre("28", 1, mGetMovies);

        //then
        verifyNoMoreInteractions(mMovieDataSource);
        assertEquals(mMovieRepository.cachedMoviesById.size(), 2);

    }

    @Test
    public void getMovieByGenreId_retrievesFromNetworkIfGenreIdIsDiffer() {
        mMovieRepository.refreshMovies();
        //when
        mMovieRepository.getMovieByGenre("28", 1, mGetMovies);

        //then
        verify(mMovieDataSource).getMovieByGenre(anyString(), anyInt(), mGetMovieCaptor.capture());
        mGetMovieCaptor.getValue().onSuccess(FakeMovieData.provideFakeMovieList());
        assertEquals(mMovieRepository.cachedMoviesById.size(), 2);

        //when
        mMovieRepository.getMovieByGenre("12", 1, mGetMovies);

        //then
        verify(mMovieDataSource).getMovieByGenre(eq("12"), anyInt(), mGetMovieCaptor.capture());

        //when
        mMovieRepository.getMovieByGenre("28", 1, mGetMovies);

        //then
        verifyNoMoreInteractions(mMovieDataSource);

        //when
        mMovieRepository.getMovieByGenre("12", 1, mGetMovies);

        //then
        verifyNoMoreInteractions(mMovieDataSource);
        assertEquals(mMovieRepository.cachedMoviesById.size(), 2);

    }

    @Test
    public void getMovieDetails_shouldRetrieveDetailsFromCache() {
        mMovieRepository.refreshMovies();
        mMovieRepository.getMovieDetails(23, mGetMovieDetails);

        verify(mMovieDataSource).getMovieDetails(anyInt(), mGetMovieDetailsCaptor.capture());
        mGetMovieDetailsCaptor.getValue().onSuccess(FakeMovieData.provideFakeMovie());
        verifyNoMoreInteractions(mMovieDataSource);
        assertEquals(mMovieRepository.cachedMoviesById.size(), 1);
    }

    @After
    public void destroyRepositoryInstance() {
        MovieRepository.destroyInstance();
    }
}