package com.tendai.trailerhub.data;

import com.tendai.trailerhub.data.model.Series;
import com.tendai.trailerhub.presentation.utils.Config;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class SeriesRepositoryTest {
    @Mock
    private SeriesDataSource mSeriesDataSource;

    @Mock
    private CallBacks.GetSeriesDetails mGetSeriesDetails;

    @Mock
    private CallBacks.GetSeries mGetSeries;

    @Captor
    private ArgumentCaptor<CallBacks.GetSeriesDetails> mGetSeriesDetailsCaptor;

    @Captor
    private ArgumentCaptor<CallBacks.GetSeries> mGetSeriesCaptor;

    private SeriesRepository mSeriesRepository;
    private List<Series> mSeriesList;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mSeriesRepository = SeriesRepository.getInstance(mSeriesDataSource);
        mSeriesList = FakeSeriesData.provideFakeSeriesList();
    }

    @Test
    public void getSeriesByCategory_shouldCacheResultsAfterSuccessfulApiCall() {
        mSeriesRepository.refreshSeries();
        mSeriesRepository.getSeriesByCategory(Config.UPCOMING, 1, mGetSeries);

        verify(mSeriesDataSource).getSeriesByCategory(eq(Config.UPCOMING), eq(1), mGetSeriesCaptor.capture());
        mGetSeriesCaptor.getValue().onSuccess(mSeriesList);

        verify(mGetSeries).onSuccess(eq(mSeriesList));
        verifyNoMoreInteractions(mSeriesDataSource);
        assertEquals(mSeriesRepository.cachedSeriesById.size(), 2);

    }

    @Test
    public void getSeriesByGenreId_shouldCacheResultsAfterSuccessfulApiCall() {
        mSeriesRepository.refreshSeries();
        mSeriesRepository.getSeriesByGenre("128", 1, mGetSeries);

        verify(mSeriesDataSource).getSeriesByGenre(anyString(), anyInt(), mGetSeriesCaptor.capture());
        mGetSeriesCaptor.getValue().onSuccess(mSeriesList);

        verify(mSeriesDataSource).getSeriesByGenre(anyString(), anyInt(), mGetSeriesCaptor.capture());
        verifyNoMoreInteractions(mSeriesDataSource);

        assertEquals(mSeriesRepository.cachedSeriesById.size(), 2);

    }

    @Test
    public void getSeriesDetails_shouldRetrieveDetailsFromCache() {
        mSeriesRepository.refreshSeries();
        mSeriesRepository.getSeriesDetails(23, mGetSeriesDetails);

        verify(mSeriesDataSource).getSeriesDetails(anyInt(), mGetSeriesDetailsCaptor.capture());
        mGetSeriesDetailsCaptor.getValue().onSuccess(FakeSeriesData.provideFakeSeries());
        verifyNoMoreInteractions(mSeriesDataSource);
        assertEquals(mSeriesRepository.cachedSeriesById.size(), 1);


    }

    @After
    public void destroyRepositoryInstance() {
        SeriesRepository.destroyInstance();
    }

}