package com.tendai.trailerhub.data;

import com.tendai.trailerhub.data.model.Both;
import com.tendai.trailerhub.data.model.Movie;
import com.tendai.trailerhub.presentation.utils.Config;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class RepositoryTest {
    private Repository mRepository;

    @Mock
    private DataSource mDataSource;

    @Mock
    private CallBacks.GetTrending mGetTrending;

    @Captor
    private ArgumentCaptor<CallBacks.GetTrending> mGetTrendingCaptor;

    private List<Movie> mMovies;

    @Before
    public void init() {
        initMocks(this);
        mRepository = Repository.getInstance(mDataSource);
        mMovies = FakeMovieData.provideFakeMovieList();
    }

    @After
    public void destroyRepositoryInstance() {
        Repository.destroyInstance();
    }

    @Test
    public void getTrending_shouldCacheResultsAfterSuccessfulCall() {
        //when
        mRepository.refreshTrending();
        mRepository.getTrending(Config.MEDIA_TYPE_MOVIE, Config.TIME_WINDOW, mGetTrending);

        verify(mDataSource).getTrending(eq(Config.MEDIA_TYPE_MOVIE),
                eq(Config.TIME_WINDOW),
                mGetTrendingCaptor.capture());

        mGetTrendingCaptor.getValue().onSuccess(mMovies);

        //For a success removing this line still yields a passing test.
        mRepository.getTrending(Config.MEDIA_TYPE_MOVIE, Config.TIME_WINDOW, mGetTrending);

        verify(mDataSource, times(1)).getTrending(eq(Config.MEDIA_TYPE_MOVIE),
                eq(Config.TIME_WINDOW),
                any(CallBacks.GetTrending.class));

        assertEquals(mRepository.cachedMovies.size(), 6);

    }

    @Test
    public void getTrending_shouldNotCacheResultsIfApiCallFails() {
        mRepository.refreshTrending();
        mRepository.getTrending(Config.MEDIA_TYPE_MOVIE, Config.TIME_WINDOW, mGetTrending);

        verify(mDataSource).getTrending(eq(Config.MEDIA_TYPE_MOVIE),
                eq(Config.TIME_WINDOW),
                mGetTrendingCaptor.capture());

        mGetTrendingCaptor.getValue().onFailure();

        //Still a bit confused but I kinda now have the hang of it.
        verify(mDataSource, times(1)).getTrending(eq(Config.MEDIA_TYPE_MOVIE),
                eq(Config.TIME_WINDOW),
                any(CallBacks.GetTrending.class));

        assertNull(mRepository.cachedMovies);
    }

}