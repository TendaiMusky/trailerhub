package com.tendai.trailerhub.presentation.search;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import com.tendai.trailerhub.R;
import com.tendai.trailerhub.presentation.BaseActivity;
import com.tendai.trailerhub.presentation.utils.ActivityUtils;

import java.util.Objects;

public class SearchableActivity extends BaseActivity {
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private SearchContract.Presenter mPresenter;
    private SearchFragment mSearchFragment;
    @SuppressWarnings("FieldCanBeLocal")
    private SearchView mSearchView;

    private final StringBuilder mSearchQuery = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = findViewById(R.id.topAppBar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(null);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        analytics.logEvent("SearchActivityLaunched", new Bundle());
        initSearchFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchable, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) menu.findItem(R.id.searchable_search).getActionView();
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        mSearchView.onActionViewExpanded();
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setIconifiedByDefault(true);

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            final SearchFragment searchFragment =
                    (SearchFragment) getSupportFragmentManager().findFragmentById(R.id.activity_search_fragmentContainer);

            @Override
            public boolean onQueryTextSubmit(String query) {
                assert searchFragment != null;
                searchFragment.startSearch(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() == 0) {
                    assert searchFragment != null;
                    searchFragment.showLoadingUi(false);
                } else {
                    mSearchQuery.append(newText);
                    assert searchFragment != null;
                    searchFragment.showLoadingUi(true);
                    searchFragment.startSearch(mSearchQuery.toString());
                }
                return true;
            }
        });
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initSearchFragment() {
        if (mSearchFragment == null) {
            mSearchFragment = SearchFragment.newInstance();
        }
        ActivityUtils.replaceFragmentInActivity(
                getSupportFragmentManager(),
                mSearchFragment,
                R.id.activity_search_fragmentContainer);
    }

}





