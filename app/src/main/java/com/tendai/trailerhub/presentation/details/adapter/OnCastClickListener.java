package com.tendai.trailerhub.presentation.details.adapter;

import com.tendai.trailerhub.domain.model.SimpleCast;

public interface OnCastClickListener {
    void onCastClick(SimpleCast cast);
}
