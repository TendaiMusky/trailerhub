package com.tendai.trailerhub.presentation.details.adapter;

import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.tendai.trailerhub.R;
import com.tendai.trailerhub.domain.model.SimpleCast;
import com.tendai.trailerhub.presentation.utils.Config;

class CastViewHolder extends RecyclerView.ViewHolder {

    private ImageView mCastPoster;
    private TextView mCastRealName;
    private TextView mCastCharacter;
    private LinearLayout mExpandableLayout;
    private ObjectAnimator mAnimation;

    CastViewHolder(@NonNull View itemView) {
        super(itemView);

        mCastPoster = itemView.findViewById(R.id.item_cast_image_poster);
        mCastRealName = itemView.findViewById(R.id.item_cast_text_actor_name);
        mCastCharacter = itemView.findViewById(R.id.item_cast_text_actor_character);
        mExpandableLayout = itemView.findViewById(R.id.item_cast_expandable_layout);

        ObjectAnimator animator = ObjectAnimator.ofFloat(
                itemView,
                View.ALPHA,
                1f, 0f, 1f);
        animator.setRepeatCount(ObjectAnimator.INFINITE);
        animator.setDuration(3000L);
        mAnimation = animator;
    }

    void showPlaceHolderAnimation() {
        ObjectAnimator animator = mAnimation;
        animator.setCurrentPlayTime((SystemClock.elapsedRealtime() - (long) this.getAdapterPosition() * 30) % 100L);
        animator.start();
        mCastPoster.setImageResource(R.drawable.image_trending_placeholder);
        mCastRealName.setText(null);
        mCastCharacter.setText(null);
        mCastRealName.setBackgroundResource(R.drawable.text_title_placeholder);
        mCastCharacter.setBackgroundResource(R.drawable.text_title_placeholder);
    }

    void bindData(SimpleCast cast) {
        mAnimation.end();
        Glide.with(mCastPoster)
                .asBitmap()
                .load(Config.IMAGE_POSTER_URL + cast.getPosterPath())
                .into(new BitmapImageViewTarget(mCastPoster) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        mCastPoster.setScaleType(ImageView.ScaleType.FIT_XY);
                        mCastPoster.setImageResource(R.drawable.ic_place_holder);
                    }
                });
        mCastRealName.setText(cast.getName());
        mCastCharacter.setText(cast.getCharacter());
    }

    LinearLayout getExpandableLayout() {
        return mExpandableLayout;
    }
}
