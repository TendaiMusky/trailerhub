package com.tendai.trailerhub.presentation.trailer;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tendai.trailerhub.R;
import com.tendai.trailerhub.presentation.utils.Config;

import java.util.Objects;

public class TrailerActivity extends YouTubeFailureRecoveryActivity implements YouTubePlayer.OnFullscreenListener {
    static final String YOUTUBE_API_KEY = "AIzaSyC6u-rFErG-jQ3kFfv9cjPK20q7GEmy1ZE";
    private YouTubePlayerView mYouTubePlayerView;
    private String mVideoId;
    private boolean fullscreen;

    private AdView mAdView;
    private AdRequest adRequest;
    private boolean isLandScape;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trailer);

        boolean isPlayServicesAvailable = checkPlayServices();

        if (!isPlayServicesAvailable) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_play_services_unrecoverable), Toast.LENGTH_LONG).show();
            finish();
        }


        mVideoId = getIntent().getStringExtra(Config.EXTRA_TRAILER_ID);
        mYouTubePlayerView = findViewById(R.id.activity_trailer_youtube_player_view);
        mYouTubePlayerView.initialize(YOUTUBE_API_KEY, this);
        doLayout();

        isLandScape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;

        if (!isLandScape) {
            mAdView = findViewById(R.id.adView);
            adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }

        FirebaseAnalytics.getInstance(this).logEvent("TrailerActivityLaunched", new Bundle());
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability gApi = GoogleApiAvailability.getInstance();
        int resultCode = gApi.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (gApi.isUserResolvableError(resultCode)) {
                Objects.requireNonNull(gApi.getErrorDialog(this, resultCode, R.string.get_play_services)).show();
            }
            return false;
        }
        return true;
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return mYouTubePlayerView;
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        youTubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
        youTubePlayer.setOnFullscreenListener(this);

        if (mVideoId == null) {
            Toast.makeText(getApplicationContext(), R.string.no_trailer, Toast.LENGTH_LONG).show();
            finish();
        }

        if (!b) {
            if (mVideoId == null) {
                return;
            }
            youTubePlayer.cueVideo(mVideoId);
        }
    }

    @Override
    public void onFullscreen(boolean b) {
        fullscreen = b;
        doLayout();
    }

    private void doLayout() {
        ConstraintLayout.LayoutParams playerParams =
                (ConstraintLayout.LayoutParams) mYouTubePlayerView.getLayoutParams();
        if (fullscreen) {
            // When in fullscreen, the visibility of all other views than the player should be set to
            // GONE and the player should be laid out across the whole screen.
            playerParams.width = ConstraintLayout.LayoutParams.MATCH_PARENT;
            playerParams.height = ConstraintLayout.LayoutParams.MATCH_PARENT;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        doLayout();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!isLandScape) {
            mAdView.destroy();
        }
    }
}
