package com.tendai.trailerhub.presentation.series.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tendai.trailerhub.R;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;

import java.util.List;
import java.util.Objects;

public class SeriesAdapter extends RecyclerView.Adapter<SeriesViewHolder> {
    private List<SimpleSeries> mSeries;
    private SeriesItemClickListener mItemClickListener;
    boolean mIsTablet;
    boolean mIsLandScape;

    public SeriesAdapter(List<SimpleSeries> series, SeriesItemClickListener itemClickListener, boolean isTablet, boolean isLandScape) {
        setSeries(series);
        mItemClickListener = itemClickListener;
        mIsTablet = isTablet;
        mIsLandScape = isLandScape;
    }

    @NonNull
    @Override
    public SeriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie_series_name_poster, parent, false);

        SeriesViewHolder holder = new SeriesViewHolder(itemView, mIsTablet, mIsLandScape);
        holder.itemView.setOnClickListener(view -> mItemClickListener.onSeriesClick((SimpleSeries) view.getTag()));

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull SeriesViewHolder holder, int position) {
        if (mSeries.size() != 0) {
            SimpleSeries series = mSeries.get(position);
            if (series == null) {
                holder.showPlaceHolderAnimation();
            } else {
                holder.itemView.setTag(series);
                holder.bindData(series);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mSeries.size();
    }

    public void setSeries(List<SimpleSeries> series) {
        mSeries = Objects.requireNonNull(series);
        notifyDataSetChanged();
    }

    public static class PlaceHolderAdapter extends RecyclerView.Adapter<SeriesViewHolder> {

        @NonNull
        @Override
        public SeriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_movie_series_name_poster, parent, false);

            return new SeriesViewHolder(itemView, false, false);
        }

        @Override
        public void onBindViewHolder(@NonNull SeriesViewHolder holder, int position) {
            holder.showPlaceHolderAnimation();
        }

        @Override
        public int getItemCount() {
            return 20;
        }
    }
}

// TODO: 9/10/2021 Fix Bug With Portrait * Tablet