package com.tendai.trailerhub.presentation.search.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tendai.trailerhub.R;
import com.tendai.trailerhub.domain.movieseries.model.Search;

public class SearchAdapter extends ArrayAdapter<Search> {

    public SearchAdapter(@NonNull Context context) {
        super(context, 0);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Search search = getItem(position);
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();

        }
        assert search != null;
        holder.mTitle.setText(search.getTitle());
        return convertView;
    }


    private static class ViewHolder {
        TextView mTitle;

        ViewHolder(View itemView) {
            mTitle = itemView.findViewById(R.id.item_search_title);

        }
    }
}
