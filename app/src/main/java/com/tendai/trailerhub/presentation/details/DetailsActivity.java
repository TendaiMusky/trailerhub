package com.tendai.trailerhub.presentation.details;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.tendai.trailerhub.R;
import com.tendai.trailerhub.domain.model.SimpleVideos;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;
import com.tendai.trailerhub.presentation.BaseActivity;
import com.tendai.trailerhub.presentation.trailer.TrailerActivity;
import com.tendai.trailerhub.presentation.utils.ActivityUtils;
import com.tendai.trailerhub.presentation.utils.Config;

import java.util.Objects;

public class DetailsActivity extends BaseActivity implements AppBarLayout.OnOffsetChangedListener,
        DetailsFragment.Callback {
    private static final String TAG = DetailsActivity.class.getSimpleName();

    private DetailsFragment mDetailsFragment;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private boolean mIsToolBarTitleDisplayed;
    private int mScrollRange = -1;
    private String mMovieSeriesName;
    private ImageView mBackDrop;
    private String mVideoId;
    private ImageButton mPlayTrailerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Toolbar toolbar = findViewById(R.id.topAppBar);
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setTitle(null);
        mPlayTrailerButton = findViewById(R.id.activity_movie_series_details_button_play_trailer);
        mBackDrop = findViewById(R.id.activity_movie_series_details_image_backdrop);

        initCollapsingToolbar();

        String title = getIntent().getStringExtra(Config.EXTRA_TITLE);
        int movieId = getIntent().getIntExtra(Config.EXTRA_MOVIE_ID, 0);
        int seriesId = getIntent().getIntExtra(Config.EXTRA_SERIES_ID, 0);

        if (title.equals(Config.MOVIE)) {
            initDetailsFragment(title, movieId);
        } else if (title.equals(Config.SERIES)) {
            initDetailsFragment(title, seriesId);
        }

        mPlayTrailerButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, TrailerActivity.class);
            intent.putExtra(Config.EXTRA_TRAILER_ID, mVideoId);
            startActivity(intent);
        });

        analytics.logEvent("DetailsActivityLaunched", new Bundle());
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (mScrollRange == -1) {
            mScrollRange = appBarLayout.getTotalScrollRange();
        }
        if (mScrollRange + verticalOffset == 0) {
            mCollapsingToolbarLayout.setTitle(mMovieSeriesName);
            mIsToolBarTitleDisplayed = true;
        } else if (mIsToolBarTitleDisplayed) {
            mCollapsingToolbarLayout.setTitle(null);
            mIsToolBarTitleDisplayed = false;
        }
    }

    @Override
    public void setAdditionalMovieDetails(SimpleMovie movie) {
        mMovieSeriesName = movie.getTitle();

        Glide.with(mBackDrop)
                .asBitmap()
                .load(Config.IMAGE_BACKDROP_URL + movie.getBackdropPath())
                .into(new BitmapImageViewTarget(mBackDrop) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        mBackDrop.setImageResource(R.drawable.ic_place_holder);
                    }
                });

        if (movie.getVideos() != null) {
            for (SimpleVideos videos : movie.getVideos()) {
                if (videos.getSite().equalsIgnoreCase("Youtube")
                        && videos.getType().equalsIgnoreCase("Trailer")) {
                    mVideoId = videos.getKey();
                    break;
                }
            }
        }
        mPlayTrailerButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void setAdditionalSeriesDetails(SimpleSeries series) {
        mMovieSeriesName = series.getTitle();

        Glide.with(mBackDrop)
                .asBitmap()
                .load(Config.IMAGE_BACKDROP_URL + series.getBackdropPath())
                .into(new BitmapImageViewTarget(mBackDrop) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        mBackDrop.setScaleType(ImageView.ScaleType.FIT_XY);
                        mBackDrop.setImageResource(R.drawable.ic_place_holder);
                    }
                });

        if (series.getVideos() != null) {
            for (SimpleVideos video : series.getVideos()) {
                if (video.getSite().equalsIgnoreCase("Youtube")
                        && video.getType().equalsIgnoreCase("Trailer")) {
                    mVideoId = video.getKey();
                    break;
                }
            }
        }
        mPlayTrailerButton.setVisibility(View.VISIBLE);
    }

    private void initCollapsingToolbar() {
        mCollapsingToolbarLayout = findViewById(R.id.activity_details_collapsing_toolbar_layout);
        AppBarLayout appBarLayout = findViewById(R.id.activity_details_app_bar_layout);

        if (isTablet && isLandScape) {
            appBarLayout.setExpanded(false);
        } else {
            appBarLayout.setExpanded(true);
            appBarLayout.addOnOffsetChangedListener(this);
        }
    }

    private void initDetailsFragment(String title, int id) {
        if (mDetailsFragment == null) {
            mDetailsFragment = DetailsFragment.newInstance(title, id);
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(),
                    mDetailsFragment,
                    R.id.activity_details_fragment_container);
        } else {
            ActivityUtils.replaceFragmentInActivity(
                    getSupportFragmentManager(),
                    mDetailsFragment,
                    R.id.activity_details_fragment_container);
        }
    }
}

// TODO: 9/9/2021 Fix what happens when user scrolls down in tablet*landscape mode
// TODO: 9/10/2021 Use proper settings fragment in MoreFragment