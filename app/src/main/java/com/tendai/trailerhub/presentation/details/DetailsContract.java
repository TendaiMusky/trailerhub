package com.tendai.trailerhub.presentation.details;

import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;
import com.tendai.trailerhub.presentation.BasePresenter;
import com.tendai.trailerhub.presentation.BaseView;

public interface DetailsContract {

    interface View extends BaseView<Presenter> {
        boolean isActive();

        void showLoadingUi(boolean isActive);

        void showMovieDetails(SimpleMovie movie);

        void showErrorMessage();

        void showSeriesDetails(SimpleSeries series);
    }

    interface Presenter extends BasePresenter {

    }
}
