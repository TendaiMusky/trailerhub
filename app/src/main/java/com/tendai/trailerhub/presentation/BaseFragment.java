package com.tendai.trailerhub.presentation;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.DisplayMetrics;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.analytics.FirebaseAnalytics;

public abstract class BaseFragment extends Fragment {
    protected Callback mCallback;
    protected boolean isTablet;
    protected boolean isLandScape;
    protected FirebaseAnalytics analytics;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        isTablet = (metrics.widthPixels / metrics.density) >= 600;
        isLandScape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        analytics = FirebaseAnalytics.getInstance(requireActivity());
    }

    public interface Callback {
        void displayErrorFragment();

        void removeErrorFragment();

        void displayDetailsFragment(int id, String title);

        void displayAboutFragment();

        void displayCreditsFragment();
    }
}
