package com.tendai.trailerhub.presentation.series;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.ChipGroup;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tendai.trailerhub.Injection;
import com.tendai.trailerhub.R;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;
import com.tendai.trailerhub.presentation.BaseFragment;
import com.tendai.trailerhub.presentation.details.DetailsActivity;
import com.tendai.trailerhub.presentation.home.meta.SeriesGenre;
import com.tendai.trailerhub.presentation.series.adapter.SeriesAdapter;
import com.tendai.trailerhub.presentation.series.adapter.SeriesItemClickListener;
import com.tendai.trailerhub.presentation.utils.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SeriesFragment extends BaseFragment implements SeriesContract.View,
        SeriesItemClickListener, ChipGroup.OnCheckedChangeListener {

    private SeriesContract.Presenter mPresenter;
    private SeriesAdapter mPopularAdapter;
    private SeriesAdapter mUpcomingAdapter;
    private SeriesAdapter mTopRatedAdapter;
    private SeriesAdapter.PlaceHolderAdapter mPopularPlaceHolder;
    private SeriesAdapter.PlaceHolderAdapter mTopRatedPlaceHolder;
    private SeriesAdapter.PlaceHolderAdapter mUpcomingPlaceHolder;
    private SeriesAdapter mFamilyAdapter;
    private SeriesAdapter mActionAdventureAdapter;
    private SeriesAdapter mSciFiAdapter;
    private SeriesAdapter mMainAdapter;
    private SeriesAdapter.PlaceHolderAdapter mFamilyPlaceHolder;
    private SeriesAdapter.PlaceHolderAdapter mActionAdventurePlaceHolder;
    private SeriesAdapter.PlaceHolderAdapter mSciFiPlaceHolder;
    private SeriesAdapter.PlaceHolderAdapter mMainPlaceHolder;

    private RecyclerView mRecyclerViewPopular;
    private RecyclerView mRecyclerViewOnTheAir;
    private RecyclerView mRecyclerViewTopRated;
    private RecyclerView mRecyclerViewFamily;
    private RecyclerView mRecyclerViewSciFi;
    private RecyclerView mRecyclerViewActionAdventure;
    private RecyclerView mMainRecyclerView;

    public static SeriesFragment newInstance() {
        return new SeriesFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            mCallback = (Callback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement Callback Interface");
        }
    }

    @Override
    public void setPresenter(SeriesContract.Presenter presenter) {
        mPresenter = Objects.requireNonNull(presenter);
    }

    @Override
    public boolean checkConnection() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) requireContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return connectivityManager.getActiveNetworkInfo() != null &&
                connectivityManager.getActiveNetworkInfo().isConnected();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new SeriesPresenter(
                Injection.provideUseCaseHandler(),
                Injection.provideGetSeries(requireActivity()),
                this);

        if (!isTablet) {
            mPopularAdapter = new SeriesAdapter(new ArrayList<>(0), this, isTablet, isLandScape);
            mTopRatedAdapter = new SeriesAdapter(new ArrayList<>(0), this, isTablet, isLandScape);
            mUpcomingAdapter = new SeriesAdapter(new ArrayList<>(0), this, isTablet, isLandScape);

            mPopularPlaceHolder = new SeriesAdapter.PlaceHolderAdapter();
            mTopRatedPlaceHolder = new SeriesAdapter.PlaceHolderAdapter();
            mUpcomingPlaceHolder = new SeriesAdapter.PlaceHolderAdapter();
        } else if (!isLandScape) {

            mPopularAdapter = new SeriesAdapter(new ArrayList<>(0), this, isTablet, isLandScape);
            mTopRatedAdapter = new SeriesAdapter(new ArrayList<>(0), this, isTablet, isLandScape);
            mUpcomingAdapter = new SeriesAdapter(new ArrayList<>(0), this, isTablet, isLandScape);
            mFamilyAdapter = new SeriesAdapter(new ArrayList<>(0), this, isTablet, isLandScape);
            mActionAdventureAdapter = new SeriesAdapter(new ArrayList<>(0), this, isTablet, isLandScape);
            mSciFiAdapter = new SeriesAdapter(new ArrayList<>(0), this, isTablet, isLandScape);

            mPopularPlaceHolder = new SeriesAdapter.PlaceHolderAdapter();
            mTopRatedPlaceHolder = new SeriesAdapter.PlaceHolderAdapter();
            mUpcomingPlaceHolder = new SeriesAdapter.PlaceHolderAdapter();
            mFamilyPlaceHolder = new SeriesAdapter.PlaceHolderAdapter();
            mActionAdventurePlaceHolder = new SeriesAdapter.PlaceHolderAdapter();
            mSciFiPlaceHolder = new SeriesAdapter.PlaceHolderAdapter();
        } else {
            mMainAdapter = new SeriesAdapter(new ArrayList<>(0), this, isTablet, isLandScape);
            mMainPlaceHolder = new SeriesAdapter.PlaceHolderAdapter();
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_series, container, false);

        if (!isTablet) {
            mRecyclerViewPopular = view.findViewById(R.id.fragment_series_recycler_view_popular_this_week);
            mRecyclerViewOnTheAir = view.findViewById(R.id.fragment_series_recycler_view_on_the_air);
            mRecyclerViewTopRated = view.findViewById(R.id.fragment_series_recycler_view_top_rated);

            mRecyclerViewPopular.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewPopular.setAdapter(mPopularPlaceHolder);
            mRecyclerViewPopular.setHasFixedSize(true);

            mRecyclerViewOnTheAir.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewOnTheAir.setAdapter(mUpcomingPlaceHolder);
            mRecyclerViewOnTheAir.setHasFixedSize(true);

            mRecyclerViewTopRated.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewTopRated.setAdapter(mTopRatedPlaceHolder);
            mRecyclerViewTopRated.setHasFixedSize(true);
        } else if (!isLandScape) {
            mRecyclerViewPopular = view.findViewById(R.id.fragment_series_recycler_view_popular_this_week);
            mRecyclerViewOnTheAir = view.findViewById(R.id.fragment_series_recycler_view_on_the_air);
            mRecyclerViewTopRated = view.findViewById(R.id.fragment_series_recycler_view_top_rated);
            mRecyclerViewActionAdventure = view.findViewById(R.id.fragment_series_recycler_view_action_and_adventure);
            mRecyclerViewSciFi = view.findViewById(R.id.fragment_series_recycler_view_sci_fi_and_fantasy);
            mRecyclerViewFamily = view.findViewById(R.id.fragment_series_recycler_view_family);

            mRecyclerViewPopular.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewPopular.setAdapter(mPopularPlaceHolder);
            mRecyclerViewPopular.setHasFixedSize(true);

            mRecyclerViewOnTheAir.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewOnTheAir.setAdapter(mUpcomingPlaceHolder);
            mRecyclerViewOnTheAir.setHasFixedSize(true);

            mRecyclerViewTopRated.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewTopRated.setAdapter(mTopRatedPlaceHolder);
            mRecyclerViewTopRated.setHasFixedSize(true);
            //
            mRecyclerViewActionAdventure.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewActionAdventure.setAdapter(mActionAdventurePlaceHolder);
            mRecyclerViewActionAdventure.setHasFixedSize(true);

            mRecyclerViewSciFi.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewSciFi.setAdapter(mSciFiPlaceHolder);
            mRecyclerViewSciFi.setHasFixedSize(true);

            mRecyclerViewFamily.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewFamily.setAdapter(mFamilyPlaceHolder);
            mRecyclerViewFamily.setHasFixedSize(true);

        } else {
            mMainRecyclerView = view.findViewById(R.id.activity_main_fragment_series_recycler_view);
            ChipGroup mChipGroup = view.findViewById(R.id.fragment_series_chipGroup);

            mChipGroup.setOnCheckedChangeListener(this);

            mMainRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.VERTICAL,
                    false));
            mMainRecyclerView.setAdapter(mMainPlaceHolder);
            mMainRecyclerView.setHasFixedSize(true);
        }


        if (!mPresenter.checkConnection()) {
            mCallback.displayErrorFragment();
        }

        analytics.logEvent("SearchFragmentLaunched", new Bundle());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void showLoadingUi(String category, boolean isActive) {
        if (!isActive) {
            switch (category) {
                case Config.POPULAR:
                    if (isTablet && isLandScape) {
                        if (mMainRecyclerView.getAdapter() != mMainAdapter) {
                            mMainRecyclerView.setAdapter(mMainAdapter);
                        }
                    } else {
                        if (mRecyclerViewPopular.getAdapter() != mPopularAdapter) {
                            mRecyclerViewPopular.setAdapter(mPopularAdapter);
                        }
                    }
                    break;
                case Config.ON_THE_AIR:
                    if (isTablet && isLandScape) {
                        if (mMainRecyclerView.getAdapter() != mMainAdapter) {
                            mMainRecyclerView.setAdapter(mMainAdapter);
                        }
                    } else {
                        if (mRecyclerViewOnTheAir.getAdapter() != mUpcomingAdapter) {
                            mRecyclerViewOnTheAir.setAdapter(mUpcomingAdapter);
                        }
                    }
                    break;
                case Config.TOP_RATED:
                    if (isTablet && isLandScape) {
                        if (mMainRecyclerView.getAdapter() != mMainAdapter) {
                            mMainRecyclerView.setAdapter(mMainAdapter);
                        }
                    } else {
                        if (mRecyclerViewTopRated.getAdapter() != mTopRatedAdapter) {
                            mRecyclerViewTopRated.setAdapter(mTopRatedAdapter);
                        }
                    }
                    break;
                case SeriesGenre.SCI_FI:
                    if (mRecyclerViewSciFi.getAdapter() != mSciFiAdapter) {
                        mRecyclerViewSciFi.setAdapter(mSciFiAdapter);
                    }
                    break;
                case SeriesGenre.FAMILY:
                    if (mRecyclerViewFamily.getAdapter() != mFamilyAdapter) {
                        mRecyclerViewFamily.setAdapter(mFamilyAdapter);
                    }
                    break;
                case SeriesGenre.ACTION_ADVENTURE:
                    if (mRecyclerViewActionAdventure.getAdapter() != mActionAdventureAdapter) {
                        mRecyclerViewActionAdventure.setAdapter(mActionAdventureAdapter);
                    }
                    break;
            }
        }
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void showSeries(List<SimpleSeries> seriesList, String category) {
        switch (category) {
            case Config.POPULAR:
                if (isTablet && isLandScape) {
                    mMainAdapter.setSeries(seriesList);
                } else {
                    mPopularAdapter.setSeries(seriesList);
                    mPresenter.retrieveOnTheAir();
                }
                break;
            case Config.ON_THE_AIR:
                if (isTablet && isLandScape) {
                    mMainAdapter.setSeries(seriesList);
                } else {
                    mUpcomingAdapter.setSeries(seriesList);
                    mPresenter.retrieveTopRated();
                }
                break;
            case Config.TOP_RATED:
                if (isTablet && isLandScape) {
                    mMainAdapter.setSeries(seriesList);
                } else {
                    mTopRatedAdapter.setSeries(seriesList);
                    if (isTablet && !isLandScape) {
                        mPresenter.retrieveFamily();
                    }
                }
                break;
            case SeriesGenre.FAMILY:
                mFamilyAdapter.setSeries(seriesList);
                if (isTablet && !isLandScape) {
                    mPresenter.retrieveActionAdventure();
                }
                break;
            case SeriesGenre.ACTION_ADVENTURE:
                mActionAdventureAdapter.setSeries(seriesList);
                if (isTablet && !isLandScape) {
                    mPresenter.retrieveSciFi();
                }
                break;
            case SeriesGenre.SCI_FI:
                mSciFiAdapter.setSeries(seriesList);
                break;
        }
    }

    @Override
    public void showErrorMessage() {
        if (requireActivity().getLifecycle().getCurrentState() == Lifecycle.State.RESUMED) {
            mCallback.displayErrorFragment();
        }
    }

    @Override
    public void showSeriesDetails(SimpleSeries series) {
        if (series == null) return;

        if (isTablet && isLandScape) {
            mCallback.displayDetailsFragment(series.getId(), Config.SERIES);
        } else {
            Intent intent = new Intent(getActivity(), DetailsActivity.class);
            intent.putExtra(Config.EXTRA_SERIES_ID, series.getId());
            intent.putExtra(Config.EXTRA_TITLE, Config.SERIES);
            startActivity(intent);
        }
    }

    @Override
    public void onSeriesClick(SimpleSeries series) {

        Bundle bundle = new Bundle();
        bundle.putLong(FirebaseAnalytics.Param.ITEM_ID, series.getId());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, series.getTitle());
        analytics.logEvent("SeriesSelected", bundle);

        mPresenter.showSeriesDetails(series);
    }

    @Override
    public void onCheckedChanged(ChipGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.activity_main_fragment_series_popular_chip:
                mPresenter.start();
                break;
            case R.id.activity_main_fragment_series_on_the_air_chip:
                mPresenter.retrieveOnTheAir();
                break;
            case R.id.activity_main_fragment_top_rated_chip:
                mPresenter.retrieveTopRated();
                break;
        }
    }
}
