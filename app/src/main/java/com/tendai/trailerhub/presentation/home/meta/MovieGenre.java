package com.tendai.trailerhub.presentation.home.meta;

public final class MovieGenre {
    public static final String DRAMA = "18";
    public static final String ACTION = "28";
    public static final String ANIMATION = "16";
    public static final String COMEDY = "35";
    public static final String FANTASY = "14";

    private MovieGenre() {
    }
}
