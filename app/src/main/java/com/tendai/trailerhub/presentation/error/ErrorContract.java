package com.tendai.trailerhub.presentation.error;

import com.tendai.trailerhub.presentation.BasePresenter;
import com.tendai.trailerhub.presentation.BaseView;

public interface ErrorContract {
    interface View extends BaseView<Presenter> {
        void removeErrorFragment();
    }

    interface Presenter extends BasePresenter {
        void removeErrorFragment();
    }
}
