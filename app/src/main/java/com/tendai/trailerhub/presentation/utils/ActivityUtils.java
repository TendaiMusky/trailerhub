package com.tendai.trailerhub.presentation.utils;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.Objects;

public class ActivityUtils {

    @SuppressLint("NewApi")
    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment,
                                             int frameId) {

        Objects.requireNonNull(fragmentManager, "Fragment Manager Cannot Be Null");
        Objects.requireNonNull(fragment, "Fragment Cannot Be Null");
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment);
        transaction.commit();
    }

    @SuppressLint("NewApi")
    public static void replaceFragmentInActivity(@NonNull FragmentManager fragmentManager,
                                                 @NonNull Fragment fragment,
                                                 int frameId) {

        Objects.requireNonNull(fragmentManager, "Fragment Manager Cannot Be Null");
        Objects.requireNonNull(fragment, "Fragment Cannot Be Null");
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(frameId, fragment);
        transaction.commit();
    }

    @SuppressLint("NewApi")
    public static void removeFragmentInActivity(@NonNull FragmentManager fragmentManager,
                                                @NonNull Fragment fragment) {
        Objects.requireNonNull(fragmentManager, "Fragment Manager Cannot Be Null");
        Objects.requireNonNull(fragment, "Fragment Cannot Be Null");
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.remove(fragment);
        transaction.commit();
    }
}
