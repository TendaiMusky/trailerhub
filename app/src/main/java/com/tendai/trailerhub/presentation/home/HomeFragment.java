package com.tendai.trailerhub.presentation.home;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.chip.ChipGroup;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tendai.trailerhub.Injection;
import com.tendai.trailerhub.R;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;
import com.tendai.trailerhub.presentation.BaseFragment;
import com.tendai.trailerhub.presentation.details.DetailsActivity;
import com.tendai.trailerhub.presentation.home.adapter.TrendingAdapter;
import com.tendai.trailerhub.presentation.home.meta.MovieGenre;
import com.tendai.trailerhub.presentation.home.meta.SeriesGenre;
import com.tendai.trailerhub.presentation.movie.adapter.MovieAdapter;
import com.tendai.trailerhub.presentation.movie.adapter.MovieItemClickListener;
import com.tendai.trailerhub.presentation.series.adapter.SeriesAdapter;
import com.tendai.trailerhub.presentation.series.adapter.SeriesItemClickListener;
import com.tendai.trailerhub.presentation.utils.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HomeFragment extends BaseFragment implements
        MovieItemClickListener,
        SeriesItemClickListener,
        ChipGroup.OnCheckedChangeListener,
        HomeContract.View {

    private static final String CHECKED_MOVIE_CHIP = "CHECKED_MOVIE_CHIP";
    private static final String CHECKED_SERIES_CHIP = "CHECKED_SERIES_CHIP";
    private static final String TAG = HomeFragment.class.getSimpleName();

    private MovieAdapter mMovieAdapter;
    private MovieAdapter.PlaceHolderAdapter mMoviePlaceHolderAdapter;
    private SeriesAdapter mSeriesAdapter;
    private SeriesAdapter.PlaceHolderAdapter mSeriesPlaceHolderAdapter;
    private HomeContract.Presenter mPresenter;
    private TrendingAdapter mTrendingAdapter;
    private TrendingAdapter.PlaceHolderAdapter mTrendingPlaceHolderAdapter;
    private MovieAdapter mDramaticMoviesAdapter;
    private MovieAdapter.PlaceHolderAdapter mDramaticPlaceHolder;
    private SeriesAdapter mExcitingShowsAdapter;
    private SeriesAdapter.PlaceHolderAdapter mExcitingShowsPlaceHolder;
    private MovieAdapter mActionMoviesAdapter;
    private MovieAdapter.PlaceHolderAdapter maActionPlaceHolder;
    private SeriesAdapter mCrimeShowsAdapter;
    private SeriesAdapter.PlaceHolderAdapter mCrimeShowsPlaceHolder;
    private MovieAdapter mAnimationMoviesAdapter;
    private MovieAdapter.PlaceHolderAdapter mAnimationPlaceHolder;

    private ViewPager2.PageTransformer mAnimator;

    private RecyclerView mSeriesRecyclerView;
    private RecyclerView mMovieRecyclerView;
    private RecyclerView mDramaticMoviesRecyclerView;
    private RecyclerView mExcitingShowsRecyclerView;
    private RecyclerView mActionMoviesRecyclerview;
    private RecyclerView mCrimeShowsRecyclerView;
    private RecyclerView mAnimationMoviesRecyclerView;
    private RecyclerView mMainRecyclerView;
    private ChipGroup mMovieChipGroup;
    private ChipGroup mSeriesChipGroup;
    private ViewPager2 mViewPager2;

    private int mSavedSeriesChipId;
    private int mSaveMovieChipId;
    private boolean mSavedSeriesChipRestored;
    private boolean mSavedMovieChipRestored;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            mCallback = (Callback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement Callback Interface");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new HomePresenter(
                Injection.provideUseCaseHandler(),
                Injection.provideGetTrending(requireActivity()),
                Injection.provideGetSeries(requireActivity()),
                Injection.provideGetMovie(requireActivity()),
                this);


        if (!isTablet) {
            mMovieAdapter = new MovieAdapter(new ArrayList<>(0), this, isTablet, isLandScape);
            mSeriesAdapter = new SeriesAdapter(new ArrayList<>(0), this, isTablet, isLandScape);

            mMoviePlaceHolderAdapter = new MovieAdapter.PlaceHolderAdapter();
            mSeriesPlaceHolderAdapter = new SeriesAdapter.PlaceHolderAdapter();
        } else if (!isLandScape) {
            mDramaticMoviesAdapter = new MovieAdapter(new ArrayList<>(0), this, isTablet, isLandScape);
            mExcitingShowsAdapter = new SeriesAdapter(new ArrayList<>(0), this, isTablet, isLandScape);
            mActionMoviesAdapter = new MovieAdapter(new ArrayList<>(0), this, isTablet, isLandScape);
            mCrimeShowsAdapter = new SeriesAdapter(new ArrayList<>(0), this, isTablet, isLandScape);
            mAnimationMoviesAdapter = new MovieAdapter(new ArrayList<>(0), this, isTablet, isLandScape);

            mDramaticPlaceHolder = new MovieAdapter.PlaceHolderAdapter();
            mExcitingShowsPlaceHolder = new SeriesAdapter.PlaceHolderAdapter();
            maActionPlaceHolder = new MovieAdapter.PlaceHolderAdapter();
            mCrimeShowsPlaceHolder = new SeriesAdapter.PlaceHolderAdapter();
            mAnimationPlaceHolder = new MovieAdapter.PlaceHolderAdapter();
        }
        mTrendingAdapter = new TrendingAdapter(new ArrayList<>(0), this, isTablet, isLandScape);
        mTrendingPlaceHolderAdapter = new TrendingAdapter.PlaceHolderAdapter();

        mAnimator = (view, i) -> {
            float absPos = Math.abs(i);
            float scale = (absPos > 1) ? 0F : 1 - absPos;
            view.setScaleX(scale);
            view.setScaleY(scale);
        };
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        if (!isTablet) {
            mViewPager2 = view.findViewById(R.id.layout_trending_viewPager2);
            TabLayout tabLayout = view.findViewById(R.id.layout_trending_tabs);

            mViewPager2.setAdapter(mTrendingPlaceHolderAdapter);
            mViewPager2.setPageTransformer(mAnimator);

            new TabLayoutMediator(tabLayout, mViewPager2, (tab, position) -> {
                tab.parent.setTabIconTintResource(R.color.color_tab_indicator_state_list);
                tab.setIcon(R.drawable.ic_tab_indicator);
            }).attach();

            mMovieRecyclerView = view.findViewById(R.id.fragment_home_drama_movies_recycler_view);
            mSeriesRecyclerView = view.findViewById(R.id.fragment_home_series_recycler_view);
            mMovieChipGroup = view.findViewById(R.id.fragment_home_movie_chip_group_genres);
            mSeriesChipGroup = view.findViewById(R.id.fragment_home_series_chip_group_genres);

            mMovieRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mMovieRecyclerView.setAdapter(mMoviePlaceHolderAdapter);
            mMovieRecyclerView.setHasFixedSize(true);

            mSeriesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mSeriesRecyclerView.setAdapter(mSeriesPlaceHolderAdapter);
            mSeriesRecyclerView.setHasFixedSize(true);

            mMovieChipGroup.setOnCheckedChangeListener(this);
            mSeriesChipGroup.setOnCheckedChangeListener(this);

        } else if (!isLandScape) {// tablet but portrait
            mViewPager2 = view.findViewById(R.id.layout_trending_viewPager2);
            TabLayout tabLayout = view.findViewById(R.id.layout_trending_tabs);

            mViewPager2.setAdapter(mTrendingPlaceHolderAdapter);
            mViewPager2.setPageTransformer(mAnimator);

            new TabLayoutMediator(tabLayout, mViewPager2, (tab, position) -> {
                tab.parent.setTabIconTintResource(R.color.color_tab_indicator_state_list);
                tab.setIcon(R.drawable.ic_tab_indicator);
            }).attach();

            mDramaticMoviesRecyclerView = view.findViewById(R.id.fragment_home_drama_movies_recycler_view);
            mExcitingShowsRecyclerView = view.findViewById(R.id.fragment_home_exciting_tv_shows_recycler_view);
            mActionMoviesRecyclerview = view.findViewById(R.id.fragment_home_action_movies_recycler_view);
            mCrimeShowsRecyclerView = view.findViewById(R.id.fragment_home_crime_shows_recycler_view);
            mAnimationMoviesRecyclerView = view.findViewById(R.id.fragment_home_animation_movies_recycler_view);

            mDramaticMoviesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mDramaticMoviesRecyclerView.setAdapter(mDramaticPlaceHolder);
            mDramaticMoviesRecyclerView.setHasFixedSize(true);

            mExcitingShowsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mExcitingShowsRecyclerView.setAdapter(mExcitingShowsPlaceHolder);
            mExcitingShowsRecyclerView.setHasFixedSize(true);

            mActionMoviesRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mActionMoviesRecyclerview.setAdapter(maActionPlaceHolder);
            mActionMoviesRecyclerview.setHasFixedSize(true);

            mCrimeShowsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mCrimeShowsRecyclerView.setAdapter(mCrimeShowsPlaceHolder);
            mCrimeShowsRecyclerView.setHasFixedSize(true);

            mAnimationMoviesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mAnimationMoviesRecyclerView.setAdapter(mAnimationPlaceHolder);
            mAnimationMoviesRecyclerView.setHasFixedSize(true);

        } else {//tablet but landscape
            mMainRecyclerView = view.findViewById(R.id.activity_main_fragment_home_recycler_view);

            mMainRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.VERTICAL,
                    false));
            mMainRecyclerView.setAdapter(mTrendingPlaceHolderAdapter);
            mMainRecyclerView.setHasFixedSize(true);
        }

        if (!mPresenter.checkConnection()) {
            mCallback.displayErrorFragment();
        }
        analytics.logEvent("HomeFragmentLaunched", new Bundle());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isTablet) {
            mPresenter.start();
        } else if (!isLandScape) {
            mPresenter.retrieveTrendingStuff();
            mPresenter.retrieveDramaMovies();
        } else {
            mPresenter.retrieveTrendingStuff();
        }

        if (mSavedMovieChipRestored) {
            this.onCheckedChanged(mMovieChipGroup, mSaveMovieChipId);
        }
        if (mSavedSeriesChipRestored) {
            this.onCheckedChanged(mSeriesChipGroup, mSavedSeriesChipId);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (!isTablet) {
            mSavedSeriesChipId = mSeriesChipGroup.getCheckedChipId();
            mSavedSeriesChipRestored = true;
            mSaveMovieChipId = mMovieChipGroup.getCheckedChipId();
            mSavedMovieChipRestored = true;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (!isTablet) {
            outState.putInt(CHECKED_MOVIE_CHIP, mMovieChipGroup.getCheckedChipId());
            outState.putInt(CHECKED_SERIES_CHIP, mSeriesChipGroup.getCheckedChipId());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onMovieClick(SimpleMovie movie) {
        Bundle bundle = new Bundle();
        bundle.putLong(FirebaseAnalytics.Param.ITEM_ID, movie.getId());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, movie.getTitle());
        analytics.logEvent("MovieItemSelected", bundle);
        mPresenter.showMovieDetails(movie);
    }

    @Override
    public void onSeriesClick(SimpleSeries series) {
        Bundle bundle = new Bundle();
        bundle.putLong(FirebaseAnalytics.Param.ITEM_ID, series.getId());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, series.getTitle());
        analytics.logEvent("SeriesSelected", bundle);

        mPresenter.showSeriesDetails(series);
    }

    @Override
    public void onCheckedChanged(ChipGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.fragment_home_movie_chip_action:
                mMovieRecyclerView.setAdapter(mMoviePlaceHolderAdapter);
                mPresenter.getMovieWithGenreId(MovieGenre.ACTION);
                break;
            case R.id.fragment_home_movie_chip_animation:
                mMovieRecyclerView.setAdapter(mMoviePlaceHolderAdapter);
                mPresenter.getMovieWithGenreId(MovieGenre.ANIMATION);
                break;
            case R.id.fragment_home_movie_chip_comedy:
                mMovieRecyclerView.setAdapter(mMoviePlaceHolderAdapter);
                mPresenter.getMovieWithGenreId(MovieGenre.COMEDY);
                break;
            case R.id.fragment_home_movie_chip_drama:
                mMovieRecyclerView.setAdapter(mMoviePlaceHolderAdapter);
                mPresenter.getMovieWithGenreId(MovieGenre.DRAMA);
                break;
            case R.id.fragment_home_movie_chip_fantasy:
                mMovieRecyclerView.setAdapter(mMoviePlaceHolderAdapter);
                mPresenter.getMovieWithGenreId(MovieGenre.FANTASY);
                break;

            //series
            case R.id.fragment_home_series_chip_action_adventure:
                mSeriesRecyclerView.setAdapter(mSeriesPlaceHolderAdapter);
                mPresenter.getSeriesWithGenreId(SeriesGenre.ACTION_ADVENTURE);
                break;
            case R.id.fragment_home_series_chip_crime:
                mSeriesRecyclerView.setAdapter(mSeriesPlaceHolderAdapter);
                mPresenter.getSeriesWithGenreId(SeriesGenre.CRIME);
                break;
            case R.id.fragment_home_series_chip_family:
                mSeriesRecyclerView.setAdapter(mSeriesPlaceHolderAdapter);
                mPresenter.getSeriesWithGenreId(SeriesGenre.FAMILY);
                break;
            case R.id.fragment_home_series_chip_mystery:
                mSeriesRecyclerView.setAdapter(mSeriesPlaceHolderAdapter);
                mPresenter.getSeriesWithGenreId(SeriesGenre.MYSTERY);
                break;
            case R.id.fragment_home_series_chip_sci_fi:
                mSeriesRecyclerView.setAdapter(mSeriesPlaceHolderAdapter);
                mPresenter.getSeriesWithGenreId(SeriesGenre.SCI_FI);
                break;
        }
    }

    @Override
    public void showLoadingUi(boolean isActive, String genreId) {
        if (!isActive) {
            if (!isTablet) {
                if (mViewPager2.getAdapter() != mTrendingAdapter) {
                    mViewPager2.setAdapter(mTrendingAdapter);
                }
                if (mMovieRecyclerView.getAdapter() != mMovieAdapter) {
                    mMovieRecyclerView.setAdapter(mMovieAdapter);
                }
                if (mSeriesRecyclerView.getAdapter() != mSeriesAdapter) {
                    mSeriesRecyclerView.setAdapter(mSeriesAdapter);
                }
            } else if (!isLandScape) {// tablet and portrait
                switch (genreId) {
                    case MovieGenre.DRAMA:
                        if (mDramaticMoviesRecyclerView.getAdapter() != mDramaticMoviesAdapter) {
                            mDramaticMoviesRecyclerView.setAdapter(mDramaticMoviesAdapter);
                        }
                        break;
                    case SeriesGenre.MYSTERY:
                        if (mExcitingShowsRecyclerView.getAdapter() != mExcitingShowsAdapter) {
                            mExcitingShowsRecyclerView.setAdapter(mExcitingShowsAdapter);
                        }
                        break;
                    case MovieGenre.ACTION:
                        if (mActionMoviesRecyclerview.getAdapter() != mActionMoviesAdapter) {
                            mActionMoviesRecyclerview.setAdapter(mActionMoviesAdapter);
                        }
                        break;
                    case SeriesGenre.CRIME:
                        if (mCrimeShowsRecyclerView.getAdapter() != mCrimeShowsAdapter) {
                            mCrimeShowsRecyclerView.setAdapter(mCrimeShowsAdapter);
                        }
                        break;
                    case MovieGenre.ANIMATION:
                        if (mAnimationMoviesRecyclerView.getAdapter() != mAnimationMoviesAdapter) {
                            mAnimationMoviesRecyclerView.setAdapter(mAnimationMoviesAdapter);
                        }
                        break;
                    default:
                        if (mViewPager2.getAdapter() != mTrendingAdapter) {
                            mViewPager2.setAdapter(mTrendingAdapter);
                        }
                }
            } else { // tablet and landscape
                if (mMainRecyclerView.getAdapter() != mTrendingAdapter) {
                    mMainRecyclerView.setAdapter(mTrendingAdapter);
                }
            }
        }
    }

    @Override
    public boolean isFragmentActive() {
        return isAdded();
    }

    @Override
    public void showSeries(List<SimpleSeries> series, String genreId) {
        if (!isTablet) {
            mSeriesAdapter.setSeries(series);
        } else if (!isLandScape) {
            switch (genreId) {
                case SeriesGenre.CRIME:
                    mCrimeShowsAdapter.setSeries(series);
                    mPresenter.retrieveAnimationMovies();
                    break;
                case SeriesGenre.MYSTERY:
                    mExcitingShowsAdapter.setSeries(series);
                    mPresenter.retrieveActionMovies();
                    break;
            }
        }
    }

    @Override
    public void showTrendingMovies(List<SimpleMovie> movies) {
        mTrendingAdapter.setTrendingStuff(movies);
    }

    @Override
    public void showMovies(List<SimpleMovie> movies, String genreId) {
        if (!isTablet) {
            mMovieAdapter.setMovies(movies);
        } else if (!isLandScape) {
            switch (genreId) {
                case MovieGenre.DRAMA:
                    mDramaticMoviesAdapter.setMovies(movies);
                    mPresenter.retrieveExcitingShows();
                    break;
                case MovieGenre.ACTION:
                    mActionMoviesAdapter.setMovies(movies);
                    mPresenter.retrieveCrimeShows();
                    break;
                case MovieGenre.ANIMATION:
                    mAnimationMoviesAdapter.setMovies(movies);
                    break;
            }
        }
    }

    @Override
    public void showErrorMessage() {
        if (requireActivity().getLifecycle().getCurrentState() == Lifecycle.State.RESUMED) {
            mCallback.displayErrorFragment();
        }
    }

    @Override
    public void showSeriesDetails(SimpleSeries series) {
        if (series == null) return;

        if (isTablet && isLandScape) {
            mCallback.displayDetailsFragment(series.getId(), Config.SERIES);
        } else {
            Intent intent = new Intent(getActivity(), DetailsActivity.class);
            intent.putExtra(Config.EXTRA_SERIES_ID, series.getId());
            intent.putExtra(Config.EXTRA_TITLE, Config.SERIES);
            if (getActivity() != null) {
                getActivity().startActivity(intent);
            }
        }
    }

    @Override
    public void showMovieDetails(SimpleMovie movie) {
        if (movie == null) return;

        if (isTablet && isLandScape) {
            mCallback.displayDetailsFragment(movie.getId(), Config.MOVIE);
        } else {
            Intent intent = new Intent(getActivity(), DetailsActivity.class);
            intent.putExtra(Config.EXTRA_MOVIE_ID, movie.getId());
            intent.putExtra(Config.EXTRA_TITLE, Config.MOVIE);
            if (getActivity() != null) {
                getActivity().startActivity(intent);
            }
        }
    }

    @Override
    public void setPresenter(HomeContract.Presenter presenter) {
        mPresenter = Objects.requireNonNull(presenter);
    }

    @Override
    public boolean checkConnection() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) requireContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return connectivityManager.getActiveNetworkInfo() != null &&
                connectivityManager.getActiveNetworkInfo().isConnected();
    }

}
// TODO: 9/10/2021 Handle NDK Crashes and Recyclerview Overlays
// TODO: 9/10/2021 Properly Add animations everywhere showing correct view for either orientation

