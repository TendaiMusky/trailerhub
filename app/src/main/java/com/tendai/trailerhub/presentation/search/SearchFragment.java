package com.tendai.trailerhub.presentation.search;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tendai.trailerhub.Injection;
import com.tendai.trailerhub.R;
import com.tendai.trailerhub.domain.movieseries.model.Search;
import com.tendai.trailerhub.presentation.BaseFragment;
import com.tendai.trailerhub.presentation.details.DetailsActivity;
import com.tendai.trailerhub.presentation.search.adapter.SearchAdapter;
import com.tendai.trailerhub.presentation.utils.Config;

import java.util.List;
import java.util.Objects;

public class SearchFragment extends BaseFragment implements
        AdapterView.OnItemClickListener, SearchContract.View {

    private SearchContract.Presenter mPresenter;

    private SearchAdapter mAdapter;
    private View mProgressFrame;
    private ListView mListView;

    static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new SearchAdapter(requireActivity());
        new SearchPresenter(
                Injection.provideUseCaseHandler(),
                Injection.provideGetSearch(requireActivity()),
                this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        mListView = view.findViewById(R.id.activity_search_listView);
        mListView.setOnItemClickListener(this);
        mListView.setAdapter(mAdapter);

        mProgressFrame = view.findViewById(R.id.activity_search_progressFrame);
        mProgressFrame.setVisibility(View.GONE);
        return view;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (mAdapter.getItem(position) == null) {
            return;
        }
        String type = mAdapter.getItem(position).getType();

        if (type.equalsIgnoreCase(Config.MOVIE)) {

            Search search = mAdapter.getItem(position);
            Bundle bundle = new Bundle();
            bundle.putLong(FirebaseAnalytics.Param.ITEM_ID, search.getId());
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, search.getTitle());
            analytics.logEvent("SearchItemSelected", bundle);

            Intent intent = new Intent(getActivity(), DetailsActivity.class);
            intent.putExtra(Config.EXTRA_MOVIE_ID, mAdapter.getItem(position).getId());
            intent.putExtra(Config.EXTRA_TITLE, Config.MOVIE);
            startActivity(intent);

        } else if (type.equalsIgnoreCase(Config.SERIES)) {

            Search search = mAdapter.getItem(position);
            Bundle bundle = new Bundle();
            bundle.putLong(FirebaseAnalytics.Param.ITEM_ID, search.getId());
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, search.getTitle());
            analytics.logEvent("SearchItemSelected", bundle);

            Intent intent = new Intent(getActivity(), DetailsActivity.class);
            intent.putExtra(Config.EXTRA_SERIES_ID, mAdapter.getItem(position).getId());
            intent.putExtra(Config.EXTRA_TITLE, Config.SERIES);
            startActivity(intent);
        }
    }

    @Override
    public boolean isFragmentActive() {
        return isAdded();
    }

    @Override
    public void showLoadingUi(boolean showLoadingUi) {
        if (showLoadingUi) {
            mProgressFrame.setVisibility(View.VISIBLE);
        }
        if (!showLoadingUi) {
            mProgressFrame.setVisibility(View.GONE);
        }
    }

    @Override
    public void showSearchedStuff(List<Search> searches) {
        mAdapter.clear();
        mAdapter.addAll(searches);
    }

    @Override
    public void showErrorMessage() {
        String message = getResources().getString(R.string.error_retrieving_content);
        Snackbar.make(mListView, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setPresenter(SearchContract.Presenter presenter) {
        mPresenter = Objects.requireNonNull(presenter);
    }

    @Override
    public boolean checkConnection() {
        return false;
    }

    void startSearch(String query) {
        if (mPresenter == null) requireActivity().recreate();
        mPresenter.startSearch(query);
    }
}
