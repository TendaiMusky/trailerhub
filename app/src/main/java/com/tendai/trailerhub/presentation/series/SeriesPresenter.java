package com.tendai.trailerhub.presentation.series;

import com.tendai.trailerhub.domain.UseCase;
import com.tendai.trailerhub.domain.UseCaseHandler;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;
import com.tendai.trailerhub.domain.series.usecase.GetSeries;
import com.tendai.trailerhub.presentation.home.meta.SeriesGenre;
import com.tendai.trailerhub.presentation.utils.Config;

import java.util.List;

public class SeriesPresenter implements SeriesContract.Presenter {
    private static final String TAG = SeriesPresenter.class.getSimpleName();

    private final UseCaseHandler mUseCaseHandler;
    private final GetSeries mGetSeries;
    private final SeriesContract.View mSeriesView;

    public SeriesPresenter(UseCaseHandler useCaseHandler, GetSeries getSeries, SeriesContract.View seriesView) {
        mUseCaseHandler = useCaseHandler;
        mGetSeries = getSeries;
        mSeriesView = seriesView;
        mSeriesView.setPresenter(this);
    }

    @Override
    public void start() {
        getSeries(Config.POPULAR);
    }

    @Override
    public boolean checkConnection() {
        return mSeriesView.checkConnection();
    }

    @Override
    public void retrieveOnTheAir() {
        getSeries(Config.ON_THE_AIR);
    }

    @Override
    public void retrieveTopRated() {
        getSeries(Config.TOP_RATED);
    }

    @Override
    public void showSeriesDetails(SimpleSeries series) {
        mSeriesView.showSeriesDetails(series);
    }

    @Override
    public void retrieveFamily() {
        getSeries(SeriesGenre.FAMILY);
    }

    @Override
    public void retrieveActionAdventure() {
        getSeries(SeriesGenre.ACTION_ADVENTURE);
    }

    @Override
    public void retrieveSciFi() {
        getSeries(SeriesGenre.SCI_FI);
    }

    private void getSeries(String category) {
        int number = (int) (Math.random() * 10) + 1;

        GetSeries.RequestValues requestValues;

        if (category.equals(SeriesGenre.FAMILY)
                || category.equals(SeriesGenre.ACTION_ADVENTURE)
                || category.equals(SeriesGenre.SCI_FI)) {

            requestValues = new GetSeries.RequestValues(category, number);
            requestValues.setRequestType(GetSeries.RequestValues.TYPE_GENRE_ID);
        } else {
            requestValues = new GetSeries.RequestValues(category, number, false);
            requestValues.setRequestType(GetSeries.RequestValues.TYPE_CATEGORY);
        }

        mUseCaseHandler.execute(mGetSeries,
                requestValues,
                new UseCase.UseCaseCallback<GetSeries.ResponseValues>() {
                    @Override
                    public void onSuccess(GetSeries.ResponseValues response) {
                        List<SimpleSeries> series = response.getSeriesList();

                        if (!mSeriesView.isActive()) {
                            return;
                        }
                        mSeriesView.showLoadingUi(category, false);
                        mSeriesView.showSeries(series, category);
                    }

                    @Override
                    public void onFailure() {
                        if (!mSeriesView.isActive()) {
                            return;
                        }
                        mSeriesView.showErrorMessage();
                    }
                });
    }
}
