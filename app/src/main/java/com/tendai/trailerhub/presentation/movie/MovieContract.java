package com.tendai.trailerhub.presentation.movie;

import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.presentation.BasePresenter;
import com.tendai.trailerhub.presentation.BaseView;

import java.util.List;

public interface MovieContract {
    interface View extends BaseView<Presenter> {

        void showLoadingUi(String category, boolean isActive);

        boolean isFragmentActive();

        void showMovies(List<SimpleMovie> movies, String category);

        void showErrorMessage();

        void showMovieDetails(SimpleMovie movie);
    }

    interface Presenter extends BasePresenter {

        void retrieveUpcoming();

        void retrieveNowPlaying();

        void retrieveTopRated();

        void showMovieDetails(SimpleMovie movie);

        void retrieveComedy();

        void retrieveAnimation();
    }
}
