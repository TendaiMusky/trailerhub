package com.tendai.trailerhub.presentation.movie;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.ChipGroup;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tendai.trailerhub.Injection;
import com.tendai.trailerhub.R;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.presentation.BaseFragment;
import com.tendai.trailerhub.presentation.details.DetailsActivity;
import com.tendai.trailerhub.presentation.home.meta.MovieGenre;
import com.tendai.trailerhub.presentation.movie.adapter.MovieAdapter;
import com.tendai.trailerhub.presentation.movie.adapter.MovieItemClickListener;
import com.tendai.trailerhub.presentation.utils.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MovieFragment extends BaseFragment implements MovieContract.View,
        MovieItemClickListener, ChipGroup.OnCheckedChangeListener {

    private MovieContract.Presenter mPresenter;
    private MovieAdapter mPopularAdapter;
    private MovieAdapter mUpcomingAdapter;
    private MovieAdapter mNowPlayingAdapter;
    private MovieAdapter mTopRatedAdapter;
    private MovieAdapter mComedyAdapter;
    private MovieAdapter mAnimationAdapter;
    private MovieAdapter mMainAdapter;
    private MovieAdapter.PlaceHolderAdapter mPopularPlaceHolder;
    private MovieAdapter.PlaceHolderAdapter mNowPlayingPlaceHolder;
    private MovieAdapter.PlaceHolderAdapter mUpcomingPlaceHolder;
    private MovieAdapter.PlaceHolderAdapter mTopRatedPlaceHolder;
    private MovieAdapter.PlaceHolderAdapter mComedyPlaceHolder;
    private MovieAdapter.PlaceHolderAdapter mAnimationPlaceHolder;
    private MovieAdapter.PlaceHolderAdapter mMainPlaceHolder;

    private RecyclerView mRecyclerViewPopular;
    private RecyclerView mRecyclerViewUpcoming;
    private RecyclerView mRecyclerViewNowPlaying;
    private RecyclerView mRecyclerViewTopRated;
    private RecyclerView mRecyclerViewComedy;
    private RecyclerView mRecyclerViewAnimation;
    private RecyclerView mMainRecyclerView;

    public static MovieFragment newInstance() {
        return new MovieFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            mCallback = (Callback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement Callback Interface");
        }
    }

    @Override
    public void setPresenter(MovieContract.Presenter presenter) {
        mPresenter = Objects.requireNonNull(presenter);
    }

    @Override
    public boolean checkConnection() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) requireContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return connectivityManager.getActiveNetworkInfo() != null &&
                connectivityManager.getActiveNetworkInfo().isConnected();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new MoviePresenter(
                Injection.provideUseCaseHandler(),
                Injection.provideGetMovie(requireActivity()),
                this);

        if (!isTablet) {
            mPopularAdapter = new MovieAdapter(new ArrayList<>(0), this,isTablet ,isLandScape );
            mNowPlayingAdapter = new MovieAdapter(new ArrayList<>(0), this,isTablet ,isLandScape );
            mUpcomingAdapter = new MovieAdapter(new ArrayList<>(0), this,isTablet ,isLandScape );

            mPopularPlaceHolder = new MovieAdapter.PlaceHolderAdapter();
            mNowPlayingPlaceHolder = new MovieAdapter.PlaceHolderAdapter();
            mUpcomingPlaceHolder = new MovieAdapter.PlaceHolderAdapter();

        } else if (!isLandScape) {
            mPopularAdapter = new MovieAdapter(new ArrayList<>(0), this,isTablet ,isLandScape );
            mNowPlayingAdapter = new MovieAdapter(new ArrayList<>(0), this,isTablet ,isLandScape );
            mUpcomingAdapter = new MovieAdapter(new ArrayList<>(0), this,isTablet ,isLandScape );
            mTopRatedAdapter = new MovieAdapter(new ArrayList<>(0), this,isTablet ,isLandScape );
            mComedyAdapter = new MovieAdapter(new ArrayList<>(0), this,isTablet ,isLandScape );
            mAnimationAdapter = new MovieAdapter(new ArrayList<>(0), this,isTablet ,isLandScape );

            mPopularPlaceHolder = new MovieAdapter.PlaceHolderAdapter();
            mNowPlayingPlaceHolder = new MovieAdapter.PlaceHolderAdapter();
            mUpcomingPlaceHolder = new MovieAdapter.PlaceHolderAdapter();
            mTopRatedPlaceHolder = new MovieAdapter.PlaceHolderAdapter();
            mComedyPlaceHolder = new MovieAdapter.PlaceHolderAdapter();
            mAnimationPlaceHolder = new MovieAdapter.PlaceHolderAdapter();

        } else {
            mMainAdapter = new MovieAdapter(new ArrayList<>(0), this,isTablet ,isLandScape );
            mMainPlaceHolder = new MovieAdapter.PlaceHolderAdapter();
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie, container, false);

        if (!isTablet) {
            mRecyclerViewPopular = view.findViewById(R.id.fragment_movie_recycler_view_popular_this_week);
            mRecyclerViewUpcoming = view.findViewById(R.id.fragment_movie_recycler_view_upcoming);
            mRecyclerViewNowPlaying = view.findViewById(R.id.fragment_movie_recycler_view_now_playing);

            mRecyclerViewPopular.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewPopular.setAdapter(mPopularPlaceHolder);
            mRecyclerViewPopular.setHasFixedSize(true);

            mRecyclerViewUpcoming.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewUpcoming.setAdapter(mUpcomingPlaceHolder);
            mRecyclerViewUpcoming.setHasFixedSize(true);

            mRecyclerViewNowPlaying.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewNowPlaying.setAdapter(mNowPlayingPlaceHolder);
            mRecyclerViewNowPlaying.setHasFixedSize(true);

        } else if (!isLandScape) { // tablet but portrait
            mRecyclerViewPopular = view.findViewById(R.id.fragment_movie_recycler_view_popular_this_week);
            mRecyclerViewUpcoming = view.findViewById(R.id.fragment_movie_recycler_view_upcoming);
            mRecyclerViewNowPlaying = view.findViewById(R.id.fragment_movie_recycler_view_now_playing);
            mRecyclerViewTopRated = view.findViewById(R.id.fragment_movie_recycler_view_top_rated);
            mRecyclerViewComedy = view.findViewById(R.id.fragment_movie_recycler_view_comedy);
            mRecyclerViewAnimation = view.findViewById(R.id.fragment_movie_recycler_view_animation);

            mRecyclerViewPopular.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewPopular.setAdapter(mPopularPlaceHolder);
            mRecyclerViewPopular.setHasFixedSize(true);

            mRecyclerViewUpcoming.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewUpcoming.setAdapter(mUpcomingPlaceHolder);
            mRecyclerViewUpcoming.setHasFixedSize(true);

            mRecyclerViewNowPlaying.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewNowPlaying.setAdapter(mNowPlayingPlaceHolder);
            mRecyclerViewNowPlaying.setHasFixedSize(true);

            mRecyclerViewTopRated.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewTopRated.setAdapter(mTopRatedPlaceHolder);
            mRecyclerViewTopRated.setHasFixedSize(true);

            mRecyclerViewComedy.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewComedy.setAdapter(mComedyPlaceHolder);
            mRecyclerViewComedy.setHasFixedSize(true);

            mRecyclerViewAnimation.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.HORIZONTAL,
                    false));
            mRecyclerViewAnimation.setAdapter(mAnimationPlaceHolder);
            mRecyclerViewAnimation.setHasFixedSize(true);

        } else { // tablet but landscape
            mMainRecyclerView = view.findViewById(R.id.activity_main_fragment_movie_recycler_view);
            ChipGroup mChipGroup = view.findViewById(R.id.fragment_movie_chipGroup);

            mChipGroup.setOnCheckedChangeListener(this);

            mMainRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    LinearLayoutManager.VERTICAL,
                    false));
            mMainRecyclerView.setAdapter(mMainPlaceHolder);
            mMainRecyclerView.setHasFixedSize(true);
        }


        if (!mPresenter.checkConnection()) {
            mCallback.displayErrorFragment();
        }
        analytics.logEvent("MovieFragmentLaunched", new Bundle());
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void onMovieClick(SimpleMovie movie) {
        Bundle bundle = new Bundle();
        bundle.putLong(FirebaseAnalytics.Param.ITEM_ID, movie.getId());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, movie.getTitle());
        analytics.logEvent("MovieItemSelected", bundle);
        mPresenter.showMovieDetails(movie);
    }

    @Override
    public void showLoadingUi(String category, boolean isActive) {
        if (!isActive) {
            switch (category) {
                case Config.POPULAR:
                    if (isTablet && isLandScape) {
                        if (mMainRecyclerView.getAdapter() != mMainAdapter) {
                            mMainRecyclerView.setAdapter(mMainAdapter);
                        }
                    } else {
                        if (mRecyclerViewPopular.getAdapter() != mPopularAdapter) {
                            mRecyclerViewPopular.setAdapter(mPopularAdapter);
                        }
                    }
                    break;
                case Config.UPCOMING:
                    if (isTablet && isLandScape) {
                        if (mMainRecyclerView.getAdapter() != mMainAdapter) {
                            mMainRecyclerView.setAdapter(mMainAdapter);
                        }
                    } else {
                        if (mRecyclerViewUpcoming.getAdapter() != mUpcomingAdapter) {
                            mRecyclerViewUpcoming.setAdapter(mUpcomingAdapter);
                        }
                    }
                    break;
                case Config.NOW_PLAYING:
                    if (isTablet && isLandScape) {
                        if (mMainRecyclerView.getAdapter() != mMainAdapter) {
                            mMainRecyclerView.setAdapter(mMainAdapter);
                        }
                    } else {
                        if (mRecyclerViewNowPlaying.getAdapter() != mNowPlayingAdapter) {
                            mRecyclerViewNowPlaying.setAdapter(mNowPlayingAdapter);
                        }
                    }
                    break;
                case Config.TOP_RATED:
                    if (mRecyclerViewTopRated.getAdapter() != mTopRatedAdapter) {
                        mRecyclerViewTopRated.setAdapter(mTopRatedAdapter);
                    }
                    break;
                case MovieGenre.COMEDY:
                    if (mRecyclerViewComedy.getAdapter() != mComedyAdapter) {
                        mRecyclerViewComedy.setAdapter(mComedyAdapter);
                    }
                    break;
                case MovieGenre.ANIMATION:
                    if (mRecyclerViewAnimation.getAdapter() != mAnimationAdapter) {
                        mRecyclerViewAnimation.setAdapter(mAnimationAdapter);
                    }
                    break;
            }
        }
    }

    @Override
    public boolean isFragmentActive() {
        return isAdded();
    }

    @Override
    public void showMovies(List<SimpleMovie> movies, String category) {
        switch (category) {
            case Config.POPULAR:
                if (isTablet && isLandScape) {
                    mMainAdapter.setMovies(movies);
                } else {
                    mPopularAdapter.setMovies(movies);
                    mPresenter.retrieveUpcoming();
                }
                break;
            case Config.UPCOMING:
                if (isTablet && isLandScape) {
                    mMainAdapter.setMovies(movies);
                } else {
                    mUpcomingAdapter.setMovies(movies);
                    mPresenter.retrieveNowPlaying();
                }
                break;
            case Config.NOW_PLAYING:
                if (isTablet && isLandScape) {
                    mMainAdapter.setMovies(movies);
                } else {
                    mNowPlayingAdapter.setMovies(movies);
                    if (isTablet && !isLandScape) {
                        mPresenter.retrieveTopRated();
                    }
                }
                break;
            case Config.TOP_RATED:
                mTopRatedAdapter.setMovies(movies);
                if (isTablet && !isLandScape) {
                    mPresenter.retrieveComedy();
                }
                break;
            case MovieGenre.COMEDY:
                mComedyAdapter.setMovies(movies);
                if (isTablet && !isLandScape) {
                    mPresenter.retrieveAnimation();
                }
                break;
            case MovieGenre.ANIMATION:
                mAnimationAdapter.setMovies(movies);
                break;
        }
    }

    @Override
    public void showErrorMessage() {
        if (requireActivity().getLifecycle().getCurrentState() == Lifecycle.State.RESUMED) {
            mCallback.displayErrorFragment();
        }
    }

    @Override
    public void showMovieDetails(SimpleMovie movie) {
        if (movie == null) return;

        if (isTablet && isLandScape) {
            mCallback.displayDetailsFragment(movie.getId(), Config.MOVIE);
        } else {
            Intent intent = new Intent(getActivity(), DetailsActivity.class);
            intent.putExtra(Config.EXTRA_MOVIE_ID, movie.getId());
            intent.putExtra(Config.EXTRA_TITLE, Config.MOVIE);
            if (getActivity() != null) {
                getActivity().startActivity(intent);
            }
        }
    }

    @Override
    public void onCheckedChanged(ChipGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.fragment_movie_chip_popular_this_week:
                mPresenter.start();
                break;
            case R.id.fragment_movie_chip_upcoming:
                mPresenter.retrieveUpcoming();
                break;
            case R.id.fragment_movie_chip_now_playing:
                mPresenter.retrieveNowPlaying();
                break;
        }
    }
}
