package com.tendai.trailerhub.presentation.series;

import com.tendai.trailerhub.domain.series.model.SimpleSeries;
import com.tendai.trailerhub.presentation.BasePresenter;
import com.tendai.trailerhub.presentation.BaseView;

import java.util.List;

public interface SeriesContract {
    interface View extends BaseView<Presenter> {
        void showLoadingUi(String category, boolean isActive);

        boolean isActive();

        void showSeries(List<SimpleSeries> seriesList, String category);

        void showErrorMessage();

        void showSeriesDetails(SimpleSeries series);
    }

    interface Presenter extends BasePresenter {
        void retrieveOnTheAir();

        void retrieveTopRated();

        void showSeriesDetails(SimpleSeries series);

        void retrieveFamily();

        void retrieveActionAdventure();

        void retrieveSciFi();
    }
}
