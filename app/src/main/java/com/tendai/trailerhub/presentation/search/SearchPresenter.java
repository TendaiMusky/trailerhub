package com.tendai.trailerhub.presentation.search;

import com.tendai.trailerhub.domain.UseCase;
import com.tendai.trailerhub.domain.UseCaseHandler;
import com.tendai.trailerhub.domain.movieseries.model.Search;
import com.tendai.trailerhub.domain.movieseries.usecase.GetSearch;
import com.tendai.trailerhub.presentation.series.SeriesPresenter;

import java.util.List;

public class SearchPresenter implements SearchContract.Presenter {
    private static final String TAG = SeriesPresenter.class.getSimpleName();

    private final UseCaseHandler mUseCaseHandler;
    private final GetSearch mGetSearch;
    private final SearchContract.View mSearchView;

    SearchPresenter(UseCaseHandler useCaseHandler, GetSearch getSearch, SearchContract.View view) {
        mUseCaseHandler = useCaseHandler;
        mGetSearch = getSearch;
        mSearchView = view;
        mSearchView.setPresenter(this);
    }

    @Override
    public void showLoadingUi(boolean showLoadingUi) {
        mSearchView.showLoadingUi(showLoadingUi);
    }

    @Override
    public void startSearch(String searchQuery) {
        getSearch(searchQuery);
    }

    private void getSearch(String searchQuery) {
        GetSearch.RequestValues requestValues = new GetSearch.RequestValues(searchQuery);

        mUseCaseHandler.execute(mGetSearch, requestValues,
                new UseCase.UseCaseCallback<GetSearch.ResponseValues>() {
                    @Override
                    public void onSuccess(GetSearch.ResponseValues response) {
                        List<Search> searches = response.getSearchResults();

                        if (!mSearchView.isFragmentActive()) {
                            return;
                        }
                        mSearchView.showLoadingUi(false);
                        mSearchView.showSearchedStuff(searches);
                    }

                    @Override
                    public void onFailure() {
                        if (!mSearchView.isFragmentActive()) {
                            return;
                        }
                        mSearchView.showLoadingUi(false);
                        mSearchView.showErrorMessage();
                    }
                });
    }

    @Override
    public void start() {
        // not needed
    }

    @Override
    public boolean checkConnection() {
        return false;
    }
}
