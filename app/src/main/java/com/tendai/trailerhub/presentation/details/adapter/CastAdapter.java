package com.tendai.trailerhub.presentation.details.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tendai.trailerhub.R;
import com.tendai.trailerhub.domain.model.SimpleCast;

import java.util.List;
import java.util.Objects;

public class CastAdapter extends RecyclerView.Adapter<CastViewHolder> {
    private List<SimpleCast> mCastList;
    private OnCastClickListener castClickListener;

    public CastAdapter(List<SimpleCast> castList, OnCastClickListener listener) {
        setCast(castList);
        castClickListener = listener;
    }

    @NonNull
    @Override
    public CastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cast, parent, false);
        CastViewHolder holder = new CastViewHolder(itemView);
        holder.itemView.setOnClickListener(v -> {
            SimpleCast cast = mCastList.get(holder.getAdapterPosition());
            cast.setExpanded(!cast.isExpanded());
            notifyItemChanged(holder.getAdapterPosition());
            castClickListener.onCastClick(cast);

        });
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CastViewHolder holder, int position) {
        SimpleCast cast = mCastList.get(position);
        if (cast == null) {
            holder.showPlaceHolderAnimation();
        } else {
            holder.bindData(cast);
            boolean isExpanded = mCastList.get(position).isExpanded();
            holder.getExpandableLayout().setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mCastList.size();
    }

    public void setCast(List<SimpleCast> cast) {
        mCastList = Objects.requireNonNull(cast);
        notifyDataSetChanged();
    }

    public static class PlaceHolderAdapter extends RecyclerView.Adapter<CastViewHolder> {
        @NonNull
        @Override
        public CastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cast, parent, false);
            return new CastViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull CastViewHolder holder, int position) {
            holder.showPlaceHolderAnimation();
        }

        @Override
        public int getItemCount() {
            return 10;
        }
    }

}

