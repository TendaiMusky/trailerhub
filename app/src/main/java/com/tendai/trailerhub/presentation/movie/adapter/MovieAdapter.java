package com.tendai.trailerhub.presentation.movie.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tendai.trailerhub.R;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;

import java.util.List;
import java.util.Objects;

public class MovieAdapter extends RecyclerView.Adapter<MovieViewHolder> {

    private List<SimpleMovie> mMovies;
    private final MovieItemClickListener mItemClickListener;
    boolean mIsTablet;
    boolean mIsLandScape;

    public MovieAdapter(List<SimpleMovie> movies, MovieItemClickListener itemClickListener, boolean isTablet, boolean isLandScape) {
        setMovies(movies);
        mItemClickListener = itemClickListener;
        mIsTablet = isTablet;
        mIsLandScape = isLandScape;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie_series_name_poster, parent, false);

        MovieViewHolder viewHolder = new MovieViewHolder(itemView, mIsTablet, mIsLandScape);
        viewHolder.itemView.setOnClickListener(view -> mItemClickListener.onMovieClick((SimpleMovie) view.getTag()));

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        if (mMovies.size() != 0) {
            SimpleMovie movie = mMovies.get(position);
            if (movie == null) {
                holder.showPlaceHolderAnimation();
            } else {
                holder.itemView.setTag(movie);
                holder.bindData(movie);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mMovies.size();
    }

    public void setMovies(List<SimpleMovie> movies) {
        mMovies = Objects.requireNonNull(movies);
        notifyDataSetChanged();
    }

    public static class PlaceHolderAdapter extends RecyclerView.Adapter<MovieViewHolder> {

        @NonNull
        @Override
        public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_movie_series_name_poster,
                            parent,
                            false);
            return new MovieViewHolder(itemView, false, false);
        }

        @Override
        public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
            holder.showPlaceHolderAnimation();
        }

        @Override
        public int getItemCount() {
            return 20;
        }
    }

}
