package com.tendai.trailerhub.presentation.error;

public class ErrorPresenter implements ErrorContract.Presenter {

    private final ErrorContract.View mErrorView;

    public ErrorPresenter(ErrorContract.View view) {
        mErrorView = view;
        view.setPresenter(this);
    }

    @Override
    public void removeErrorFragment() {
        mErrorView.removeErrorFragment();
    }

    @Override
    public void start() {
    }

    @Override
    public boolean checkConnection() {
        return false;
    }
}
