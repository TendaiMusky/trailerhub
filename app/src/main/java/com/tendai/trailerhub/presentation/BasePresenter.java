package com.tendai.trailerhub.presentation;

public interface BasePresenter {
    void start();

    boolean checkConnection();
}
