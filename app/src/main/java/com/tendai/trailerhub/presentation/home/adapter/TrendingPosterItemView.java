package com.tendai.trailerhub.presentation.home.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.tendai.trailerhub.R;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.presentation.utils.Config;

public class TrendingPosterItemView {

    private final ImageView mImageView;
    private TextView mTitle;
    private TextView mSummary;
    private final View mCardView;

    public TrendingPosterItemView(LayoutInflater layoutInflater, ViewGroup container, boolean isTablet, boolean isLandScape) {
        if (isLandScape && isTablet) {
            mCardView = layoutInflater.inflate(R.layout.item_movie_series_name_poster, container, false);
            mImageView = mCardView.findViewById(R.id.item_movie_series_image_poster);
            mTitle = mCardView.findViewById(R.id.item_movie_series_text_title);
            mSummary = mCardView.findViewById(R.id.item_movie_series_text_summary);

        } else {
            mCardView = layoutInflater
                    .inflate(R.layout.item_trending_background_poster, container, false);
            mImageView = mCardView.findViewById(R.id.layout_trending_background_image_view);
        }
    }

    public View getCardView() {
        return mCardView;
    }

    public void setImage(String imagePath) {
        Glide.with(mImageView).
                asBitmap().
                load(Config.IMAGE_BACKDROP_URL + imagePath).
                into(new BitmapImageViewTarget(mImageView) {
                    @Override
                    protected void setResource(Bitmap resource) {

                        super.setResource(resource);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        mImageView.setScaleType(ImageView.ScaleType.FIT_XY);
                        mImageView.setImageResource(R.drawable.ic_place_holder);
                    }
                });
    }

    public void bindMovie(SimpleMovie movie) {
        Glide.with(mImageView).
                asBitmap().
                load(Config.IMAGE_POSTER_URL + movie.getPosterPath()).
                into(new BitmapImageViewTarget(mImageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        mTitle.setText(movie.getTitle());
                        mSummary.setText(movie.getOverview());
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        mImageView.setImageResource(R.drawable.ic_place_holder);
                    }
                });
    }
}