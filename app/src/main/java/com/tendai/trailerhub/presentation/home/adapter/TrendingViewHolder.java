package com.tendai.trailerhub.presentation.home.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tendai.trailerhub.domain.movie.model.SimpleMovie;

public class TrendingViewHolder extends RecyclerView.ViewHolder {

    private final TrendingPosterItemView mPosterItemView;

    public TrendingViewHolder(@NonNull TrendingPosterItemView posterItemView) {
        super(posterItemView.getCardView());
        mPosterItemView = posterItemView;
    }

    void bindMovie(SimpleMovie movie) {
        mPosterItemView.bindMovie(movie);
    }

    void setImage(String imagePath) {
        mPosterItemView.setImage(imagePath);
    }
}
