package com.tendai.trailerhub.presentation.search;

import com.tendai.trailerhub.domain.movieseries.model.Search;
import com.tendai.trailerhub.presentation.BasePresenter;
import com.tendai.trailerhub.presentation.BaseView;

import java.util.List;

public interface SearchContract {

    interface View extends BaseView<SearchContract.Presenter> {
        boolean isFragmentActive();

        void showLoadingUi(boolean showLoadingUi);

        void showSearchedStuff(List<Search> searches);

        void showErrorMessage();
    }

    interface Presenter extends BasePresenter {
        void showLoadingUi(boolean showLoadingUi);

        void startSearch(String searchQuery);
    }

}
