package com.tendai.trailerhub.presentation.home;

import static com.tendai.trailerhub.domain.movie.usecase.GetMovie.RequestValues.TYPE_GENRE_ID;

import com.tendai.trailerhub.domain.UseCase;
import com.tendai.trailerhub.domain.UseCaseHandler;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.domain.movie.usecase.GetMovie;
import com.tendai.trailerhub.domain.movieseries.usecase.GetTrending;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;
import com.tendai.trailerhub.domain.series.usecase.GetSeries;
import com.tendai.trailerhub.presentation.home.meta.MovieGenre;
import com.tendai.trailerhub.presentation.home.meta.SeriesGenre;
import com.tendai.trailerhub.presentation.utils.Config;

import java.util.List;

public class HomePresenter implements HomeContract.Presenter {
    private static final String TAG = HomePresenter.class.getSimpleName();

    private final UseCaseHandler mUseCaseHandler;
    private final GetTrending mGetTrending;
    private final GetSeries mGetSeries;
    private final GetMovie mGetMovie;
    private final HomeContract.View mHomeView;

    private boolean mFirstLoad = true;

    HomePresenter(UseCaseHandler useCaseHandler, GetTrending getTrending,
                  GetSeries getSeries, GetMovie getMovie, HomeContract.View view) {
        mUseCaseHandler = useCaseHandler;
        mGetTrending = getTrending;
        mGetSeries = getSeries;
        mGetMovie = getMovie;
        mHomeView = view;
        mHomeView.setPresenter(this);
    }

    @Override
    public void getSeriesWithGenreId(String genreId) {
        getSeries(genreId);
    }

    @Override
    public void getMovieWithGenreId(String genreId) {
        getMovies(genreId);
    }

    @Override
    public void retrieveDramaMovies() {
        getMovies(MovieGenre.DRAMA);
    }

    @Override
    public void retrieveExcitingShows() {
        getSeries(SeriesGenre.MYSTERY);
    }

    @Override
    public void retrieveActionMovies() {
        getMovies(MovieGenre.ACTION);
    }

    @Override
    public void retrieveCrimeShows() {
        getSeries(SeriesGenre.CRIME);
    }

    @Override
    public void retrieveAnimationMovies() {
        getMovies(MovieGenre.ANIMATION);
    }

    @Override
    public void retrieveTrendingStuff() {
        getTrendingStuff();
    }

    @Override
    public void showSeriesDetails(SimpleSeries series) {
        mHomeView.showSeriesDetails(series);
    }

    @Override
    public void showMovieDetails(SimpleMovie movie) {
        mHomeView.showMovieDetails(movie);
    }

    @Override
    public void start() {
        if (mFirstLoad) {
            getTrendingStuff();
            getMovies(MovieGenre.DRAMA);
            getSeries(SeriesGenre.SCI_FI);
            mFirstLoad = false;
        } else {
            getTrendingStuff();
        }
    }

    @Override
    public boolean checkConnection() {
        return mHomeView.checkConnection();
    }

    private void getTrendingStuff() {
        GetTrending.RequestValues requestValues = new GetTrending.RequestValues(Config.MEDIA_TYPE_MOVIE, "day");

        mUseCaseHandler.execute(mGetTrending,
                requestValues,
                new UseCase.UseCaseCallback<GetTrending.ResponseValues>() {
                    @Override
                    public void onSuccess(GetTrending.ResponseValues response) {
                        List<SimpleMovie> movies = response.getMovies();
                        if (!mHomeView.isFragmentActive()) {
                            return;
                        }
                        mHomeView.showLoadingUi(false,"trending");
                        mHomeView.showTrendingMovies(movies);
                    }

                    @Override
                    public void onFailure() {
                        if (!mHomeView.isFragmentActive()) {
                            return;
                        }
                        mHomeView.showErrorMessage();
                    }
                });
    }

    private void getSeries(String genreId) {
        int number = (int) (Math.random() * 20) + 1;
        GetSeries.RequestValues requestValues = new GetSeries.RequestValues(genreId, number);
        requestValues.setRequestType(GetSeries.RequestValues.TYPE_GENRE_ID);

        mUseCaseHandler.execute(mGetSeries,
                requestValues,
                new UseCase.UseCaseCallback<GetSeries.ResponseValues>() {
                    @Override
                    public void onSuccess(GetSeries.ResponseValues response) {
                        List<SimpleSeries> series = response.getSeriesList();
                        if (!mHomeView.isFragmentActive()) {
                            return;
                        }
                        mHomeView.showLoadingUi(false, genreId);
                        mHomeView.showSeries(series, genreId);
                    }

                    @Override
                    public void onFailure() {
                        if (!mHomeView.isFragmentActive()) {
                            return;
                        }
//                        mView.showLoadingUi(false);
                        mHomeView.showErrorMessage();
                    }
                });

    }

    private void getMovies(String genreId) {
        int number = (int) (Math.random() * 20) + 1;
        GetMovie.RequestValues requestValues = new GetMovie.RequestValues(genreId, number);
        requestValues.setRequestType(TYPE_GENRE_ID);

        mUseCaseHandler.execute(mGetMovie,
                requestValues,
                new UseCase.UseCaseCallback<GetMovie.ResponseValues>() {
                    @Override
                    public void onSuccess(GetMovie.ResponseValues response) {
                        List<SimpleMovie> movies = response.getMovies();
                        if (!mHomeView.isFragmentActive()) {
                            return;
                        }
                        mHomeView.showLoadingUi(false, genreId);
                        mHomeView.showMovies(movies, genreId);
                    }

                    @Override
                    public void onFailure() {
                        if (!mHomeView.isFragmentActive()) {
                            return;
                        }
                        mHomeView.showErrorMessage();
                    }
                });
    }
}
