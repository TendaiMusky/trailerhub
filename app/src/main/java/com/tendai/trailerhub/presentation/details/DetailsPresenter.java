package com.tendai.trailerhub.presentation.details;

import com.tendai.trailerhub.domain.UseCase;
import com.tendai.trailerhub.domain.UseCaseHandler;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.domain.movie.usecase.GetMovie;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;
import com.tendai.trailerhub.domain.series.usecase.GetSeries;
import com.tendai.trailerhub.presentation.utils.Config;

public class DetailsPresenter implements DetailsContract.Presenter {

    private final UseCaseHandler mUseCaseHandler;
    private final GetMovie mGetMovie;
    private final GetSeries mGetSeries;
    private final DetailsContract.View mDetailsView;

    private final int mId;
    private final String mType;

    DetailsPresenter(UseCaseHandler useCaseHandler, int id, String type, GetMovie getMovie, GetSeries getSeries,
                     DetailsContract.View detailsView) {
        mUseCaseHandler = useCaseHandler;
        mGetMovie = getMovie;
        mGetSeries = getSeries;
        mDetailsView = detailsView;
        mId = id;
        mType = type;
        mDetailsView.setPresenter(this);
    }

    @Override
    public void start() {
        if (mType.equals(Config.MOVIE)) {
            getMovieDetails(mId);
        }
        if (mType.equals(Config.SERIES)) {
            getSeriesDetails(mId);
        }
    }

    @Override
    public boolean checkConnection() {
        return mDetailsView.checkConnection();
    }

    private void getMovieDetails(int id) {
        GetMovie.RequestValues requestValues = new GetMovie.RequestValues(id);
        requestValues.setRequestType(GetMovie.RequestValues.TYPE_MOVIE_ID);

        mUseCaseHandler.execute(mGetMovie, requestValues,
                new UseCase.UseCaseCallback<GetMovie.ResponseValues>() {
                    @Override
                    public void onSuccess(GetMovie.ResponseValues response) {
                        SimpleMovie movie = response.getMovie();
                        if (!mDetailsView.isActive()) {
                            return;
                        }
                        mDetailsView.showLoadingUi(false);
                        mDetailsView.showMovieDetails(movie);
                    }

                    @Override
                    public void onFailure() {
                        if (!mDetailsView.isActive()) {
                            return;
                        }
                        mDetailsView.showErrorMessage();
                    }
                });
    }

    private void getSeriesDetails(int id) {
        GetSeries.RequestValues requestValues = new GetSeries.RequestValues(id);
        requestValues.setRequestType(GetSeries.RequestValues.SERIES_ID);

        mUseCaseHandler.execute(mGetSeries, requestValues,
                new UseCase.UseCaseCallback<GetSeries.ResponseValues>() {
                    @Override
                    public void onSuccess(GetSeries.ResponseValues response) {
                        SimpleSeries series = response.getSeries();
                        if (!mDetailsView.isActive()) {
                            return;
                        }
                        mDetailsView.showLoadingUi(false);
                        mDetailsView.showSeriesDetails(series);
                    }

                    @Override
                    public void onFailure() {
                        if (!mDetailsView.isActive()) {
                            return;
                        }
                        mDetailsView.showLoadingUi(false);
                        mDetailsView.showErrorMessage();
                    }
                });
    }
}
