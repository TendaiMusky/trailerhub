package com.tendai.trailerhub.presentation.details;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tendai.trailerhub.Injection;
import com.tendai.trailerhub.R;
import com.tendai.trailerhub.domain.model.SimpleCast;
import com.tendai.trailerhub.domain.model.SimpleVideos;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;
import com.tendai.trailerhub.presentation.BaseFragment;
import com.tendai.trailerhub.presentation.details.adapter.CastAdapter;
import com.tendai.trailerhub.presentation.details.adapter.OnCastClickListener;
import com.tendai.trailerhub.presentation.trailer.TrailerActivity;
import com.tendai.trailerhub.presentation.utils.Config;

import java.util.ArrayList;
import java.util.Objects;

public class DetailsFragment extends BaseFragment implements DetailsContract.View, OnCastClickListener {
    private static final String ARGUMENT_ID = "ARGUMENT_ID";
    private static final String ARGUMENT_TITLE = "ARGUMENT_TITLE";

    private DetailsContract.Presenter mPresenter;
    private CastAdapter mAdapter;
    private CastAdapter.PlaceHolderAdapter mPlaceHolderAdapter;
    private Callback mCallback;

    private TextView mTitle;
    private TextView mReleaseYear;
    private TextView mRuntimeOrNumberOfSeasons;
    private RatingBar mRatingBar;
    private TextView mSummary;
    private RecyclerView mRecyclerView;
    private TextView[] mGenres;
    private ImageView mBackDrop;
    private ImageButton mPlayTrailer;
    private String mVideoId;

    public static DetailsFragment newInstance(String title, int id) {
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_ID, id);
        arguments.putString(ARGUMENT_TITLE, title);
        DetailsFragment detailsFragment = new DetailsFragment();
        detailsFragment.setArguments(arguments);
        return detailsFragment;
    }

    @Override
    public void setPresenter(DetailsContract.Presenter presenter) {
        mPresenter = Objects.requireNonNull(presenter);
    }

    @Override
    public boolean checkConnection() {
        return false;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            mCallback = (Callback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement DetailsFragment.Callback");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            String title = getArguments().getString(ARGUMENT_TITLE);
            assert title != null;
            int id = getArguments().getInt(ARGUMENT_ID);

            if (title.equals(Config.MOVIE)) {
                new DetailsPresenter(
                        Injection.provideUseCaseHandler(),
                        id,
                        title,
                        Injection.provideGetMovie(requireActivity()),
                        Injection.provideGetSeries(requireActivity()),
                        this);
            } else if (title.equals(Config.SERIES)) {
                new DetailsPresenter(
                        Injection.provideUseCaseHandler(),
                        id,
                        title,
                        Injection.provideGetMovie(requireActivity()),
                        Injection.provideGetSeries(requireActivity()),
                        this);
            }
        }

        mAdapter = new CastAdapter(new ArrayList<>(0), this);
        mPlaceHolderAdapter = new CastAdapter.PlaceHolderAdapter();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        analytics.logEvent("DetailsFragmentLaunched", new Bundle());
        View view = inflater.inflate(R.layout.fragment_movie_series_details, container, false);

        if (isTablet && isLandScape) {
            mPlayTrailer = view.findViewById(R.id.activity_movie_series_details_button_play_trailer);
            mBackDrop = view.findViewById(R.id.activity_movie_series_details_image_backdrop);
            mPlayTrailer.setOnClickListener(v -> {
                Intent intent = new Intent(getActivity(), TrailerActivity.class);
                intent.putExtra(Config.EXTRA_TRAILER_ID, mVideoId);
                startActivity(intent);
            });
        }
        mTitle = view.findViewById(R.id.movie_series_details_text_title);
        mReleaseYear = view.findViewById(R.id.movie_series_details_text_year);
        mRuntimeOrNumberOfSeasons = view.findViewById(R.id.movie_series_details_text_runtime_number_of_seasons);
        mRatingBar = view.findViewById(R.id.fragment_movie_series_details_ratingBar);

        mGenres = new TextView[3];
        mGenres[0] = view.findViewById(R.id.movie_series_details_text_genre_1);
        mGenres[1] = view.findViewById(R.id.movie_series_details_text_genre_2);
        mGenres[2] = view.findViewById(R.id.movie_series_details_text_genre_3);
        mSummary = view.findViewById(R.id.movie_series_text_summary);

        mRecyclerView = view.findViewById(R.id.movie_series_details_recyclerview_cast);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,
                false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mPlaceHolderAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void showLoadingUi(boolean isActive) {
        if (!isActive) {
            if (mRecyclerView.getAdapter() != mAdapter) {
                mRecyclerView.setAdapter(mAdapter);
            }
        }
    }

    @Override
    public void showMovieDetails(SimpleMovie movie) {
        String minutes = movie.getRuntime() + " min";
        if (isTablet && isLandScape) {
            Glide.with(mBackDrop)
                    .asBitmap()
                    .load(Config.IMAGE_BACKDROP_URL + movie.getBackdropPath())
                    .into(new BitmapImageViewTarget(mBackDrop) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            super.setResource(resource);
                        }
                    });

            if (movie.getVideos() != null) {
                for (SimpleVideos videos : movie.getVideos()) {
                    if (videos.getSite().equalsIgnoreCase("Youtube")
                            && videos.getType().equalsIgnoreCase("Trailer")) {
                        mVideoId = videos.getKey();
                        break;
                    }
                }
            }
            mPlayTrailer.setVisibility(View.VISIBLE);

        } else {
            mCallback.setAdditionalMovieDetails(movie);
        }

        mTitle.setText(movie.getTitle());
        mReleaseYear.setText(movie.getReleaseDate());

        mRuntimeOrNumberOfSeasons.setText(minutes);
        mSummary.setText(movie.getOverview());
        mRatingBar.setRating((float) ((movie.getVoteAverage() / 2)));
        if (movie.getGenres() != null) {
            int x = Math.min(movie.getGenres().size(), mGenres.length);
            for (int i = 0; i < x; i++) {
                mGenres[i].setText(movie.getGenres().get(i).getName());
            }
            if (x == 1) {
                mGenres[1].setVisibility(View.GONE);
                mGenres[2].setVisibility(View.GONE);
            } else if (x == 2) {
                mGenres[2].setVisibility(View.GONE);
            }
        }
        mAdapter.setCast(movie.getCast());
    }

    @Override
    public void showErrorMessage() {
        String message = getResources().getString(R.string.error_retrieving_content);
        Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void showSeriesDetails(SimpleSeries series) {
        if (isTablet && isLandScape) {
            Glide.with(mBackDrop)
                    .asBitmap()
                    .load(Config.IMAGE_BACKDROP_URL + series.getBackdropPath())
                    .into(new BitmapImageViewTarget(mBackDrop) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            super.setResource(resource);
                        }
                    });

            if (series.getVideos() != null) {
                for (SimpleVideos video : series.getVideos()) {
                    if (video.getSite().equalsIgnoreCase("Youtube")
                            && video.getType().equalsIgnoreCase("Trailer")) {
                        mVideoId = video.getKey();
                        break;
                    }
                }
            }
            mPlayTrailer.setVisibility(View.VISIBLE);

        } else {
            mCallback.setAdditionalSeriesDetails(series);
        }

        mTitle.setText(series.getTitle());
        mReleaseYear.setText(series.getReleaseDate());
        String seasons = series.getNumberOfSeasons() + " Seasons";
        mRuntimeOrNumberOfSeasons.setText(seasons);
        mSummary.setText(series.getOverview());
        mRatingBar.setRating((float) ((series.getVoteAverage() / 2)));
        if (series.getGenres() != null) {
            int x = Math.min(series.getGenres().size(), mGenres.length);
            for (int i = 0; i < x; i++) {
                mGenres[i].setText(series.getGenres().get(i).getName());
            }
            if (x == 1) {
                mGenres[1].setVisibility(View.GONE);
                mGenres[2].setVisibility(View.GONE);
            } else if (x == 2) {
                mGenres[2].setVisibility(View.GONE);
            }
        }
        mAdapter.setCast(series.getCast());
    }

    @Override
    public void onCastClick(SimpleCast cast) {
        Bundle bundle = new Bundle();
        bundle.putLong(FirebaseAnalytics.Param.ITEM_ID, cast.getId());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, cast.getName());
        analytics.logEvent("CastItemSelected", bundle);
    }

    public interface Callback {
        void setAdditionalMovieDetails(SimpleMovie movie);

        void setAdditionalSeriesDetails(SimpleSeries series);
    }
}

