package com.tendai.trailerhub.presentation.error;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tendai.trailerhub.R;
import com.tendai.trailerhub.presentation.BaseFragment;

import java.util.Objects;

public class ErrorFragment extends BaseFragment implements ErrorContract.View {
    private Callback mCallback;
    private ErrorContract.Presenter mPresenter;

    public static ErrorFragment newInstance() {
        return new ErrorFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            mCallback = (Callback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement Callback Interface");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new ErrorPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_something_went_wrong, container, false);
        Button retryButton = view.findViewById(R.id.fragment_something_went_wrong_button);

        retryButton.setOnClickListener(v -> mPresenter.removeErrorFragment());
        analytics.logEvent("ErrorFragmentLaunched", new Bundle());
        return view;
    }

    @Override
    public void removeErrorFragment() {
        mCallback.removeErrorFragment();
    }

    @Override
    public void setPresenter(ErrorContract.Presenter presenter) {
        mPresenter = Objects.requireNonNull(presenter);
    }

    @Override
    public boolean checkConnection() {
        return false;
    }
}

