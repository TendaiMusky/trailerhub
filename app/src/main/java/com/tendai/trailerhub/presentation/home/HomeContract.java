package com.tendai.trailerhub.presentation.home;

import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;
import com.tendai.trailerhub.presentation.BasePresenter;
import com.tendai.trailerhub.presentation.BaseView;

import java.util.List;

public interface HomeContract {

    interface View extends BaseView<Presenter> {
        void showLoadingUi(boolean isActive, String genreId);

        boolean isFragmentActive();

        void showSeries(List<SimpleSeries> series, String genreId);

        void showTrendingMovies(List<SimpleMovie> movies);

        void showMovies(List<SimpleMovie> movies, String genreId);

        void showErrorMessage();

        void showSeriesDetails(SimpleSeries series);

        void showMovieDetails(SimpleMovie movie);
    }

    interface Presenter extends BasePresenter {
        void getSeriesWithGenreId(String genreId);

        void getMovieWithGenreId(String genreId);

        void retrieveDramaMovies();

        void retrieveExcitingShows();

        void retrieveActionMovies();

        void retrieveCrimeShows();

        void retrieveAnimationMovies();

        void retrieveTrendingStuff();

        void showSeriesDetails(SimpleSeries series);

        void showMovieDetails(SimpleMovie movie);

    }
}
