package com.tendai.trailerhub.presentation.series.adapter;

import com.tendai.trailerhub.domain.series.model.SimpleSeries;

public interface SeriesItemClickListener {
    void onSeriesClick(SimpleSeries series);
}
