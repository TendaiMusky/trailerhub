package com.tendai.trailerhub.presentation.utils;

public final class Config {

    public static final String EXTRA_TITLE = "EXTRA_TITLE";
    public static final String SERIES = "Series";
    public static final String MOVIE = "Movie";
    public static final String EXTRA_TRAILER_ID = "EXTRA_TRAILER_ID";
    public static final String API_KEY = "a9e7c415f821126a55689adb36808458";
    public static final String IMAGE_POSTER_URL = "https://image.tmdb.org/t/p/w185";
    public static final String IMAGE_BACKDROP_URL = "https://image.tmdb.org/t/p/w780";
    public static final String CREDITS_VIDEOS = "credits,videos";
    public static final String POPULARITY_DESCENDING = "popularity.desc";
    public static final String MEDIA_TYPE_MOVIE = "movie";
    public static final String NOW_PLAYING = "now_playing";
    public static final String POPULAR = "popular";
    public static final String UPCOMING = "upcoming";
    public static final String TOP_RATED = "top_rated";
    public static final String ON_THE_AIR = "on_the_air";
    public static final String EXTRA_SERIES_ID = "EXTRA_SERIES_ID";
    public static final String EXTRA_MOVIE_ID = "EXTRA_MOVIE_ID";


    private Config() {
    }
}
