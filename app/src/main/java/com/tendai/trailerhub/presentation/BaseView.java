package com.tendai.trailerhub.presentation;

public interface BaseView <T extends BasePresenter>{
    void setPresenter(T presenter);

    boolean checkConnection();
}

