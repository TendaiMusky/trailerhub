package com.tendai.trailerhub.presentation.movie.adapter;

import com.tendai.trailerhub.domain.movie.model.SimpleMovie;

public interface MovieItemClickListener {
    void onMovieClick(SimpleMovie movie);
}
