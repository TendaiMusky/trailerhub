package com.tendai.trailerhub.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.android.material.navigationrail.NavigationRailView;
import com.tendai.trailerhub.R;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;
import com.tendai.trailerhub.presentation.details.DetailsFragment;
import com.tendai.trailerhub.presentation.error.ErrorFragment;
import com.tendai.trailerhub.presentation.home.HomeFragment;
import com.tendai.trailerhub.presentation.more.AboutFragment;
import com.tendai.trailerhub.presentation.more.CreditsFragment;
import com.tendai.trailerhub.presentation.more.MoreFragment;
import com.tendai.trailerhub.presentation.movie.MovieFragment;
import com.tendai.trailerhub.presentation.search.SearchableActivity;
import com.tendai.trailerhub.presentation.series.SeriesFragment;
import com.tendai.trailerhub.presentation.utils.ActivityUtils;

import java.util.Objects;

public class MainActivity extends BaseActivity implements
        BaseFragment.Callback, NavigationBarView.OnItemSelectedListener, DetailsFragment.Callback {

    public static final String FRAGMENT_NAME = "FRAGMENT_NAME";
    public static final String MOVIE_FRAGMENT = "MOVIE_FRAGMENT";
    public static final String SERIES_FRAGMENT = "SERIES_FRAGMENT";
    public static final String MORE_FRAGMENT = "MORE_FRAGMENT";
    public static final String FIRST_LOAD = "FIRST_LOAD";

    private BottomNavigationView mBottomNavigationView;
    private NavigationRailView mNavigationRailView;
    private NestedScrollView mErrorScrollView;

    private boolean mIsHomeFragmentActive;
    private boolean mIsMovieFragmentActive;
    private boolean mIsSeriesFragmentActive;
    private boolean mIsMoreFragmentActive;
    private boolean mFirstLoad = true;
    private DetailsFragment mDetailsFragment;
    private View mDetailsFragmentContainer;
    private View mMainFragmentContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.topAppBar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(getResources().getString(R.string.app_name));

        if (isTablet && isLandScape) {
            mNavigationRailView = findViewById(R.id.navigation_rail);
            mNavigationRailView.setOnItemSelectedListener(this);
            mErrorScrollView = findViewById(R.id.fragment_home_error_scroll_view);
            mDetailsFragmentContainer = findViewById(R.id.activity_main_details_fragment_container);
            mMainFragmentContainer = findViewById(R.id.activity_main_fragment_container);
        } else {
            mBottomNavigationView = findViewById(R.id.bottom_navigation_view);
            mBottomNavigationView.setOnItemSelectedListener(this);
        }


        if (savedInstanceState != null) mFirstLoad = savedInstanceState.getBoolean(FIRST_LOAD);
        initHomeFragment();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            String fragmentName = savedInstanceState.getString(FRAGMENT_NAME);
            if (fragmentName != null && fragmentName.equals(MOVIE_FRAGMENT)) {
                if (isTablet && !isLandScape) {
                    mBottomNavigationView.setSelectedItemId(R.id.menu_navigation_movies);
                } else if (isTablet) {
                    mNavigationRailView.setSelectedItemId(R.id.menu_navigation_movies);
                }
                initMovieFragment();
                mIsHomeFragmentActive = false;
                mIsSeriesFragmentActive = false;
                mIsMoreFragmentActive = false;
            }

            if (fragmentName != null && fragmentName.equals(SERIES_FRAGMENT)) {
                if (isTablet && !isLandScape) {
                    mBottomNavigationView.setSelectedItemId(R.id.menu_navigation_series);
                } else if (isTablet) {
                    mNavigationRailView.setSelectedItemId(R.id.menu_navigation_series);
                }
                initSeriesFragment();
                mIsHomeFragmentActive = false;
                mIsMovieFragmentActive = false;
                mIsMoreFragmentActive = false;
            }

            if (fragmentName != null && fragmentName.equals(MORE_FRAGMENT)) {
                if (isTablet && !isLandScape) {
                    mBottomNavigationView.setSelectedItemId(R.id.menu_navigation_more);
                } else if (isTablet) {
                    mNavigationRailView.setSelectedItemId(R.id.menu_navigation_more);
                }
                initMoreFragment();
                mIsSeriesFragmentActive = false;
                mIsHomeFragmentActive = false;
                mIsMovieFragmentActive = false;
            }
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FIRST_LOAD, mFirstLoad);
        if (mIsMovieFragmentActive) {
            outState.putString(FRAGMENT_NAME, MOVIE_FRAGMENT);
        }
        if (mIsSeriesFragmentActive) {
            outState.putString(FRAGMENT_NAME, SERIES_FRAGMENT);
        }
        if (mIsMoreFragmentActive) {
            outState.putString(FRAGMENT_NAME, MORE_FRAGMENT);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // disable searching when device is tablet and landscape.
        if (isTablet && isLandScape) return false;
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.main_search) {
            startActivity(new Intent(this, SearchableActivity.class));
        }
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_navigation_home) {
            if (!mIsHomeFragmentActive) {
                mIsSeriesFragmentActive = false;
                mIsMovieFragmentActive = false;
                mIsMoreFragmentActive = false;
                initHomeFragment();
            }
            return true;
        } else if (id == R.id.menu_navigation_movies) {
            if (!mIsMovieFragmentActive) {
                mIsHomeFragmentActive = false;
                mIsSeriesFragmentActive = false;
                mIsMoreFragmentActive = false;
                initMovieFragment();
            }
            return true;
        } else if (id == R.id.menu_navigation_series) {
            if (!mIsSeriesFragmentActive) {
                mIsHomeFragmentActive = false;
                mIsMovieFragmentActive = false;
                mIsMoreFragmentActive = false;
                initSeriesFragment();
            }
            return true;
        } else if (id == R.id.menu_navigation_more) {
            if (!mIsMoreFragmentActive) {
                mIsHomeFragmentActive = false;
                mIsMovieFragmentActive = false;
                mIsSeriesFragmentActive = false;
                initMoreFragment();
            }
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (mIsHomeFragmentActive) {
            super.onBackPressed();
        }
        if (mIsMovieFragmentActive || mIsSeriesFragmentActive || mIsMoreFragmentActive) {
            initHomeFragment();
            if (isTablet && isLandScape) {
                mNavigationRailView.setSelectedItemId(R.id.menu_navigation_home);
            } else {
                mBottomNavigationView.setSelectedItemId(R.id.menu_navigation_home);
            }
        }
    }

    @Override
    public void displayErrorFragment() {
        if (isTablet && isLandScape) {
            mErrorScrollView.setVisibility(View.VISIBLE); // TODO: 9/9/2021 just show view on top instead of boilerplate
            mMainFragmentContainer.setVisibility(View.GONE);
            mDetailsFragmentContainer.setVisibility(View.GONE);
            ActivityUtils.replaceFragmentInActivity(
                    getSupportFragmentManager(),
                    ErrorFragment.newInstance(),
                    R.id.fragment_home_error_container);
        } else {
            ActivityUtils.replaceFragmentInActivity(
                    getSupportFragmentManager(),
                    ErrorFragment.newInstance(),
                    R.id.activity_main_fragment_container);
        }
    }

    @Override
    public void removeErrorFragment() {
        if (isTablet && isLandScape) {
            mErrorScrollView.setVisibility(View.GONE);
            mMainFragmentContainer.setVisibility(View.VISIBLE);
            mDetailsFragmentContainer.setVisibility(View.VISIBLE);

            if (mDetailsFragment != null) ActivityUtils.removeFragmentInActivity(getSupportFragmentManager(), mDetailsFragment);
        }
        if (mIsHomeFragmentActive) {
            initHomeFragment();
        } else if (mIsMovieFragmentActive) {
            initMovieFragment();
        } else if (mIsSeriesFragmentActive) {
            initSeriesFragment();
        }
    }

    @Override
    public void displayDetailsFragment(int id, String title) {
        mDetailsFragment = DetailsFragment.newInstance(title, id);
        ActivityUtils.replaceFragmentInActivity(
                getSupportFragmentManager(),
                mDetailsFragment,
                R.id.activity_main_details_fragment_container);

    }

    @Override
    public void displayAboutFragment() {
        ActivityUtils.replaceFragmentInActivity(
                getSupportFragmentManager(),
                AboutFragment.newInstance(),
                R.id.activity_main_details_fragment_container);
    }

    @Override
    public void displayCreditsFragment() {
        ActivityUtils.replaceFragmentInActivity(
                getSupportFragmentManager(),
                CreditsFragment.newInstance(),
                R.id.activity_main_details_fragment_container);
    }

    private void initHomeFragment() {
        if (mFirstLoad) {
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(),
                    HomeFragment.newInstance(),
                    R.id.activity_main_fragment_container);

            mFirstLoad = false;
        } else {
            ActivityUtils.replaceFragmentInActivity(
                    getSupportFragmentManager(),
                    HomeFragment.newInstance(),
                    R.id.activity_main_fragment_container);
        }
        mIsHomeFragmentActive = true;
    }

    private void initMovieFragment() {
        ActivityUtils.replaceFragmentInActivity(
                getSupportFragmentManager(),
                MovieFragment.newInstance(),
                R.id.activity_main_fragment_container);

        mIsMovieFragmentActive = true;
    }

    private void initMoreFragment() {
        ActivityUtils.replaceFragmentInActivity(
                getSupportFragmentManager(),
                MoreFragment.newInstance(),
                R.id.activity_main_fragment_container);

        mIsMoreFragmentActive = true;
    }

    private void initSeriesFragment() {
        ActivityUtils.replaceFragmentInActivity(
                getSupportFragmentManager(),
                SeriesFragment.newInstance(),
                R.id.activity_main_fragment_container);

        mIsSeriesFragmentActive = true;
    }

    @Override
    public void setAdditionalMovieDetails(SimpleMovie movie) {
        //stub
    }

    @Override
    public void setAdditionalSeriesDetails(SimpleSeries series) {
        //stub
    }
}


