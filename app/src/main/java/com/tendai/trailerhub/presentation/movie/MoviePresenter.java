package com.tendai.trailerhub.presentation.movie;

import static com.tendai.trailerhub.domain.movie.usecase.GetMovie.RequestValues.TYPE_GENRE_ID;

import com.tendai.trailerhub.domain.UseCase;
import com.tendai.trailerhub.domain.UseCaseHandler;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.domain.movie.usecase.GetMovie;
import com.tendai.trailerhub.presentation.home.meta.MovieGenre;
import com.tendai.trailerhub.presentation.utils.Config;

import java.util.List;

public class MoviePresenter implements MovieContract.Presenter {
    private static final String TAG = MoviePresenter.class.getSimpleName();

    private final UseCaseHandler mUseCaseHandler;
    private final GetMovie mGetMovie;
    private final MovieContract.View mMovieView;

    MoviePresenter(UseCaseHandler useCaseHandler, GetMovie getMovie, MovieContract.View movieView) {
        mUseCaseHandler = useCaseHandler;
        mGetMovie = getMovie;
        mMovieView = movieView;
        mMovieView.setPresenter(this);
    }

    @Override
    public void start() {
        getMovies(Config.POPULAR);
    }

    @Override
    public boolean checkConnection() {
        return mMovieView.checkConnection();
    }

    @Override
    public void retrieveUpcoming() {
        getMovies(Config.UPCOMING);
    }

    @Override
    public void retrieveNowPlaying() {
        getMovies(Config.NOW_PLAYING);
    }

    @Override
    public void retrieveTopRated() {
        getMovies(Config.TOP_RATED);
    }

    @Override
    public void showMovieDetails(SimpleMovie movie) {
        mMovieView.showMovieDetails(movie);
    }

    @Override
    public void retrieveComedy() {
        getMovies(MovieGenre.COMEDY);
    }

    @Override
    public void retrieveAnimation() {
        getMovies(MovieGenre.ANIMATION);
    }

    private void getMovies(String category) {
        int number = (int) (Math.random() * 10) + 1;

        GetMovie.RequestValues requestValues;

        if (category.equals(MovieGenre.COMEDY) || category.equals(MovieGenre.ANIMATION)) {
            requestValues = new GetMovie.RequestValues(category, 1);
            requestValues.setRequestType(TYPE_GENRE_ID);
        } else {
            requestValues = new GetMovie.RequestValues(category, false, number);
            requestValues.setRequestType(GetMovie.RequestValues.TYPE_CATEGORY);
        }

        mUseCaseHandler.execute(mGetMovie,
                requestValues,
                new UseCase.UseCaseCallback<GetMovie.ResponseValues>() {
                    @Override
                    public void onSuccess(GetMovie.ResponseValues response) {
                        List<SimpleMovie> movies = response.getMovies();
//                        String cat = movies.get(0).getCategory();

                        if (!mMovieView.isFragmentActive()) {
                            return;
                        }
                        mMovieView.showLoadingUi(category, false);
                        mMovieView.showMovies(movies, category);
                    }

                    @Override
                    public void onFailure() {
                        if (!mMovieView.isFragmentActive()) {
                            return;
                        }
//                        mMovieView.showLoadingUi(category, false);
                        mMovieView.showErrorMessage();
                    }
                });
    }
}
// TODO: 9/5/2021 Concurrently fetch stuff instead of fetching one after the other.