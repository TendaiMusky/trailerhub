package com.tendai.trailerhub.presentation.home.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.presentation.movie.adapter.MovieItemClickListener;

import java.util.List;
import java.util.Objects;

public class TrendingAdapter extends RecyclerView.Adapter<TrendingViewHolder> {
    private final MovieItemClickListener mMovieItemClickListener;
    private List<SimpleMovie> mMovies;
    boolean mIsTablet;
    boolean mIsLandScape;

    public TrendingAdapter(List<SimpleMovie> movies, MovieItemClickListener movieItemClickListener,
                           boolean isTablet, boolean isLandScape) {
        setTrendingStuff(movies);
        mMovieItemClickListener = movieItemClickListener;
        mIsTablet = isTablet;
        mIsLandScape = isLandScape;
    }

    public void setTrendingStuff(List<SimpleMovie> movies) {
        mMovies = Objects.requireNonNull(movies);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TrendingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        TrendingPosterItemView itemView =
                new TrendingPosterItemView(LayoutInflater.from(viewGroup.getContext()), viewGroup, mIsTablet, mIsLandScape);
        TrendingViewHolder trendingViewHolder = new TrendingViewHolder(itemView);

        trendingViewHolder.itemView.setOnClickListener(view ->
                mMovieItemClickListener.onMovieClick((SimpleMovie) view.getTag()));
        return trendingViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TrendingViewHolder trendingViewHolder, int i) {
        if (mMovies.size() != 0) {
            SimpleMovie movie = mMovies.get(i);
            if (movie != null) {
                trendingViewHolder.itemView.setTag(movie);
                if (mIsTablet && mIsLandScape) {
                    trendingViewHolder.bindMovie(movie);
                } else {
                    trendingViewHolder.setImage(movie.getBackdropPath());
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if (!mIsTablet || !mIsLandScape) {
            return 3;
        } else {
            return mMovies.size();
        }
    }

    public static class PlaceHolderAdapter extends RecyclerView.Adapter<TrendingViewHolder> {

        @NonNull
        @Override
        public TrendingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new TrendingViewHolder(
                    new TrendingPosterItemView(
                            LayoutInflater.from(viewGroup.getContext()), viewGroup, false, false));
        }

        @Override
        public void onBindViewHolder(@NonNull TrendingViewHolder trendingViewHolder, int i) {
        }

        @Override
        public int getItemCount() {
            return 3;
        }
    }
}
