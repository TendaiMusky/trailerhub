package com.tendai.trailerhub.presentation.home.meta;

public class SeriesGenre {
     private SeriesGenre() {
     }
    public static final String SCI_FI = "10765";
    public static final String FAMILY = "10751";
    public static final String MYSTERY = "9648";
    public static final String ACTION_ADVENTURE = "10759";
    public static final String CRIME = "80";
 }
