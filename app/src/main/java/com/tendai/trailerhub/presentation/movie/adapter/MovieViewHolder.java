package com.tendai.trailerhub.presentation.movie.adapter;

import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.tendai.trailerhub.R;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.presentation.utils.Config;

class MovieViewHolder extends RecyclerView.ViewHolder {

    private final ImageView mPoster;
    private final TextView mTitle;
    private final ObjectAnimator mAnimation;
    private TextView mSummary;

    MovieViewHolder(@NonNull View itemView, boolean isTablet, boolean isLandScape) {
        super(itemView);

        mPoster = itemView.findViewById(R.id.item_movie_series_image_poster);
        mTitle = itemView.findViewById(R.id.item_movie_series_text_title);
        if (isTablet && isLandScape) mSummary = itemView.findViewById(R.id.item_movie_series_text_summary);

        ObjectAnimator animator = ObjectAnimator.ofFloat(
                itemView,
                View.ALPHA,
                1f, 0f, 1f);
        animator.setRepeatCount(ObjectAnimator.INFINITE);
        animator.setDuration(3000L);
        mAnimation = animator;
    }

    void showPlaceHolderAnimation() {
        ObjectAnimator animator = mAnimation;
        animator.setCurrentPlayTime((SystemClock.elapsedRealtime() - (long) this.getAdapterPosition() * 30) % 100L);
        animator.start();
        mPoster.setImageResource(R.drawable.image_trending_placeholder);
        mTitle.setText(null);
        mTitle.setBackgroundResource(R.drawable.text_title_placeholder);
    }

    void bindData(SimpleMovie movie) {
        Glide.with(mPoster)
                .asBitmap()
                .load(Config.IMAGE_POSTER_URL + movie.getPosterPath())
                .into(new BitmapImageViewTarget(mPoster) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        mAnimation.end();
                        mPoster.setImageBitmap(resource);
                        mTitle.setText(movie.getTitle());
                        if (mSummary != null) mSummary.setText(movie.getOverview());
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        mAnimation.end();
                        mPoster.setScaleType(ImageView.ScaleType.FIT_XY);
                        mPoster.setImageResource(R.drawable.ic_place_holder);
                    }
                });

    }
}
