package com.tendai.trailerhub.presentation.more;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tendai.trailerhub.R;
import com.tendai.trailerhub.presentation.BaseFragment;

public class MoreFragment extends BaseFragment {

    public static MoreFragment newInstance() {
        return new MoreFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            mCallback = (Callback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement Callback Interface");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        TextView credits = view.findViewById(R.id.fragment_more_text_view_credits);
        TextView about = view.findViewById(R.id.fragment_more_text_view_about);

        credits.setOnClickListener(v -> {
            if (isTablet && isLandScape) {
                mCallback.displayCreditsFragment();
            } else {
                startActivity(new Intent(getActivity(), CreditsActivity.class));
            }
        });

        about.setOnClickListener(v -> {
            if (isTablet && isLandScape) {
                mCallback.displayAboutFragment();
            } else {
                startActivity(new Intent(getActivity(), AboutActivity.class));
            }
        });
        analytics.logEvent("MoreFragmentLaunched", new Bundle());
        return view;
    }
}
