package com.tendai.trailerhub.data;

import android.util.Log;

import androidx.annotation.NonNull;

import com.tendai.trailerhub.data.model.Series;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SeriesRepository implements SeriesDataSource {
    private static final String TAG = SeriesRepository.class.getSimpleName();
    private static SeriesRepository INSTANCE;

    private final SeriesDataSource mRemoteSeriesDataSource;
    private boolean mIsCacheDirty = true;
    private boolean mIsDetailsCacheDirty;
    private List<String> mGenres;
    private List<String> mCategories;

    Map<Integer, Series> cachedSeriesById;
    private Map<Integer, Series> cachedSeriesDetails;

    private SeriesRepository(SeriesDataSource remoteSeriesDataSource) {
        this.mRemoteSeriesDataSource = remoteSeriesDataSource;
        mGenres = new ArrayList<>(0);
        mCategories = new ArrayList<>(0);
    }

    @Override
    public void getSeriesDetails(int seriesId, @NonNull CallBacks.GetSeriesDetails callback) {
        if (!mIsDetailsCacheDirty && cachedSeriesDetails != null) {
            Series series = getSeriesById(seriesId);
            if (series != null) {
                callback.onSuccess(series);
                return;
            }

        }
        mRemoteSeriesDataSource.getSeriesDetails(seriesId, new CallBacks.GetSeriesDetails() {
            @Override
            public void onSuccess(Series series) {
                refreshCache(series);
                callback.onSuccess(series);
            }

            @Override
            public void onFailure() {
                callback.onFailure();
            }
        });
    }

    @Override
    public void getSeriesByCategory(String category, int pageNumber, @NonNull CallBacks.GetSeries callback) {
        if (mCategories.size() == 0) {
            mCategories.add(category);
        }
        if (mCategories.contains(category)) {
            if (!mIsCacheDirty && cachedSeriesById != null) {
                List<Series> seriesList = getSeriesWithCategory(category);
                if (!seriesList.isEmpty()) {
                    callback.onSuccess(seriesList);
                    return;
                }
            }
        } else {
            mCategories.add(category);
        }

        mRemoteSeriesDataSource.getSeriesByCategory(category, pageNumber, new CallBacks.GetSeries() {
            @Override
            public void onSuccess(List<Series> series) {
                refreshCache(series);
                callback.onSuccess(series);
            }

            @Override
            public void onFailure() {
                callback.onFailure();
            }
        });
    }

    @Override
    public void getSeriesByGenre(String genreName, int pageNumber, @NonNull CallBacks.GetSeries callback) {
        if (mGenres.size() == 0) {
            mGenres.add(genreName);
        }
        if (mGenres.contains(genreName)) {
            if (!mIsCacheDirty && cachedSeriesById != null) {
                int genreId = Integer.parseInt(genreName);
                List<Series> seriesList = getSeriesWithGenre(genreId);
                if (!seriesList.isEmpty()) {
                    callback.onSuccess(seriesList);
                    return;
                }
            }
        } else {
            mGenres.add(genreName);
        }


        mRemoteSeriesDataSource.getSeriesByGenre(genreName, pageNumber, new CallBacks.GetSeries() {
            @Override
            public void onSuccess(List<Series> series) {
                refreshCache(series);
                callback.onSuccess(series);
            }

            @Override
            public void onFailure() {
                callback.onFailure();
            }
        });
    }

    @Override
    public void refreshSeries() {
        mIsDetailsCacheDirty = true;
//        if (cachedSeriesById != null) cachedSeriesById.clear();
    }

    public static SeriesRepository getInstance(SeriesDataSource remoteSeriesDataSource) {
        if (INSTANCE == null) {
            INSTANCE = new SeriesRepository(remoteSeriesDataSource);
        }
        return INSTANCE;
    }

    static void destroyInstance() {
        INSTANCE = null;
    }

    private Series getSeriesById(int seriesId) {
        if (cachedSeriesDetails == null || cachedSeriesDetails.isEmpty()) {
            return null;
        } else {
            return cachedSeriesDetails.get(seriesId);
        }
    }

    private List<Series> getSeriesWithCategory(String category) {
        List<Series> seriesList = new ArrayList<>();
        for (Series series : cachedSeriesById.values()) {
            if (series.getCategory() != null) {
                for (int i = 0; i < series.getCategory().length; i++) {
                    if (series.getCategory()[i] != null) {
                        if (series.getCategory()[i].equalsIgnoreCase(category)) {
                            seriesList.add(series);
                        }
                    }
                }
            }
        }
        return seriesList;
    }

    private List<Series> getSeriesWithGenre(int genreId) {
        List<Series> seriesList = new ArrayList<>();
        for (Series series : cachedSeriesById.values()) {
            if (series.getGenreIds() != null) {
                for (int i = 0; i < series.getGenreIds().length; i++) {
                    if (series.getGenreIds()[i] == genreId) {
                        seriesList.add(series);
                    }
                }
            }
        }
        return seriesList;
    }

    // TODO: 9/21/2021 fix bug whereby series with similar ids and multiple categories are overridden
    private void refreshCache(List<Series> series) {
        if (cachedSeriesById == null) {
            cachedSeriesById = Collections.synchronizedMap(new LinkedHashMap<>());
//            cachedSeriesById = new LinkedHashMap<>();
        }
        for (Series series1 : series) {
            if (!cachedSeriesById.containsKey(series1.getId())) {
                cachedSeriesById.put(series1.getId(), series1);
            } else if (cachedSeriesById.containsKey(series1.getId())) {
                cachedSeriesById.remove(series1.getId());
                cachedSeriesById.put(series1.getId(), series1);
            }
        }
        mIsCacheDirty = false;
    }

    private void refreshCache(Series series) {
        if (cachedSeriesDetails == null) {
            cachedSeriesDetails = Collections.synchronizedMap(new LinkedHashMap<>());
//            cachedSeriesById = new LinkedHashMap<>();
        }
        if (!cachedSeriesDetails.containsKey(series.getId())) {
            cachedSeriesDetails.put(series.getId(), series);
        }
        mIsDetailsCacheDirty = false;
    }

}