package com.tendai.trailerhub.data.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Movie {
    @Expose
    @SerializedName("id")
    private int mId;

    @Expose
    @SerializedName("title")
    private String mTitle;

    @Expose
    @SerializedName("overview")
    private String mOverview;

    @Expose
    @SerializedName("runtime")
    private int mRuntime;

    @Expose
    @SerializedName("vote_average")
    private double mRating;

    @Expose
    @SerializedName("poster_path")
    private String mPosterPath;

    @Expose
    @SerializedName("backdrop_path")
    private String mBackdropPath;

    @Expose
    @SerializedName("release_date")
    private String mReleaseDate;

    @Expose
    @SerializedName("genres")
    private List<Genres> mMovieGenres;

    @Expose
    @SerializedName("genre_ids")
    private int [] mGenreIds;

    @Expose
    @SerializedName("credits")
    private Credits mCredits;

    @Expose
    @SerializedName("videos")
    private VideoResponse mVideos;

    private String[] mCategory;

    public Movie() {
        mCategory = new String[4];
    }

    public Movie(int id, String title, String overview, int runtime, double rating, String posterPath, String backdropPath, String releaseDate, int[] genreIds, Credits credits, String[] category) {
        mId = id;
        mTitle = title;
        mOverview = overview;
        mRuntime = runtime;
        mRating = rating;
        mPosterPath = posterPath;
        mBackdropPath = backdropPath;
        mReleaseDate = releaseDate;
        mGenreIds = genreIds;
        mCredits = credits;
        mCategory = category;
    }

    public Movie(int id, String title, String overview, int runtime, double rating, String posterPath, String backdropPath, String releaseDate, int[] genreIds, Credits credits, String category) {
        mId = id;
        mTitle = title;
        mOverview = overview;
        mRuntime = runtime;
        mRating = rating;
        mPosterPath = posterPath;
        mBackdropPath = backdropPath;
        mReleaseDate = releaseDate;
        mGenreIds = genreIds;
        mCredits = credits;
        mCategory[0] = category;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getOverview() {
        return mOverview;
    }

    public void setOverview(String overview) {
        this.mOverview = overview;
    }

    public int getRuntime() {
        return mRuntime;
    }

    public double getRating() {
        return mRating;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public String getBackdropPath() {
        return mBackdropPath;
    }

    public String getReleaseDate() {
        return mReleaseDate;
    }

    public List<Genres> getMovieGenres() {
        return mMovieGenres;
    }

    public Credits getCredits() {
        return mCredits;
    }

    public int[] getGenreIds() {
        return mGenreIds;
    }

    public String[] getCategory() {
        return mCategory;
    }

    public void setCategory(int position, String category) {
        mCategory[position] = category;
    }

    public VideoResponse getVideoResponse() {
        return mVideos;
    }

}

