package com.tendai.trailerhub.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Series {

    @Expose
    @SerializedName("id")
    private int mId;

    @Expose
    @SerializedName("name")
    private String mTitle;

    @Expose
    @SerializedName("overview")
    private String mOverview;

    @Expose
    @SerializedName("number_of_seasons")
    private int mNumberOfSeasons;

    @Expose
    @SerializedName("vote_average")
    private double mRating;

    @Expose
    @SerializedName("poster_path")
    private String mPosterPath;

    @Expose
    @SerializedName("backdrop_path")
    private String mBackdropPath;

    @Expose
    @SerializedName("first_air_date")
    private String mAirDate;

    @Expose
    @SerializedName("genres")
    private List<Genres> mSeriesGenres;

    @Expose
    @SerializedName("credits")
    private Credits mCredits;

    @Expose
    @SerializedName("genre_ids")
    private int[] mGenreIds;

    @Expose
    @SerializedName("videos")
    private VideoResponse mVideoResponse;

    private String[] mCategory;

    public Series() {
        mCategory = new String[3];
    }

    public Series(int id, String title, String overview, int numberOfSeasons, double rating, String posterPath, String backdropPath, String airDate, List<Genres> mSeriesGenres, Credits credits) {
        mId = id;
        mTitle = title;
        mOverview = overview;
        mNumberOfSeasons = numberOfSeasons;
        mRating = rating;
        mPosterPath = posterPath;
        mBackdropPath = backdropPath;
        mAirDate = airDate;
        this.mSeriesGenres = mSeriesGenres;
        mCredits = credits;
    }

    public int getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getOverview() {
        return mOverview;
    }

    public void setOverview(String overview) {
        this.mOverview = overview;
    }

    public int getNumberOfSeasons() {
        return mNumberOfSeasons;
    }

    public double getRating() {
        return mRating;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public String getBackdropPath() {
        return mBackdropPath;
    }

    public String getAirDate() {
        return mAirDate;
    }

    public void setId(int id) {
        mId = id;
    }

    public List<Genres> getSeriesGenres() {
        return mSeriesGenres;
    }

    public Credits getCredits() {
        return mCredits;
    }


    public int[] getGenreIds() {
        return mGenreIds;
    }

    public String[] getCategory() {
        return mCategory;
    }

    public void setCategory(int position, String category) {
        mCategory[position] = category;
    }

    public VideoResponse getVideoResponse() {
        return mVideoResponse;
    }
}
