package com.tendai.trailerhub.data;

import androidx.annotation.NonNull;

public interface MovieDataSource {

    void getMovieDetails(int movieId, @NonNull CallBacks.GetMovieDetails callback);

    void getMovieByCategory(String category, int pageNumber, @NonNull CallBacks.GetMovies callback);

    void getMovieByGenre(String genreName, int pageNumber, @NonNull CallBacks.GetMovies callback);

    void refreshMovies();
}
