package com.tendai.trailerhub.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Cast {

    @Expose
    @SerializedName("id")
    private long mId;

    @Expose
    @SerializedName("name")
    private String mName;

    @Expose
    @SerializedName("profile_path")
    private String mProfilePath;

    @Expose
    @SerializedName("character")
    private String mCharacter;

    public Cast(long id, String name, String profilePath, String character) {
        this.mId = id;
        this.mName = name;
        this.mProfilePath = profilePath;
        this.mCharacter = character;
    }

    public long getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getProfilePath() {
        return mProfilePath;
    }

    public void setProfilePath(String mProfilePath) {
        this.mProfilePath = mProfilePath;
    }

    public String getCharacter() {
        return mCharacter;
    }

    public void setCharacter(String character) {
        this.mCharacter = character;
    }
}
