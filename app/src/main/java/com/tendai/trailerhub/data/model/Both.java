package com.tendai.trailerhub.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Both {
    @Expose
    @SerializedName("id")
    private int mId;

    @Expose
    @SerializedName("title")
    private String mMovieTitle;

    @Expose
    @SerializedName("name")
    private String mSeriesTitle;

    @Expose
    @SerializedName("poster_path")
    private String mPosterPath;

    @Expose
    @SerializedName("backdrop_path")
    private String mBackdropPath;

    @Expose
    @SerializedName("media_type")
    private String mMediaType;

    public Both() {
    }

    public Both(int id, String movieTitle, String seriesTitle, String posterPath, String mediaType) {
        mId = id;
        mMovieTitle = movieTitle;
        mSeriesTitle = seriesTitle;
        mPosterPath = posterPath;
        mMediaType = mediaType;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getMovieTitle() {
        return mMovieTitle;
    }

    public String getSeriesTitle() {
        return mSeriesTitle;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public String getMediaType() {
        return mMediaType;
    }

    public String getBackdropPath() {
        return mBackdropPath;
    }
}