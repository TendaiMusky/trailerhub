package com.tendai.trailerhub.data;

import com.tendai.trailerhub.data.model.Both;
import com.tendai.trailerhub.data.model.Movie;
import com.tendai.trailerhub.data.model.Series;

import java.util.List;

public interface CallBacks {

    void onFailure();

    interface GetSearch  extends CallBacks{
        void onSuccess(List<Both> searchedMovies);
    }

    interface GetMovies extends CallBacks {
        void onSuccess(List<Movie> movies);
    }

    interface GetMovieDetails extends CallBacks {
        void onSuccess(Movie movie);
    }

    //Series Callbacks
    interface GetSeries extends CallBacks {
        void onSuccess(List<Series> series);
    }

    interface GetSeriesDetails extends CallBacks {
        void onSuccess(Series series);
    }

    //Both
    interface GetTrending extends CallBacks {
        void onSuccess(List<Movie> movies);
    }
}
