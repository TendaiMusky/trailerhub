package com.tendai.trailerhub.data.remote;

import androidx.annotation.NonNull;

import com.tendai.trailerhub.data.CallBacks;
import com.tendai.trailerhub.data.DataSource;
import com.tendai.trailerhub.data.model.Both;
import com.tendai.trailerhub.data.model.BothResponse;
import com.tendai.trailerhub.data.model.Movie;
import com.tendai.trailerhub.data.model.MovieListResponse;
import com.tendai.trailerhub.data.remote.api.MovieSeriesWebService;
import com.tendai.trailerhub.presentation.utils.Config;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class RemoteDataSource implements DataSource {
    private static final String TAG = RemoteDataSource.class.getSimpleName();
    private static volatile RemoteDataSource INSTANCE;
    private final MovieSeriesWebService mWebService;

    private RemoteDataSource(MovieSeriesWebService webService) {
        mWebService = webService;
    }

    @Override
    public void getTrending(String mediaType, String timeWindow,  @NonNull CallBacks.GetTrending callback) {

        Call<MovieListResponse> call = mWebService.getTrending(mediaType, timeWindow, Config.API_KEY);
        try {
            Response<MovieListResponse> response = call.execute();
            if (response.isSuccessful() && response.body() != null) {
                List<Movie> movies = response.body().getResults();
                callback.onSuccess(movies);
            } else {
                callback.onFailure();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            callback.onFailure();
        }
    }

    @Override
    public void refreshTrending() {
        //Not necessary handled by repository;
    }

    @Override
    public void getSearchedMovies(String searchQuery, @NonNull CallBacks.GetSearch callback) {
        Call<BothResponse> call = mWebService.getSearched(searchQuery,Config.API_KEY,1);
        try {
            Response<BothResponse> response = call.execute();
            if (response.isSuccessful() && response.body() != null) {
                List<Both> movieSeriesList = response.body().getResults();
                callback.onSuccess(movieSeriesList);
            } else {
                callback.onFailure();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            callback.onFailure();
        }
    }

    public static RemoteDataSource getInstance(MovieSeriesWebService webService) {
        if (INSTANCE == null) {
            INSTANCE = new RemoteDataSource(webService);
        }
        return INSTANCE;
    }

}
