package com.tendai.trailerhub.data;

import androidx.annotation.NonNull;

public interface DataSource {

    void getTrending(String mediaType,
                     String timeWindow,
                     @NonNull CallBacks.GetTrending callback);


    void refreshTrending();

    void getSearchedMovies(String searchQuery, @NonNull CallBacks.GetSearch callback);
}
