package com.tendai.trailerhub.data.remote;

import android.util.Log;

import androidx.annotation.NonNull;

import com.tendai.trailerhub.data.CallBacks;
import com.tendai.trailerhub.data.SeriesDataSource;
import com.tendai.trailerhub.data.model.Series;
import com.tendai.trailerhub.data.model.SeriesListResponse;
import com.tendai.trailerhub.data.remote.api.MovieSeriesWebService;
import com.tendai.trailerhub.presentation.utils.Config;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class RemoteSeriesDataSource implements SeriesDataSource {
    private static final String TAG = RemoteSeriesDataSource.class.getSimpleName();

    private static volatile RemoteSeriesDataSource INSTANCE;
    private final MovieSeriesWebService mWebService;

    private RemoteSeriesDataSource(MovieSeriesWebService webService) {
        mWebService = webService;
        new Series();
    }

    @Override
    public void getSeriesDetails(int seriesId, @NonNull CallBacks.GetSeriesDetails callback) {
        Call<Series> call = mWebService.getSeriesDetails(seriesId, Config.API_KEY, Config.CREDITS_VIDEOS);
        try {
            Response<Series> response = call.execute();
            if (response.isSuccessful() && response.body() != null) {
                Series series = response.body();
                callback.onSuccess(series);
            } else {
                callback.onFailure();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            callback.onFailure();
        }
    }

    @Override
    public void getSeriesByCategory(String category, int pageNumber, @NonNull CallBacks.GetSeries callback) {
        Call<SeriesListResponse> call;
        switch (category) {
            case Config.POPULAR:
                call = mWebService.getPopularSeries(Config.API_KEY,pageNumber);
                break;
            case Config.TOP_RATED:
                call = mWebService.getTopRatedSeries(Config.API_KEY,pageNumber);
                break;
            case Config.ON_THE_AIR:
                call = mWebService.getSeriesOnAir(Config.API_KEY,pageNumber);
                break;

            default:
                return;
        }
        try {
            Response<SeriesListResponse> response = call.execute();
            if (response.isSuccessful() && response.body() != null) {
                List<Series> seriesList = response.body().getResults();
                for (int i = 0; i < seriesList.size(); i++) {
                    switch (category) {
                        case Config.POPULAR:
                            seriesList.get(i).setCategory(0, category);
                            break;
                        case Config.ON_THE_AIR:
                            seriesList.get(i).setCategory(1, category);
                            break;
                        case Config.TOP_RATED:
                            seriesList.get(i).setCategory(2, category);
                            break;
                    }
                }
                callback.onSuccess(seriesList);
            } else {
                callback.onFailure();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            callback.onFailure();
        }
    }

    @Override
    public void getSeriesByGenre(String genreName, int pageNumber, @NonNull CallBacks.GetSeries callback) {
        Call<SeriesListResponse> call =
                mWebService.getSeriesByGenre(Config.API_KEY, genreName, Config.POPULARITY_DESCENDING, 1);

        try {
            Response<SeriesListResponse> response = call.execute();
            if (response.isSuccessful() && response.body() != null) {
                List<Series> seriesList = response.body().getResults();
                callback.onSuccess(seriesList);
            } else {
                callback.onFailure();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            callback.onFailure();
        }
    }

    @Override
    public void refreshSeries() {

    }

    public static RemoteSeriesDataSource getInstance(MovieSeriesWebService webService) {
        if (INSTANCE == null) {
            INSTANCE = new RemoteSeriesDataSource(webService);
        }
        return INSTANCE;
    }
}