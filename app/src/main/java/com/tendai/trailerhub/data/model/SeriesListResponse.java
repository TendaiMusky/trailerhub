package com.tendai.trailerhub.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SeriesListResponse {
    @Expose
    @SerializedName("page")
    private int mPage;

    @Expose
    @SerializedName("results")
    private List<Series> mResults;

    @Expose
    @SerializedName("total_results")
    private int mTotalResults;

    @Expose
    @SerializedName("total_pages")
    private int mTotalPages;

    public int getPage() {
        return mPage;
    }

    public void setPage(int page) {
        this.mPage = page;
    }

    public List<Series> getResults() {
        return mResults;
    }

    public void setResults(List<Series> results) {
        this.mResults = results;
    }

    public int getTotalResults() {
        return mTotalResults;
    }

    public void setTotalResults(int totalResults) {
        this.mTotalResults = totalResults;
    }

    public int getTotalPages() {
        return mTotalPages;
    }

    public void setTotalPages(int totalPages) {
        this.mTotalPages = totalPages;
    }
}
