package com.tendai.trailerhub.data;

import androidx.annotation.NonNull;

public interface SeriesDataSource {

    //Remote has genres , local does not have genres
    // Handle Only retrieval of certain fields in callback parameters.Overloading
    // Mapping and My models find a work around.
    void getSeriesDetails(int seriesId, @NonNull CallBacks.GetSeriesDetails callback);

    void getSeriesByCategory(String category, int pageNumber, @NonNull CallBacks.GetSeries callback);

    void getSeriesByGenre(String genreName, int pageNumber, @NonNull CallBacks.GetSeries callback);


    void refreshSeries();

}
