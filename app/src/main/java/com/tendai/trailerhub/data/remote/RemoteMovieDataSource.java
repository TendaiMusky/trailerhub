package com.tendai.trailerhub.data.remote;

import android.util.Log;

import androidx.annotation.NonNull;

import com.tendai.trailerhub.data.CallBacks;
import com.tendai.trailerhub.data.MovieDataSource;
import com.tendai.trailerhub.data.model.Movie;
import com.tendai.trailerhub.data.model.MovieListResponse;
import com.tendai.trailerhub.data.remote.api.MovieSeriesWebService;
import com.tendai.trailerhub.presentation.utils.Config;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class RemoteMovieDataSource implements MovieDataSource {
    private static final String TAG = RemoteMovieDataSource.class.getSimpleName();
    private static volatile RemoteMovieDataSource INSTANCE;

    private final MovieSeriesWebService mWebService;

    private RemoteMovieDataSource(MovieSeriesWebService webService) {
        mWebService = webService;
        new Movie();
    }

    @Override
    public void getMovieDetails(int movieId, @NonNull CallBacks.GetMovieDetails callback) {
        Call<Movie> call = mWebService.getMovieDetails(movieId, Config.API_KEY, Config.CREDITS_VIDEOS);
        try {
            Response<Movie> response = call.execute();
            if (response.isSuccessful() && response.body() != null) {
                Movie movie = response.body();
                callback.onSuccess(movie);
            } else {
                callback.onFailure();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            callback.onFailure();
        }
    }

    @Override
    public void getMovieByCategory(String category, int pageNumber, @NonNull CallBacks.GetMovies callback) {
        Call<MovieListResponse> call;
        switch (category) {
            case Config.POPULAR:
                call = mWebService.getPopularMovies(Config.API_KEY, pageNumber);
                break;
            case Config.TOP_RATED:
                call = mWebService.getTopRatedMovies(Config.API_KEY, pageNumber);
                break;
            case Config.UPCOMING:
                call = mWebService.getUpcomingMovies(Config.API_KEY, pageNumber);
                break;
            case Config.NOW_PLAYING:
                call = mWebService.getNowPlaying(Config.API_KEY, pageNumber);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + category);
        }

        try {
            Response<MovieListResponse> response = call.execute();
            if (response.isSuccessful() && response.body() != null) {
                List<Movie> movieList = response.body().getResults();
                for (int i = 0; i < movieList.size(); i++) {
                    switch (category) {
                        case Config.POPULAR:
                            movieList.get(i).setCategory(0, category);
                            break;
                        case Config.UPCOMING:
                            movieList.get(i).setCategory(1, category);
                            break;
                        case Config.TOP_RATED:
                            movieList.get(i).setCategory(2, category);
                            break;
                        case Config.NOW_PLAYING:
                            movieList.get(i).setCategory(3, category);
                    }
                }

                if (movieList.size() != 0) {
                    callback.onSuccess(movieList);
                }

            } else {
                callback.onFailure();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            callback.onFailure();
        }

    }


    @Override
    public void getMovieByGenre(String genreName, int pageNumber, @NonNull CallBacks.GetMovies callback) {
        Call<MovieListResponse> call =
                mWebService.getMoviesByGenre(Config.API_KEY, genreName, Config.POPULARITY_DESCENDING, 1);

        try {
            Response<MovieListResponse> response = call.execute();
            if (response.isSuccessful() && response.body() != null) {
                List<Movie> movieList = response.body().getResults();

                callback.onSuccess(movieList);
            } else {

                callback.onFailure();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            callback.onFailure();
        }
    }

    @Override
    public void refreshMovies() {
        // Not necessary repository handles data refreshing the cache.
    }

    public static RemoteMovieDataSource getInstance(MovieSeriesWebService webService) {
        if (INSTANCE == null) {
            INSTANCE = new RemoteMovieDataSource(webService);
        }
        return INSTANCE;
    }

}