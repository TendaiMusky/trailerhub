package com.tendai.trailerhub.data;

import androidx.annotation.NonNull;

import com.tendai.trailerhub.data.model.Both;
import com.tendai.trailerhub.data.model.Movie;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Repository implements DataSource {
    private static Repository INSTANCE;

    private boolean mIsCacheDirty = true;
    private final DataSource mDataSource;

    @SuppressWarnings("WeakerAccess")
    Map<Integer, Movie> cachedMovies;

    private Repository(DataSource dataSource) {
        mDataSource = dataSource;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    @Override
    public void getTrending(String mediaType,
                            String timeWindow,
                            @NonNull CallBacks.GetTrending callback) {
        if (!mIsCacheDirty && cachedMovies != null) {
            callback.onSuccess(new ArrayList<>(cachedMovies.values()));
            return;
        }

        if (mIsCacheDirty) {
            getTrendingFromRemoteDataSource(mediaType, timeWindow, callback);
        }
    }

    @Override
    public void refreshTrending() {
        mIsCacheDirty = true;
    }

    @Override
    public void getSearchedMovies(String searchQuery, @NonNull CallBacks.GetSearch callback) {
        mDataSource.getSearchedMovies(searchQuery, new CallBacks.GetSearch() {
            @Override
            public void onSuccess(List<Both> searchedMovies) {
                callback.onSuccess(searchedMovies);
            }

            @Override
            public void onFailure() {
                callback.onFailure();
            }
        });
    }

    public static Repository getInstance(DataSource dataSource) {
        if (INSTANCE == null) {
            INSTANCE =
                    new Repository(dataSource);
        }
        return INSTANCE;
    }

    private void getTrendingFromRemoteDataSource(String mediaType, String timeFrame,
                                                 CallBacks.GetTrending callback) {
        mDataSource.getTrending(mediaType, timeFrame, new CallBacks.GetTrending() {
                    @Override
                    public void onSuccess(List<Movie> movies) {
                        refreshCache(movies);
                        callback.onSuccess(movies);
                    }

                    @Override
                    public void onFailure() {
                        callback.onFailure();
                    }
                }
        );
    }


    private void refreshCache(List<Movie> movies) {
        if (cachedMovies == null) {
            cachedMovies = new LinkedHashMap<>();
        }
        cachedMovies.clear();
        for (Movie movie : movies) {
            cachedMovies.put(movie.getId(), movie);
        }
        mIsCacheDirty = false;
    }

}
