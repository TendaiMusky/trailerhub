package com.tendai.trailerhub.data.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Credits {
    @Expose
    @SerializedName("id")
    private long mId;

    @Expose
    @SerializedName("cast")
    private List<Cast> mCast;

    public long getId() {
        return mId;
    }

    public void setId(long mId) {
        this.mId = mId;
    }

    public List<Cast> getCast() {
        return mCast;
    }

    public void setCast(List<Cast> mCast) {
        this.mCast = mCast;
    }
}
