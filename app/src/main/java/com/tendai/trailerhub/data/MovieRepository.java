package com.tendai.trailerhub.data;

import androidx.annotation.NonNull;

import com.tendai.trailerhub.data.model.Movie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class MovieRepository implements MovieDataSource {
    private static final String TAG = MovieRepository.class.getSimpleName();
    private static MovieRepository INSTANCE;

    private final MovieDataSource mRemoteMovieDataSource;
    private boolean mIsCacheDirty;
    private boolean mIsDetailsCacheDirty;
    private List<String> mGenres;
    private List<String> mCategories;

    Map<Integer, Movie> cachedMoviesById;
    private Map<Integer, Movie> cachedMovieDetails;

    private MovieRepository(MovieDataSource remoteMovieDataSource) {
        this.mRemoteMovieDataSource = remoteMovieDataSource;
        mGenres = new ArrayList<>(0);
        mCategories = new ArrayList<>(0);
    }

    @Override
    public void getMovieDetails(int movieId, @NonNull CallBacks.GetMovieDetails callback) {
        if (!mIsDetailsCacheDirty && cachedMovieDetails != null) {
            Movie movie = getMovieDetails(movieId);
            if (movie != null) {
                callback.onSuccess(movie);
                return;
            }
        }

        mRemoteMovieDataSource.getMovieDetails(movieId, new CallBacks.GetMovieDetails() {
            @Override
            public void onSuccess(Movie movie) {
                refreshCache(movie);
                callback.onSuccess(movie);
            }

            @Override
            public void onFailure() {
                callback.onFailure();
            }
        });
    }

    @Override
    public void getMovieByCategory(String category, int pageNumber, @NonNull CallBacks.GetMovies callback) {
        if (mCategories.size() == 0) {
            mCategories.add(category);
        }
        if (mCategories.contains(category)) {
            if (!mIsCacheDirty && cachedMoviesById != null) {
                List<Movie> movieList = getMoviesWithCategory(category);
                if (!movieList.isEmpty()) {
                    callback.onSuccess(movieList);
                    return;
                }
            }
        } else {
            mCategories.add(category);
        }

        mRemoteMovieDataSource.getMovieByCategory(category, pageNumber, new CallBacks.GetMovies() {
            @Override
            public void onSuccess(List<Movie> movies) {
                refreshCache(movies);
                callback.onSuccess(movies);
            }

            @Override
            public void onFailure() {
                callback.onFailure();
            }
        });
    }

    @Override
    public void getMovieByGenre(String genreName, int pageNumber, @NonNull CallBacks.GetMovies callback) {

        if (mGenres.size() == 0) {
            mGenres.add(genreName);
        }
        if (mGenres.contains(genreName)) {
            if (!mIsCacheDirty && cachedMoviesById != null) {
                int genreId = Integer.parseInt(genreName);
                List<Movie> movieList = getMoviesWithGenre(genreId);
                if (!movieList.isEmpty()) {
                    callback.onSuccess(movieList);
                    return;
                }
            }
        } else {
            mGenres.add(genreName);
        }

        mRemoteMovieDataSource.getMovieByGenre(genreName, pageNumber, new CallBacks.GetMovies() {
            @Override
            public void onSuccess(List<Movie> movies) {
                refreshCache(movies);
                callback.onSuccess(movies);
            }

            @Override
            public void onFailure() {
                callback.onFailure();

            }
        });
    }

    @Override
    public void refreshMovies() {
//        mIsCacheDirty = true;
        mIsDetailsCacheDirty = true;
//        if (cachedMoviesById != null) cachedMoviesById.clear();
    }

    public static MovieRepository getInstance(MovieDataSource remoteMovieDataSource) {
        if (INSTANCE == null) {
            INSTANCE = new MovieRepository(remoteMovieDataSource);
        }
        return INSTANCE;
    }

    static void destroyInstance() {

        INSTANCE = null;
    }


    private Movie getMovieDetails(int movieId) {
        if (cachedMovieDetails == null || cachedMovieDetails.isEmpty()) {
            return null;
        } else {
            return cachedMovieDetails.get(movieId);
        }
    }

    private List<Movie> getMoviesWithCategory(String category) {
        List<Movie> movieList = new ArrayList<>();
        for (Movie movie : cachedMoviesById.values()) {
            if (movie.getCategory() != null) {
                for (int i = 0; i < movie.getCategory().length; i++) {
                    if (movie.getCategory()[i] != null) {
                        if (movie.getCategory()[i].equalsIgnoreCase(category)) {
                            movieList.add(movie);
                        }
                    }
                }
            }
        }
        return movieList;
    }

    private List<Movie> getMoviesWithGenre(int genreId) {
        List<Movie> movieList = new ArrayList<>();
        for (Movie movie : cachedMoviesById.values()) {
            if (movie.getGenreIds() != null) {
                for (int i = 0; i < movie.getGenreIds().length; i++) {
                    if (movie.getGenreIds()[i] == genreId) {
                        movieList.add(movie);
                    }
                }
            }
        }
        return movieList;
    }

    private void refreshCache(List<Movie> movies) {
        if (cachedMoviesById == null) {
            cachedMoviesById = Collections.synchronizedMap(new LinkedHashMap<>());
        }
        for (Movie movie : movies) {
            if (!cachedMoviesById.containsKey(movie.getId())) {
                cachedMoviesById.put(movie.getId(), movie);
            } else if (cachedMoviesById.containsKey(movie.getId())) {
                cachedMoviesById.remove(movie.getId());
                cachedMoviesById.put(movie.getId(), movie);
            }
        }
        mIsCacheDirty = false;
    }

    private void refreshCache(Movie movie) {
        if (cachedMovieDetails == null) {
            cachedMovieDetails = Collections.synchronizedMap(new LinkedHashMap<>());
        }
        if (!cachedMovieDetails.containsKey(movie.getId())) {
            cachedMovieDetails.put(movie.getId(), movie);
        }
        mIsDetailsCacheDirty = false;
    }

}
