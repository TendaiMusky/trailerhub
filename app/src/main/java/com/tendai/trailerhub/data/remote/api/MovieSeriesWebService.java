package com.tendai.trailerhub.data.remote.api;

import com.tendai.trailerhub.data.model.BothResponse;
import com.tendai.trailerhub.data.model.Movie;
import com.tendai.trailerhub.data.model.MovieListResponse;
import com.tendai.trailerhub.data.model.Series;
import com.tendai.trailerhub.data.model.SeriesListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieSeriesWebService {
    //movies
    @GET("movie/popular")
    Call<MovieListResponse> getPopularMovies(@Query("api_key") String apiKey, @Query("page") int pageNo);

    @GET("discover/movie")
    Call<MovieListResponse> getMoviesByGenre(@Query("api_key") String apiKey,
                                             @Query("with_genres") String genreId,
                                             @Query("sort_by") String popularityDescending,
                                             @Query("page") int pageNo);

    @GET("movie/top_rated")
    Call<MovieListResponse> getTopRatedMovies(@Query("api_key") String apiKey, @Query("page") int pageNo);

    @GET("movie/now_playing")
    Call<MovieListResponse> getNowPlaying(@Query("api_key") String apiKey, @Query("page") int pageNo);

    @GET("movie/upcoming")
    Call<MovieListResponse> getUpcomingMovies(@Query("api_key") String apiKey, @Query("page") int pageNo);

    @GET("movie/{movie_id}")
    Call<Movie> getMovieDetails(@Path("movie_id") int movieId,
                                @Query("api_key") String apiKey,
                                @Query("append_to_response") String creditsVideos);

    //Movies
    @GET("trending/{media_type}/{time_window}")
    Call<MovieListResponse> getTrending(@Path("media_type") String mediaType,
                                        @Path("time_window") String timeWindow,
                                        @Query("api_key") String apiKey);

    @GET("search/multi")
    Call<BothResponse> getSearched(@Query("query") String searchQuery, @Query("api_key") String apiKey, @Query("page") int pageNo);

    //Series
    @GET("tv/popular")
    Call<SeriesListResponse> getPopularSeries(@Query("api_key") String apiKey, @Query("page") int pageNo);

    @GET("discover/tv")
    Call<SeriesListResponse> getSeriesByGenre(@Query("api_key") String apiKey,
                                              @Query("with_genres") String genreId,
                                              @Query("sort_by") String popularityDescending,
                                              @Query("page") int pageNo);

    @GET("tv/top_rated")
    Call<SeriesListResponse> getTopRatedSeries(@Query("api_key") String apiKey, @Query("page") int pageNo);

    @GET("tv/on_the_air")
    Call<SeriesListResponse> getSeriesOnAir(@Query("api_key") String apiKey, @Query("page") int pageNo);

    @GET("tv/{tv_id}")
    Call<Series> getSeriesDetails(@Path("tv_id") int tvId,
                                  @Query("api_key") String apiKey,
                                  @Query("append_to_response") String creditsVideos);

}
