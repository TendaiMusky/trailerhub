package com.tendai.trailerhub.domain.series.model;

import com.tendai.trailerhub.domain.model.SimpleCast;
import com.tendai.trailerhub.domain.model.SimpleGenres;
import com.tendai.trailerhub.domain.model.SimpleVideos;

import java.util.List;

public class SimpleSeries {
    private int mId;
    private String mTitle;
    private String mOverview;
    private String mPosterPath;
    private String mReleaseDate;
    private String mBackdropPath;
    private List<SimpleGenres> mGenres;
    private List<SimpleCast> mCast;
    private int[] mGenreIds;
    private int mNumberOfSeasons;
    private double mVoteAverage;
    private List<SimpleVideos> mVideos;

    public SimpleSeries(int id, String title, String posterPath) {
        mId = id;
        mTitle = title;
        mPosterPath = posterPath;
    }

    public SimpleSeries(int id, String title, String posterPath, String backdropPath) {
        mId = id;
        mTitle = title;
        mPosterPath = posterPath;
        mBackdropPath = backdropPath;
    }

    public SimpleSeries(int id, String title, String backdropPath, int numberOfSeasons) {
        mId = id;
        mTitle = title;
        mBackdropPath = backdropPath;
        mNumberOfSeasons = numberOfSeasons;
    }

    public SimpleSeries(int id, String title, String overview, String posterPath, int[] genreIds, int numberOfSeasons, double voteAverage) {
        mId = id;
        mTitle = title;
        mOverview = overview;
        mPosterPath = posterPath;
        mGenreIds = genreIds;
        mNumberOfSeasons = numberOfSeasons;
        mVoteAverage = voteAverage;
    }

    public SimpleSeries(int id, String title, String releaseDate, String overView, String posterPath, String backdropPath,
                        int numberOfSeasons, double voteAverage) {
        mId = id;
        mTitle = title;
        mPosterPath = posterPath;
        mReleaseDate = releaseDate;
        mNumberOfSeasons = numberOfSeasons;
        mOverview = overView;
        mVoteAverage = voteAverage;
        mBackdropPath = backdropPath;
    }

    public SimpleSeries(int id,
                        String title,
                        String overview,
                        String posterPath,
                        String releaseDate,
                        String backdropPath,
                        List<SimpleGenres> genres,
                        List<SimpleCast> cast,
                        List<SimpleVideos> videos,
                        int numberOfSeasons,
                        double voteAverage) {
        mId = id;
        mTitle = title;
        mOverview = overview;
        mPosterPath = posterPath;
        mReleaseDate = releaseDate;
        mBackdropPath = backdropPath;
        mGenres = genres;
        mCast = cast;
        mVideos = videos;
        mNumberOfSeasons = numberOfSeasons;
        mVoteAverage = voteAverage;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getOverview() {
        return mOverview;
    }

    public void setOverview(String overview) {
        mOverview = overview;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public void setPosterPath(String posterPath) {
        mPosterPath = posterPath;
    }

    public String getReleaseDate() {
        return mReleaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        mReleaseDate = releaseDate;
    }

    public String getBackdropPath() {
        return mBackdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        mBackdropPath = backdropPath;
    }

    public List<SimpleGenres> getGenres() {
        return mGenres;
    }

    public void setGenres(List<SimpleGenres> genres) {
        mGenres = genres;
    }

    public List<SimpleCast> getCast() {
        return mCast;
    }

    public void setCast(List<SimpleCast> cast) {
        mCast = cast;
    }

    public int getNumberOfSeasons() {
        return mNumberOfSeasons;
    }

    public void setNumberOfSeasons(int numberOfSeasons) {
        mNumberOfSeasons = numberOfSeasons;
    }

    public double getVoteAverage() {
        return mVoteAverage;
    }

    public void setVoteAverage(double voteAverage) {
        mVoteAverage = voteAverage;
    }

    public int[] getGenreIds() {
        return mGenreIds;
    }

    public void setGenreIds(int[] genreIds) {
        mGenreIds = genreIds;
    }

    public List<SimpleVideos> getVideos() {
        return mVideos;
    }

    public void setVideos(List<SimpleVideos> videos) {
        mVideos = videos;
    }
}
