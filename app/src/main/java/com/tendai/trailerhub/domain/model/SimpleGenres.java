package com.tendai.trailerhub.domain.model;

public class SimpleGenres {
    private int mId;
    private String mName;

    public SimpleGenres(int id, String name) {
        this.mId = id;
        this.mName = name;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }
}
