package com.tendai.trailerhub.domain.movieseries;

import java.util.List;

public abstract class BothMapper<S, U, W> {

    public abstract List<S> mapMovie(List<W> movieSeriesList);

    public abstract List<U> mapSeries(List<W> movieSeriesList);
}

