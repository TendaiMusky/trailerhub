package com.tendai.trailerhub.domain.model;

public class SimpleCast {
    private long mId;
    private String mName;
    private String mCharacter;
    private String mPosterPath;
    private boolean isExpanded;

    public SimpleCast(long id, String name, String posterPath, String character) {
        mId = id;
        mName = name;
        mCharacter = character;
        mPosterPath = posterPath;
        isExpanded = false;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getCharacter() {
        return mCharacter;
    }

    public void setCharacter(String character) {
        mCharacter = character;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public void setPosterPath(String posterPath) {
        mPosterPath = posterPath;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }
}
