package com.tendai.trailerhub.domain.movieseries.model;

public class Search {
    private int mId;
    private String mTitle;
    private String mType;

    public Search(int id, String title, String type) {
        mId = id;
        mTitle = title;
        mType = type;
    }

    public int getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getType() {
        return mType;
    }
}
