package com.tendai.trailerhub.domain;

public interface UseCaseScheduler {

    void  execute(Runnable runnable);

    <V extends UseCase.ResponseValues> void notifySuccess(final V response, final UseCase.UseCaseCallback<V> useCaseCallback);

    <V extends UseCase.ResponseValues> void notifyFailure(final UseCase.UseCaseCallback<V> useCaseCallback);
}
