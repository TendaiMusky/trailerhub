package com.tendai.trailerhub.domain.movieseries;

import com.tendai.trailerhub.data.model.Both;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;

import java.util.ArrayList;
import java.util.List;

public class MovieSeriesMapper extends BothMapper<SimpleMovie, SimpleSeries, Both> {
    private static final String MEDIA_TYPE_SERIES = "tv";
    private static final String MEDIA_TYPE_MOVIE = "movie";

    private List<SimpleSeries> mSimpleSeries;
    private List<SimpleMovie> mSimpleMovies;

    public MovieSeriesMapper() {
        mSimpleMovies = new ArrayList<>();
        mSimpleSeries = new ArrayList<>();
    }

    @Override
    public List<SimpleMovie> mapMovie(List<Both> movieSeriesList) {
        for (Both both : movieSeriesList) {
            String mediaType = both.getMediaType();

            if (mediaType.equals(MEDIA_TYPE_MOVIE)) {
                SimpleMovie movie = new SimpleMovie(both.getId(), both.getMovieTitle(), both.getPosterPath(), both.getBackdropPath());
                mSimpleMovies.add(movie);
            }
        }
        return mSimpleMovies;
    }

    @Override
    public List<SimpleSeries> mapSeries(List<Both> movieSeriesList) {
        for (Both both : movieSeriesList) {
            String mediaType = both.getMediaType();

            if (mediaType.equals(MEDIA_TYPE_SERIES)) {
                SimpleSeries series = new SimpleSeries(both.getId(), both.getSeriesTitle(), both.getPosterPath(), both.getBackdropPath());
                mSimpleSeries.add(series);
            }
        }
        return mSimpleSeries;
    }
}
