package com.tendai.trailerhub.domain.movie;

import com.tendai.trailerhub.data.model.Movie;
import com.tendai.trailerhub.domain.Mapper;
import com.tendai.trailerhub.domain.model.SimpleCast;
import com.tendai.trailerhub.domain.model.SimpleGenres;
import com.tendai.trailerhub.domain.model.SimpleVideos;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MovieMapper extends Mapper<Movie, SimpleMovie> {
    private List<SimpleMovie> mSimpleMovies;
    private String mFormattedDate;

    public MovieMapper() {
        genres = new ArrayList<>();
        cast = new ArrayList<>();
        videos = new ArrayList<>();
        mSimpleMovies = new ArrayList<>();
    }

    @Override
    public SimpleMovie map(Movie input) {

        if (input.getMovieGenres() != null && input.getMovieGenres().size() != 0) {
            for (int i = 0; i < input.getMovieGenres().size(); i++) {
                genres.add(new SimpleGenres(input.getMovieGenres().get(i).getId(),
                        input.getMovieGenres().get(i).getName()));
            }
        }

        if (input.getVideoResponse() != null && input.getVideoResponse().getVideos().size() != 0) {
            for (int i = 0; i < input.getVideoResponse().getVideos().size(); i++) {
                videos.add(new SimpleVideos(
                        input.getVideoResponse().getVideos().get(i).getKey(),
                        input.getVideoResponse().getVideos().get(i).getSite(),
                        input.getVideoResponse().getVideos().get(i).getType()));
            }
        }

        if (input.getCredits() != null && input.getCredits().getCast().size() != 0) {
            for (int i = 0; i < input.getCredits().getCast().size(); i++) {
                cast.add(new SimpleCast(input.getCredits().getCast().get(i).getId(),
                        input.getCredits().getCast().get(i).getName(),
                        input.getCredits().getCast().get(i).getProfilePath(),
                        input.getCredits().getCast().get(i).getCharacter()));
            }
        }


        String date = input.getReleaseDate();

        if (date != null) {
            int year = Integer.parseInt(date.substring(0, 4));
            int month = Integer.parseInt(date.substring(5, 7)) - 1;
            int day = Integer.parseInt(date.substring(8));

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            dateFormat.applyPattern("dd MMM yyyy");
            mFormattedDate = dateFormat.format(calendar.getTime());
        }


        return new SimpleMovie(input.getId(),
                input.getTitle(),
                input.getOverview(),
                input.getPosterPath(),
                mFormattedDate,
                input.getBackdropPath(),
                genres,
                cast,
                videos,
                input.getRuntime(),
                input.getRating());
    }

    @Override
    public List<SimpleMovie> map(List<Movie> input) {

        for (Movie movie : input) {
            SimpleMovie simpleMovie =
                    new SimpleMovie(movie.getId(), movie.getTitle(), movie.getReleaseDate(), movie.getOverview(),
                            movie.getPosterPath(), movie.getBackdropPath(), movie.getRuntime(), movie.getRating());
//            simpleMovie.setCategory(movie.getCategory());
            mSimpleMovies.add(simpleMovie);
        }
        return mSimpleMovies;
    }
}
