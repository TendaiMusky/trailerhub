package com.tendai.trailerhub.domain;

import com.tendai.trailerhub.domain.model.SimpleCast;
import com.tendai.trailerhub.domain.model.SimpleGenres;
import com.tendai.trailerhub.domain.model.SimpleVideos;

import java.util.List;

public abstract class Mapper<T, V> {
    public List<SimpleGenres> genres;
    public List<SimpleCast> cast;
    public List<SimpleVideos> videos;

    public abstract V map(T input);

    public abstract List<V> map(List<T> input);

}


