package com.tendai.trailerhub.domain.model;

public class SimpleVideos {
    private String mKey;
    private String mSite;
    private String mType;

    public SimpleVideos(String key, String site, String type) {
        mKey = key;
        mSite = site;
        mType = type;
    }

    public String getKey() {
        return mKey;
    }

    public String getSite() {
        return mSite;
    }

    public String getType() {
        return mType;
    }

}
