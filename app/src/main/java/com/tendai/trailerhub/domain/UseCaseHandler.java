package com.tendai.trailerhub.domain;

//This class is the invoker in the command pattern. Though it's using the scheduler to execute the command.
public class UseCaseHandler {
    private static final String TAG = UseCaseHandler.class.getSimpleName();

    private static UseCaseHandler INSTANCE;

    private final UseCaseScheduler mUseCaseScheduler;

    public UseCaseHandler(UseCaseScheduler useCaseScheduler) {
        mUseCaseScheduler = useCaseScheduler;
    }

    public static UseCaseHandler getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UseCaseHandler(new UseCaseThreadPoolScheduler());
        }
        return INSTANCE;
    }

    public <T extends UseCase.RequestValues, R extends UseCase.ResponseValues> void execute(
            final UseCase<T, R> useCase,
            T values,
            UseCase.UseCaseCallback<R> callback) {

        useCase.setRequestValues(values);
        useCase.setUseCaseCallback(new UiCallbackWrapper<>(callback, this));


        // Log.e(TAG,Thread.currentThread().getName()+ " Start. Command = ");
        // Log.e(TAG,Thread.currentThread().getName()+ " End. Command = ");
        mUseCaseScheduler.execute(useCase::run);
    }

    <V extends UseCase.ResponseValues> void notifySuccess(final V response, final UseCase.UseCaseCallback<V> useCaseCallback) {
        mUseCaseScheduler.notifySuccess(response, useCaseCallback);
    }

    <V extends UseCase.ResponseValues> void notifyFailure(final UseCase.UseCaseCallback<V> useCaseCallback) {
        mUseCaseScheduler.notifyFailure(useCaseCallback);
    }
}
