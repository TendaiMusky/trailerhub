package com.tendai.trailerhub.domain.movieseries.usecase;

import com.tendai.trailerhub.data.CallBacks;
import com.tendai.trailerhub.data.DataSource;
import com.tendai.trailerhub.data.model.Movie;
import com.tendai.trailerhub.domain.UseCase;
import com.tendai.trailerhub.domain.movie.MovieMapper;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.domain.movieseries.MovieSeriesMapper;

import java.util.Collections;
import java.util.List;

public class GetTrending extends UseCase<GetTrending.RequestValues, GetTrending.ResponseValues> {
    private DataSource mRepository;

    public GetTrending(DataSource repository) {
        mRepository = repository;
    }

    @Override
    public void executeUseCase(RequestValues requestValues) {
        mRepository.getTrending(requestValues.mMediaType, requestValues.mTimeWindow,
                new CallBacks.GetTrending() {
                    @Override
                    public void onSuccess(List<Movie> movies) {
                        MovieMapper mapper = new MovieMapper();
                        List<SimpleMovie> movieList = mapper.map(movies);
                        Collections.shuffle(movieList);
                        ResponseValues response = new ResponseValues(movieList);
                        getUseCaseCallback().onSuccess(response);
                    }

                    @Override
                    public void onFailure() {
                        getUseCaseCallback().onFailure();
                    }
                }
        );

    }

    public static final class RequestValues implements UseCase.RequestValues {
        private final String mMediaType;
        private final String mTimeWindow;

        public RequestValues(String mediaType, String timeWindow) {
            this.mMediaType = mediaType;
            this.mTimeWindow = timeWindow;
        }
    }

    public static final class ResponseValues implements UseCase.ResponseValues {
        private List<SimpleMovie> mMovies;
        
        public ResponseValues(List<SimpleMovie> movies) {
            mMovies = movies;
                  }
        public List<SimpleMovie> getMovies() {
            return mMovies;
        }

        public void setMovies(List<SimpleMovie> movies) {
            mMovies = movies;
        }


    }
}
