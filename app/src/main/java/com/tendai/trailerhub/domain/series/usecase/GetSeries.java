package com.tendai.trailerhub.domain.series.usecase;

import com.tendai.trailerhub.data.CallBacks;
import com.tendai.trailerhub.data.SeriesDataSource;
import com.tendai.trailerhub.data.model.Series;
import com.tendai.trailerhub.domain.UseCase;
import com.tendai.trailerhub.domain.series.SeriesMapper;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;

import java.util.List;

public class GetSeries extends UseCase<GetSeries.RequestValues, GetSeries.ResponseValues> {
    private final SeriesDataSource mSeriesRepository;

    public GetSeries(SeriesDataSource seriesRepository) {
        this.mSeriesRepository = seriesRepository;
    }

    @Override
    public void executeUseCase(RequestValues requestValues) {
        String genreId = requestValues.getGenreId();
        int pageNumber = requestValues.getPageNumber();
        int seriesId = requestValues.getSeriesId();
        String category = requestValues.getCategory();

        switch (requestValues.getRequestType()) {
            case RequestValues.TYPE_CATEGORY:
                getSeriesByCategory(category, pageNumber);
                break;

            case RequestValues.TYPE_GENRE_ID:
                getSeriesByGenreId(genreId, pageNumber);
                break;

            case RequestValues.SERIES_ID:
                getSeriesDetails(seriesId);
                break;
        }
    }

    private void getSeriesDetails(int seriesId) {
        mSeriesRepository.getSeriesDetails(seriesId, new CallBacks.GetSeriesDetails() {
            @Override
            public void onSuccess(Series series) {
                SeriesMapper mapper = new SeriesMapper();
                SimpleSeries simpleSeriesDetails = mapper.map(series);

                ResponseValues responseValues = new ResponseValues(simpleSeriesDetails);
                getUseCaseCallback().onSuccess(responseValues);
            }

            @Override
            public void onFailure() {
                getUseCaseCallback().onFailure();
            }
        });
    }

    private void getSeriesByCategory(String category, int pageNumber) {
        mSeriesRepository.getSeriesByCategory(category, pageNumber, new CallBacks.GetSeries() {
            @Override
            public void onSuccess(List<Series> series) {
                SeriesMapper mapper = new SeriesMapper();
                List<SimpleSeries> simpleSeries = mapper.map(series);
//                Collections.shuffle(simpleSeries);

                ResponseValues responseValues = new ResponseValues(simpleSeries);
                getUseCaseCallback().onSuccess(responseValues);
            }

            @Override
            public void onFailure() {
                getUseCaseCallback().onFailure();
            }
        });
    }

    private void getSeriesByGenreId(String genreId, int pageNumber) {
        mSeriesRepository.getSeriesByGenre(genreId, pageNumber, new CallBacks.GetSeries() {
            @Override
            public void onSuccess(List<Series> series) {
                SeriesMapper mapper = new SeriesMapper();
                List<SimpleSeries> simpleSeries = mapper.map(series);
//                Collections.shuffle(simpleSeries);

                ResponseValues responseValues = new ResponseValues(simpleSeries);
                getUseCaseCallback().onSuccess(responseValues);
            }

            @Override
            public void onFailure() {
                getUseCaseCallback().onFailure();
            }
        });
    }

    public static final class RequestValues implements UseCase.RequestValues {
        public static final String TYPE_CATEGORY = "CATEGORY";
        public static final String TYPE_GENRE_ID = "GENRE_ID";
        public static final String SERIES_ID = "SERIES_ID";

        private int mSeriesId;
        private int mPageNumber;
        private String mCategory;
        private String mGenreId;
        private String requestType;
        boolean mIsWithExtraDetails;

        public RequestValues(int mSeriesId) {
            this.mSeriesId = mSeriesId;
        }

        public RequestValues(String category, int pageNumber, boolean isWithExtraDetails) {
            mCategory = category;
            mIsWithExtraDetails = isWithExtraDetails;
            mPageNumber = pageNumber;
        }

        public RequestValues(String genreId, int pageNumber) {
            mGenreId = genreId;
            mPageNumber = pageNumber;
        }

        public String getCategory() {
            return mCategory;
        }

        int getSeriesId() {
            return mSeriesId;
        }

        String getGenreId() {
            return mGenreId;
        }

        boolean isWithExtraDetails() {
            return mIsWithExtraDetails;
        }

        String getRequestType() {
            return requestType;
        }

        public void setRequestType(String requestType) {
            this.requestType = requestType;
        }

        int getPageNumber() {
            return mPageNumber;
        }
    }

    public static final class ResponseValues implements UseCase.ResponseValues {
        private SimpleSeries mSeries;
        private List<SimpleSeries> mSeriesList;

        ResponseValues(SimpleSeries series) {
            this.mSeries = series;
        }

        ResponseValues(List<SimpleSeries> seriesList) {
            this.mSeriesList = seriesList;
        }

        public SimpleSeries getSeries() {
            return mSeries;
        }

        public List<SimpleSeries> getSeriesList() {
            return mSeriesList;
        }
    }
}
