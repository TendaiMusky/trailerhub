package com.tendai.trailerhub.domain.movie.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.tendai.trailerhub.domain.model.SimpleCast;
import com.tendai.trailerhub.domain.model.SimpleGenres;
import com.tendai.trailerhub.domain.model.SimpleVideos;

import java.util.List;

public class SimpleMovie implements Parcelable {
    private int mId;
    private String mTitle;
    private String mOverview;
    private String mPosterPath;
    private String mReleaseDate;
    private String mBackdropPath;
    private String mCategory;
    private List<SimpleGenres> mGenres;
    private List<SimpleCast> mCast;
    private int[] mGenreIds;
    private int mRuntime;
    private double mVoteAverage;
    private List<SimpleVideos> mVideos;

    public SimpleMovie() {
    }

    public SimpleMovie(int id, String title, String posterPath) {
        mId = id;
        mTitle = title;
        mPosterPath = posterPath;
    }

    public SimpleMovie(int id, String title, String posterPath, String backdropPath) {
        mId = id;
        mTitle = title;
        mPosterPath = posterPath;
        mBackdropPath = backdropPath;
    }

    public SimpleMovie(int id, String title, String backdropPath, int runtime) {
        mId = id;
        mTitle = title;
        mBackdropPath = backdropPath;
        mRuntime = runtime;
    }

    public SimpleMovie(int id, String title, String releaseDate, String overView, String posterPath, String backdropPath,
                       int runtime, double voteAverage) {
        mId = id;
        mTitle = title;
        mPosterPath = posterPath;
        mReleaseDate = releaseDate;
        mRuntime = runtime;
        mOverview = overView;
        mVoteAverage = voteAverage;
        mBackdropPath = backdropPath;
    }

    public SimpleMovie(int id,
                       String title,
                       String overview,
                       String posterPath,
                       int[] genreIds,
                       int runtime,
                       double voteAverage) {
        mId = id;
        mTitle = title;
        mOverview = overview;
        mPosterPath = posterPath;
        mGenreIds = genreIds;
        mRuntime = runtime;
        mVoteAverage = voteAverage;
    }

    public SimpleMovie(int id,
                       String title,
                       String overview,
                       String posterPath,
                       String releaseDate,
                       String backdropPath,
                       List<SimpleGenres> genres,
                       List<SimpleCast> cast,
                       List<SimpleVideos> videos,
                       int runtime,
                       double voteAverage) {
        mId = id;
        mTitle = title;
        mOverview = overview;
        mPosterPath = posterPath;
        mReleaseDate = releaseDate;
        mBackdropPath = backdropPath;
        mGenres = genres;
        mCast = cast;
        mRuntime = runtime;
        mVideos = videos;
        mVoteAverage = voteAverage;
    }

    protected SimpleMovie(Parcel in) {
        mId = in.readInt();
        mTitle = in.readString();
        mOverview = in.readString();
        mPosterPath = in.readString();
        mReleaseDate = in.readString();
        mBackdropPath = in.readString();
        mCategory = in.readString();
        mGenreIds = in.createIntArray();
        mRuntime = in.readInt();
        mVoteAverage = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mTitle);
        dest.writeString(mOverview);
        dest.writeString(mPosterPath);
        dest.writeString(mReleaseDate);
        dest.writeString(mBackdropPath);
        dest.writeString(mCategory);
        dest.writeIntArray(mGenreIds);
        dest.writeInt(mRuntime);
        dest.writeDouble(mVoteAverage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SimpleMovie> CREATOR = new Creator<SimpleMovie>() {
        @Override
        public SimpleMovie createFromParcel(Parcel in) {
            return new SimpleMovie(in);
        }

        @Override
        public SimpleMovie[] newArray(int size) {
            return new SimpleMovie[size];
        }
    };

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getOverview() {
        return mOverview;
    }

    public void setOverview(String overview) {
        mOverview = overview;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public void setPosterPath(String posterPath) {
        mPosterPath = posterPath;
    }

    public String getReleaseDate() {
        return mReleaseDate;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public void setReleaseDate(String releaseDate) {
        mReleaseDate = releaseDate;
    }

    public String getBackdropPath() {
        return mBackdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        mBackdropPath = backdropPath;
    }

    public List<SimpleGenres> getGenres() {
        return mGenres;
    }

    public void setGenres(List<SimpleGenres> genres) {
        mGenres = genres;
    }

    public List<SimpleCast> getCast() {
        return mCast;
    }

    public void setCast(List<SimpleCast> cast) {
        mCast = cast;
    }

    public int getRuntime() {
        return mRuntime;
    }

    public void setRuntime(int runtime) {
        mRuntime = runtime;
    }

    public double getVoteAverage() {
        return mVoteAverage;
    }

    public void setVoteAverage(double voteAverage) {
        mVoteAverage = voteAverage;
    }

    public int[] getGenreIds() {
        return mGenreIds;
    }

    public void setGenreIds(int[] genreIds) {
        mGenreIds = genreIds;
    }

    public List<SimpleVideos> getVideos() {
        return mVideos;
    }

    public void setVideos(List<SimpleVideos> videos) {
        mVideos = videos;
    }
}