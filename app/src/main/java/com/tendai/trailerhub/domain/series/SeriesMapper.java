package com.tendai.trailerhub.domain.series;

import com.tendai.trailerhub.data.model.Series;
import com.tendai.trailerhub.domain.Mapper;
import com.tendai.trailerhub.domain.model.SimpleCast;
import com.tendai.trailerhub.domain.model.SimpleGenres;
import com.tendai.trailerhub.domain.model.SimpleVideos;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class SeriesMapper extends Mapper<Series, SimpleSeries> {
    private List<SimpleSeries> mSimpleSeries;
    private String formattedDate;

    public SeriesMapper() {
        genres = new ArrayList<>();
        cast = new ArrayList<>();
        videos = new ArrayList<>();
        mSimpleSeries = new ArrayList<>();
    }

    @Override
    public SimpleSeries map(Series input) {
        if (input.getSeriesGenres() != null && input.getSeriesGenres().size() != 0) {
            for (int i = 0; i < input.getSeriesGenres().size(); i++) {
                genres.add(new SimpleGenres(input.getSeriesGenres().get(i).getId(),
                        input.getSeriesGenres().get(i).getName()));
            }
        }

        if (input.getCredits() != null && input.getCredits().getCast().size() != 0) {
            for (int i = 0; i < input.getCredits().getCast().size(); i++) {
                cast.add(new SimpleCast(input.getCredits().getCast().get(i).getId(),
                        input.getCredits().getCast().get(i).getName(),
                        input.getCredits().getCast().get(i).getProfilePath(),
                        input.getCredits().getCast().get(i).getCharacter()));
            }
        }

        if (input.getVideoResponse() != null && input.getVideoResponse().getVideos().size() != 0) {
            for (int i = 0; i < input.getVideoResponse().getVideos().size(); i++) {
                videos.add(new SimpleVideos(
                        input.getVideoResponse().getVideos().get(i).getKey(),
                        input.getVideoResponse().getVideos().get(i).getSite(),
                        input.getVideoResponse().getVideos().get(i).getType()));
            }
        }

        String date = input.getAirDate();
        if (date != null) {
            int year = Integer.parseInt(date.substring(0, 4));
            int month = Integer.parseInt(date.substring(5, 7)) - 1;
            int day = Integer.parseInt(date.substring(8));

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            dateFormat.applyPattern("dd MMM yyyy");
            formattedDate = dateFormat.format(calendar.getTime());
        }


        return new SimpleSeries(input.getId(),
                input.getTitle(),
                input.getOverview(),
                input.getPosterPath(),
                formattedDate,
                input.getBackdropPath(),
                genres,
                cast,
                videos,
                input.getNumberOfSeasons(),
                input.getRating());
    }

    @Override
    public List<SimpleSeries> map(List<Series> input) {
        for (Series input1 : input) {
            SimpleSeries simpleSeries =
                    new SimpleSeries(input1.getId(), input1.getTitle(), input1.getAirDate(), input1.getOverview(),
                            input1.getPosterPath(), input1.getBackdropPath(), input1.getNumberOfSeasons(), input1.getRating());
            mSimpleSeries.add(simpleSeries);
        }
        return mSimpleSeries;
    }
}
