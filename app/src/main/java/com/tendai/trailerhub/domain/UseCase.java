package com.tendai.trailerhub.domain;

import java.util.concurrent.ExecutionException;

public abstract class UseCase<T extends UseCase.RequestValues, V extends UseCase.ResponseValues> {
    private UseCaseCallback<V> mUseCaseCallback;
    private T mRequestValues;

    public UseCaseCallback<V> getUseCaseCallback() {
        return mUseCaseCallback;
    }

    public void setUseCaseCallback(UseCaseCallback<V> mUseCaseCallback) {
        this.mUseCaseCallback = mUseCaseCallback;
    }

    public T getRequestValues() {
        return mRequestValues;
    }

    public void setRequestValues(T mRequestValues) {
        this.mRequestValues = mRequestValues;
    }

    public void run() {
        executeUseCase(mRequestValues);
    }

    public abstract void executeUseCase(T requestValues);

    public interface RequestValues {
    }

    public interface ResponseValues {
    }

    public interface UseCaseCallback<V> {
        // Other overloads can be added as per need
        void onSuccess(V response);

        void onFailure();
    }
}
