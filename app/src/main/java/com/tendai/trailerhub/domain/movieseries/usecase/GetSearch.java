package com.tendai.trailerhub.domain.movieseries.usecase;

import com.tendai.trailerhub.data.CallBacks;
import com.tendai.trailerhub.data.DataSource;
import com.tendai.trailerhub.data.model.Both;
import com.tendai.trailerhub.domain.UseCase;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;
import com.tendai.trailerhub.domain.movieseries.MovieSeriesMapper;
import com.tendai.trailerhub.domain.movieseries.model.Search;
import com.tendai.trailerhub.domain.series.model.SimpleSeries;
import com.tendai.trailerhub.presentation.utils.Config;

import java.util.ArrayList;
import java.util.List;

public class GetSearch extends UseCase<GetSearch.RequestValues, GetSearch.ResponseValues> {
    private DataSource mRepository;

    public GetSearch(DataSource repository) {
        mRepository = repository;
    }

    @Override
    public void executeUseCase(RequestValues requestValues) {
        mRepository.getSearchedMovies(requestValues.mQuery, new CallBacks.GetSearch() {
            @Override
            public void onSuccess(List<Both> searchedMovies) {
                MovieSeriesMapper mapper = new MovieSeriesMapper();
                List<SimpleMovie> movies = mapper.mapMovie(searchedMovies);
                List<SimpleSeries> series = mapper.mapSeries(searchedMovies);

                ArrayList<Search> searches = new ArrayList<>();
                for (int i = 0;i<movies.size();i++) {
                    searches.add(new Search(movies.get(i).getId(),movies.get(i).getTitle(), Config.MOVIE));
                }
                for (int i = 0;i<series.size();i++) {
                    searches.add(new Search(series.get(i).getId(),series.get(i).getTitle(),Config.SERIES));
                }

                ResponseValues response = new ResponseValues(searches);
                getUseCaseCallback().onSuccess(response);
            }

            @Override
            public void onFailure() {
                getUseCaseCallback().onFailure();
            }
        });
    }

    public static final class RequestValues implements UseCase.RequestValues {
        String mQuery;

        public RequestValues(String query) {
            mQuery = query;
        }

    }

    public static final class ResponseValues implements UseCase.ResponseValues {
        private List<Search> mSearches;

        public ResponseValues(List<Search> searches) {
            mSearches = searches;
        }

        public List<Search> getSearchResults() {
            return mSearches;
        }
    }
}
