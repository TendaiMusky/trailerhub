package com.tendai.trailerhub.domain.movie.usecase;

import com.tendai.trailerhub.data.CallBacks;
import com.tendai.trailerhub.data.MovieDataSource;
import com.tendai.trailerhub.data.model.Movie;
import com.tendai.trailerhub.domain.UseCase;
import com.tendai.trailerhub.domain.movie.MovieMapper;
import com.tendai.trailerhub.domain.movie.model.SimpleMovie;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;


public class GetMovie extends UseCase<GetMovie.RequestValues, GetMovie.ResponseValues> {
    private final MovieDataSource mMovieRepository;

    public GetMovie(MovieDataSource movieRepository) {
        this.mMovieRepository = movieRepository;
    }

    @Override
    public void executeUseCase(RequestValues requestValues) {
        int movieId = requestValues.getMovieId();
        int pageNumber = requestValues.getPageNumber();
        String genreId = requestValues.getGenreId();
        String category = requestValues.getCategory();


        switch (requestValues.getRequestType()) {
            case RequestValues.TYPE_CATEGORY:
                getMovieByCategory(category, pageNumber);
                break;

            case RequestValues.TYPE_GENRE_ID:
                getMovieByGenreId(genreId, pageNumber);
                break;

            case RequestValues.TYPE_MOVIE_ID:
                getMovieDetails(movieId);
                break;
            default:
        }
    }

    private void getMovieDetails(int movieId) {
//        mMovieRepository.refreshMovies();
        mMovieRepository.getMovieDetails(movieId, new CallBacks.GetMovieDetails() {
            @Override
            public void onSuccess(Movie movie) {
                MovieMapper mapper = new MovieMapper();
                SimpleMovie simpleMovieDetails = mapper.map(movie);

                ResponseValues responseValues = new ResponseValues(simpleMovieDetails);
                getUseCaseCallback().onSuccess(responseValues);
            }

            @Override
            public void onFailure() {
                getUseCaseCallback().onFailure();
            }
        });
    }

    private void getMovieByCategory(String category, int pageNumber) {
        mMovieRepository.getMovieByCategory(category, pageNumber, new CallBacks.GetMovies() {
            @Override
            public void onSuccess(List<Movie> movies) {
                MovieMapper mapper = new MovieMapper();
                List<SimpleMovie> simpleMovies = mapper.map(movies);

                ResponseValues responseValues = new ResponseValues(simpleMovies);
                getUseCaseCallback().onSuccess(responseValues);
            }

            @Override
            public void onFailure() {
                getUseCaseCallback().onFailure();
            }
        });
    }

    private void getMovieByGenreId(String genreId, int pageNumber) {
        mMovieRepository.getMovieByGenre(genreId, pageNumber, new CallBacks.GetMovies() {
            @Override
            public void onSuccess(List<Movie> movies) {
                MovieMapper mapper = new MovieMapper();
                List<SimpleMovie> simpleMovies = mapper.map(movies);

                ResponseValues responseValues = new ResponseValues(simpleMovies);
                getUseCaseCallback().onSuccess(responseValues);
            }

            @Override
            public void onFailure() {
                getUseCaseCallback().onFailure();
            }
        });
    }

    public static final class RequestValues implements UseCase.RequestValues {
        public static final String TYPE_CATEGORY = "CATEGORY";
        public static final String TYPE_GENRE_ID = "GENRE_ID";
        public static final String TYPE_MOVIE_ID = "MOVIE_ID";

        private int mMovieId;
        private int mPageNumber;
        private String mGenreId;
        private String mCategory;
        private String requestType;
        private boolean mIsWithExtraDetails;

        public RequestValues(int movieId) {
            mMovieId = movieId;
        }

        public RequestValues(String genreId, int pageNumber) {
            mGenreId = genreId;
            mPageNumber = pageNumber;
        }

        public RequestValues(String category, boolean isWithExtraDetails, int pageNumber) {
            mCategory = category;
            mIsWithExtraDetails = isWithExtraDetails;
            mPageNumber = pageNumber;
        }

        public int getMovieId() {
            return mMovieId;
        }

        public String getGenreId() {
            return mGenreId;
        }

        public String getCategory() {
            return mCategory;
        }

        String getRequestType() {
            return requestType;
        }

        public void setRequestType(String requestType) {
            this.requestType = requestType;
        }

        public int getPageNumber() {
            return mPageNumber;
        }

    }

    public static final class ResponseValues implements UseCase.ResponseValues {
        private SimpleMovie mMovieDetails;
        private List<SimpleMovie> mMovies;

        public ResponseValues(SimpleMovie movieDetails) {
            mMovieDetails = movieDetails;
        }

        private ResponseValues(List<SimpleMovie> movies) {
            mMovies = movies;
        }

        public SimpleMovie getMovie() {
            return mMovieDetails;
        }

        public List<SimpleMovie> getMovies() {
            return mMovies;
        }

    }
}
