package com.tendai.trailerhub.domain;

public class UiCallbackWrapper <V extends UseCase.ResponseValues> implements UseCase.UseCaseCallback<V> {
    private final UseCase.UseCaseCallback<V> mCallback;
    private final UseCaseHandler mUseCaseHandler;

     UiCallbackWrapper(UseCase.UseCaseCallback<V> mCallback, UseCaseHandler mUseCaseHandler) {
        this.mCallback = mCallback;
        this.mUseCaseHandler = mUseCaseHandler;
    }

    @Override
    public void onSuccess(V response) {
        mUseCaseHandler.notifySuccess(response, mCallback);
    }

    @Override
    public void onFailure() {
        mUseCaseHandler.notifyFailure(mCallback);
    }
}
