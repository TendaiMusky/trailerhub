package com.tendai.trailerhub.domain;

import android.os.Handler;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class UseCaseThreadPoolScheduler implements UseCaseScheduler {
    private static String TAG = UseCaseThreadPoolScheduler.class.getSimpleName();

    private static final int CORE_POOL_SIZE = 9;
    private static final int MAX_POOL_SIZE = 12;
    private static final long TIMEOUT = Long.MAX_VALUE;

    private final Handler mHandler = new Handler();
    //private final ThreadPoolExecutor mThreadPoolExecutor;

    private int counter = 0;
   private final ExecutorService mExecutor;

    UseCaseThreadPoolScheduler() {
     //  mThreadPoolExecutor =
     //          new ThreadPoolExecutor(CORE_POOL_SIZE, MAX_POOL_SIZE, TIMEOUT,
     //                  TimeUnit.NANOSECONDS, new LinkedBlockingQueue<>());
            mExecutor = Executors.newCachedThreadPool();

    }

    @Override
    public void execute(Runnable runnable) {
  //      Log.i(TAG, "Running Thread Pool Scheduler" + counter);
        mExecutor.execute(runnable);
      //  Log.e(TAG, "Active Threads: " + mThreadPoolExecutor.getActiveCount());
      //  Log.e(TAG, "Tasks  " + mThreadPoolExecutor.getTaskCount());
      //  Log.e(TAG, "Completed Tasks: " + mThreadPoolExecutor.getCompletedTaskCount());
      //  Log.e(TAG, "Queue Size  " + mThreadPoolExecutor.getQueue().size());
        counter++;
    //  if (!mThreadPoolExecutor.isShutdown()) {
    //      return;
    //  }
    //  mThreadPoolExecutor.shutdown();
    //  Log.e(TAG, "Finished executing all threads");

//        mExecutor.execute(runnable);
    }

    @Override
    public <V extends UseCase.ResponseValues> void notifySuccess(V response, UseCase.UseCaseCallback<V> useCaseCallback) {
        mHandler.post(() -> useCaseCallback.onSuccess(response));
    }

    @Override
    public <V extends UseCase.ResponseValues> void notifyFailure(UseCase.UseCaseCallback<V> useCaseCallback) {
        mHandler.post(useCaseCallback::onFailure);
    }
}
