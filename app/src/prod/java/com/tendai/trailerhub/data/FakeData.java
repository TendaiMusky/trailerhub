package com.tendai.trailerhub.data;

import com.tendai.trailerhub.data.model.Cast;
import com.tendai.trailerhub.data.model.Credits;
import com.tendai.trailerhub.data.model.Genres;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

abstract class FakeData {
   private static List<Cast> provideFakeCast() {
        return new ArrayList<>(Arrays.asList(
                new Cast(12345, "Cast 12345", "/profilePath", "Character 12345"),
                new Cast(12346, "Cast 12346", "/profilePath", "Character 12346"),
                new Cast(12347, "Cast 12347", "/profilePath", "Character 12347")
        ));
    }

    static Credits provideFakeCredits() {
        Credits credits = new Credits();
        credits.setCast(provideFakeCast());
        return credits;
    }

    static List<Genres> provideFakeGenres() {
        return new ArrayList<>(Arrays.asList(
                new Genres(28, "Action"),
                new Genres(12, "Adventure")
        ));
    }

    static int[] provideGenreIds() {
       return new int[]{28, 12};
    }
}
