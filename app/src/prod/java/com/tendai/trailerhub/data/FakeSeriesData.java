package com.tendai.trailerhub.data;

import com.tendai.trailerhub.data.model.Series;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FakeSeriesData extends FakeData {

    public static List<Series> provideFakeSeriesList() {
        return new ArrayList<>(Arrays.asList(
                new Series(3367,
                        "Series 3367",
                        "Series 3367 Overview",
                        189,
                        7.6,
                        "/posterUrl",
                        "/backdropUrl",
                        "2020-02-18",
                        provideFakeGenres(),
                        provideFakeCredits()),
                new Series(3368,
                        "Series 3368",
                        "Series 3368 Overview",
                        134,
                        5.6,
                        "/posterUrl",
                        "/backdropUrl",
                        "2020-02-18",
                        provideFakeGenres(),
                        provideFakeCredits())
        ));
    }

    public static Series provideFakeSeries() {
        return new Series(3367,
                "Series 3367",
                "Series 3367 Overview",
                189,
                7.6,
                "/posterUrl",
                "/backdropUrl",
                "2020-02-18",
                provideFakeGenres(),
                provideFakeCredits());
    }
}
