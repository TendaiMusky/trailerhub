package com.tendai.trailerhub.data;

import com.tendai.trailerhub.data.model.Both;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FakeBothData {
    public static List<Both> provideFakeBothData() {

        return
                new ArrayList<>(Arrays.asList(
                        new Both(1234, "Movie 1234", "", "/backdropUrl", "movie"),
                        new Both(1235, "Movie 1235", "", "/backdropUrl", "movie"),
                        new Both(1236, "Movie 1236", "", "/backdropUrl", "movie"),
                        new Both(1237, "", "Series 1237", "/backdropUrl", "tv"),
                        new Both(1238, "", "Series 1238", "/backdropUrl", "tv"),
                        new Both(1239, "", "Series 1239", "/backdropUrl", "tv")
                ));
    }
}
