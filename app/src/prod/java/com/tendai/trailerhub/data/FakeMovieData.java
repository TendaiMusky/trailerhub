package com.tendai.trailerhub.data;

import com.tendai.trailerhub.data.model.Movie;
import com.tendai.trailerhub.presentation.utils.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FakeMovieData extends FakeData {

    public static List<Movie> provideFakeMovieList() {

        return new ArrayList<>(Arrays.asList(
                new Movie(3367,
                        "Movie 3367",
                        "Movie 3367 Overview",
                        189,
                        7.6,
                        "/posterUrl",
                        "/backdropUrl",
                        "2020-02-18",
                        provideGenreIds(),
                        provideFakeCredits(),
                        Config.POPULAR),
                new Movie(3368,
                        "Movie 3368",
                        "Movie 3368 Overview",
                        134,
                        5.6,
                        "/posterUrl",
                        "/backdropUrl",
                        "2020-02-18",
                        provideGenreIds(),
                        provideFakeCredits(),
                        Config.UPCOMING)
        ));
    }

    public static Movie provideFakeMovie() {
        return new Movie(3367,
                "Movie 3367",
                "Movie 3367 Overview",
                189,
                7.6,
                "/posterUrl",
                "/backdropUrl",
                "2020-02-18",
                provideGenreIds(),
                provideFakeCredits(),
                Config.TOP_RATED);
    }

}
