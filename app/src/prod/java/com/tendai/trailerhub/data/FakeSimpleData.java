package com.tendai.trailerhub.data;

import com.tendai.trailerhub.domain.model.SimpleCast;
import com.tendai.trailerhub.domain.model.SimpleGenres;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

abstract class FakeSimpleData {

    static List<SimpleCast> provideFakeSimpleCast() {
        return new ArrayList<>(Arrays.asList(
                new SimpleCast(12345, "Cast 12345", "/profilePath", "Character 12345"),
                new SimpleCast(12346, "Cast 12346", "/profilePath", "Character 12346"),
                new SimpleCast(12347, "Cast 12347", "/profilePath", "Character 12347")
        ));
    }

    static List<SimpleGenres> provideFakeSimpleGenres() {
        return new ArrayList<>(Arrays.asList(
                new SimpleGenres(28, "Action"),
                new SimpleGenres(12, "Adventure")
        ));
    }
}
