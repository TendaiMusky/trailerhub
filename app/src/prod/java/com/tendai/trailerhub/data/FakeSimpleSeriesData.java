package com.tendai.trailerhub.data;

import com.tendai.trailerhub.domain.series.model.SimpleSeries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FakeSimpleSeriesData extends FakeSimpleData {

    public static List<SimpleSeries> provideFakeSimpleSeriesList() {

        return new ArrayList<>(Arrays.asList(
                new SimpleSeries(3367,
                        "Series 3367",
                        "Series 3367 Overview",
                        "/posterUrl",
                        "2020-02-18",
                        "/backdropUrl",
                        provideFakeSimpleGenres(),
                        provideFakeSimpleCast(),
                        null,
                        189,
                        7.6),
                new SimpleSeries(3368,
                        "Series 3368",
                        "Series 3368 Overview",
                        "/posterUrl",
                        "2020-02-18",
                        "/backdropUrl",
                        provideFakeSimpleGenres(),
                        provideFakeSimpleCast(),
                        null,
                        134,
                        5.6)
        ));
    }

    public static SimpleSeries provideFakeSimpleSeries() {

        return new SimpleSeries(3367,
                "Series 3367",
                "Series 3367 Overview",
                "/posterUrl",
                "2020-02-18",
                "/backdropUrl",
                provideFakeSimpleGenres(),
                provideFakeSimpleCast(),
                null,
                189,
                7.6);
    }

    public static List<SimpleSeries> provideFakeSimpleSeriesBoth() {
        return
                new ArrayList<>(Arrays.asList(
                        new SimpleSeries(1237, "Series 1237", "/backdropUrl", 0),
                        new SimpleSeries(1238, "Series 1238", "/backdropUrl", 0),
                        new SimpleSeries(1239, "Series 1239", "/backdropUrl", 0)
                ));
    }
}
