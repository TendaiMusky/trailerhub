package com.tendai.trailerhub.data;


import com.tendai.trailerhub.domain.movie.model.SimpleMovie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FakeSimpleMovieData extends FakeSimpleData {


    public static List<SimpleMovie> provideFakeSimpleMovieList() {
        return new ArrayList<>(Arrays.asList(
                new SimpleMovie(3367,
                        "Movie 3367",
                        "Movie 3367 Overview",
                        "/posterUrl",
                        "2020-02-18",
                        "/backdropUrl",
                        provideFakeSimpleGenres(),
                        provideFakeSimpleCast(),
                        null,
                        189,
                        7.6),
                new SimpleMovie(3368,
                        "Movie 3368",
                        "Movie 3368 Overview",
                        "/posterUrl",
                        "2020-02-18",
                        "/backdropUrl",
                        provideFakeSimpleGenres(),
                        provideFakeSimpleCast(),
                        null,
                        134,
                        5.6)
        ));
    }

    public static SimpleMovie provideFakeSimpleMovie() {
        return new SimpleMovie(3367,
                "Movie 3367",
                "Movie 3367 Overview",
                "/posterUrl",
                "2020-02-18",
                "/backdropUrl",
                provideFakeSimpleGenres(),
                provideFakeSimpleCast(),
                null,
                189,
                7.6);
    }

    public static List<SimpleMovie> provideFakeSimpleMovieListBoth() {
        return
                new ArrayList<>(Arrays.asList(
                        new SimpleMovie(1234, "Movie 1234", "/backdropUrl", 0),
                        new SimpleMovie(1235, "Movie 1235", "/backdropUrl", 0),
                        new SimpleMovie(1236, "Movie 1236", "/backdropUrl", 0)
                ));
    }
}
