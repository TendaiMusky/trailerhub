package com.tendai.trailerhub;

import android.content.Context;

import androidx.annotation.NonNull;

import com.tendai.trailerhub.data.DataSource;
import com.tendai.trailerhub.data.MovieDataSource;
import com.tendai.trailerhub.data.MovieRepository;
import com.tendai.trailerhub.data.Repository;
import com.tendai.trailerhub.data.SeriesDataSource;
import com.tendai.trailerhub.data.SeriesRepository;
import com.tendai.trailerhub.data.remote.RemoteDataSource;
import com.tendai.trailerhub.data.remote.RemoteMovieDataSource;
import com.tendai.trailerhub.data.remote.RemoteSeriesDataSource;
import com.tendai.trailerhub.data.remote.api.MovieSeriesWebService;
import com.tendai.trailerhub.data.remote.api.RetrofitClient;
import com.tendai.trailerhub.domain.UseCaseHandler;
import com.tendai.trailerhub.domain.movie.usecase.GetMovie;
import com.tendai.trailerhub.domain.movieseries.usecase.GetSearch;
import com.tendai.trailerhub.domain.movieseries.usecase.GetTrending;
import com.tendai.trailerhub.domain.series.usecase.GetSeries;

public class Injection {
    //For both
    private static DataSource provideRepository(@NonNull Context context) {
        return Repository.getInstance(
                RemoteDataSource.getInstance(
                        RetrofitClient.getInstance(context).create(MovieSeriesWebService.class)));
    }

    //For Movies
    private static MovieDataSource provideMovieRepository(@NonNull Context context) {
        return MovieRepository.getInstance(
                RemoteMovieDataSource.getInstance(
                        RetrofitClient.getInstance(context).create(MovieSeriesWebService.class)));
    }

    //For Series
    private static SeriesDataSource provideSeriesRepository(@NonNull Context context) {
        return SeriesRepository.getInstance(
                RemoteSeriesDataSource.getInstance(
                        RetrofitClient.getInstance(context).create(MovieSeriesWebService.class)));
    }

    public static UseCaseHandler provideUseCaseHandler() {
        return UseCaseHandler.getInstance();
    }

    public static GetTrending provideGetTrending(@NonNull Context context) {
        return new GetTrending(Injection.provideRepository(context));
    }

    public static GetSearch provideGetSearch(@NonNull Context context) {
        return new GetSearch(Injection.provideRepository(context));
    }

    public static GetMovie provideGetMovie(@NonNull Context context) {
        return new GetMovie(Injection.provideMovieRepository(context));
    }

    public static GetSeries provideGetSeries(@NonNull Context context) {
        return new GetSeries(Injection.provideSeriesRepository(context));
    }

}
