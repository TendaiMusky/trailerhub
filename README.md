# TrailerHub
TrailerHub is application that boasts an elegant UI and follows good architectural, design and coding conventions. 
Instead of scouring through multiple websites and going to and fro Youtube, IMDB etc, TrailerHub brings all your favourite movies and tv shows in one place. 
It displaying movie and series details and allows you to even watch trailers. 

It will be on the Google PlayStore soon.

## Features
* Displays movies and tv shows
* Search for movies and tv shows
* Watch Trailers

## Screenshots
![](https://drive.google.com/file/d/1GWtFgBj0YqI96Rtd3LpnJ9QMpv_KBCAa/preview)


![](https://db3pap007files.storage.live.com/y4m-XSjPW97Ovxnx4XnhjsvL7DFqAWVO6UfM18oYtnQPD6fHDw31G3qfhQ2nnRcUWnLZH7pz_O7P0YKZJwE5jxNorR19TIy8TDn_Aau9xukcwJTCkG7J-381NqYI3oJxb-b80tusRxlGHKrFICyAF_P84PjMotY4u0-w_thFIFZ1YAQpX7pOE8URCsr3X5kZVGU?width=720&height=1560&cropmode=none)
###### Movies Page


![](https://db3pap007files.storage.live.com/y4mKkkjZTJKxDaaNIUF9fPkb6NAbfjpF9z7mYgwYmHDIdStXVzF0tYdTurS9TizQ9q_pzKrmhklNQgosKqwh7dGO8_UMeAZPRXS6k4r2y4BpsYGFpDGl3ku9f-KbgzwePmY39UeGuFn_dl4gvcXkunqenGG-Up8doMgETAGa6I9q_3m45WREVhOjquaT0KW_MzK?width=720&height=1560&cropmode=none)
###### Series Page


![](https://db3pap007files.storage.live.com/y4mRTQf1-ca6Nky54n61BfzmWcbSbHMSB079c1EdGHKSAKbyA9E8KitVXaqHmi1J_qE36Z4YfkkSy6TW8FXt5eIo8ms96Hrw8KXN726a3HsX65gPrs2uWAppNA9zjEczE5EOyjXNaccA2U2ElWvxhe1jgyTCqWjP9ZZ0BYvJF8fPcyciCvDB0jjxsPBsjj4c4iS?width=720&height=1560&cropmode=none)
###### Trailer Page
## Architecture
The app has two build variants namely prod and mock. Each variant has both a debug and release variant. 

## Contribute
This is private repository and it is not accepting  contributions at the moment. 

## Licence
I still don't know how these open source licences work. I will add one soon. 
